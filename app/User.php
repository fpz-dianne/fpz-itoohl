<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use DJB\Confer\Traits\CanConfer;

class User extends Model implements Authenticatable {

    use Notifiable;
    use CanConfer;

	protected $fillable =  array('first_name', 'last_name', 'email', 'password', 'avatar', 'role', 'status', 'company');
    
    use \Illuminate\Auth\Authenticatable;
    
    public function portals() {
    	return $this->hasMany('App\Portal');
    }

    public function request_quotations() {
    	return $this->hasMany('App\RequestQuotation');
    }

    public function sendquotation() {
    	return $this->hasMany('App\sendquotation');
    }

    public function booking() {
        return $this->hasMany('App\booking');
    }

    public function deactivateacc() {
        return $this->hasMany('App\deactivateacc');
    }

    public function bookmark() {
        return $this->hasMany('App\bookmark');
    }
    
    public function billboards()
    {
        return $this->hasMany('App\Billboard');
    }
}
