<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EconomicClass extends Model
{
    protected $fillable = array('name');
}
