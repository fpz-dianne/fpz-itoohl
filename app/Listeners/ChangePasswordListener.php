<?php

namespace App\Listeners;

use App\Events\ChangePasswordEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use Mail;

class ChangePasswordListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChangePasswordEvent  $event
     * @return void
     */
    public function handle(ChangePasswordEvent $event)
    {
        $user = User::find($event->userId)->toArray();

        Mail::send('emails.user-profile-change-password-email', compact('user'), function ($message) use ($user) {
            $message->from( env('MAIL_USERNAME') );
            $message->to($user['email'])->subject('Change Password');
        });
    }
}
