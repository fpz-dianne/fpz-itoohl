<?php

namespace App\Listeners;

use App\Events\UserRegistrationEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;

class UserRegistrationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistrationEvent  $event
     * @return void
     */
    public function handle(UserRegistrationEvent $event)
    {
        $first_name = $event->first_name;
        $email = $event->email;

        Mail::send('emails.user-registration', compact('first_name'), function ($message) use ($email) {
            $message->from( env('MAIL_USERNAME') );
            $message->to($email)->subject('Accout has been registered');
        });
    }
}
