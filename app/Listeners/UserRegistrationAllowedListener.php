<?php

namespace App\Listeners;

use App\Events\UserRegistrationAllowedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use Mail;

class UserRegistrationAllowedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistrationAllowedEvent  $event
     * @return void
     */
    public function handle(UserRegistrationAllowedEvent $event)
    {
        $user = User::find($event->userId)->toArray();

        Mail::send('emails.user-registration-allowed', compact('user'), function ($message) use($user) {
            $message->from( env('MAIL_USERNAME') );
            $message->to($user['email'])->subject('Account Accepted');
        });
    }
}
