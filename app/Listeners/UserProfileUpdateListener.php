<?php

namespace App\Listeners;

use App\Events\UserprofileUpdateEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use Mail;

class UserProfileUpdateListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserprofileUpdateEvent  $event
     * @return void
     */
    public function handle(UserprofileUpdateEvent $event)
    {

        $user = User::find($event->userId)->toArray();

        Mail::send('emails.user-profile-update-email', compact('user'), function ($message) use ($user) {
                $message->from( env('MAIL_USERNAME') );
                $message->to($user['email'])->subject('Profile Updated');
            });
    }
}
