<?php

namespace App\Listeners;

use App\Events\BillboardCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use Mail;

class BillboardCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BillboardCreatedEvent  $event
     * @return void
     */
    public function handle(BillboardCreatedEvent $event)
    {
        $user = User::find($event->userId)->toArray();
        $billboard = $event->billboard;

        Mail::send('emails.v2.inventory-add-email',
            compact('user', 'billboard'),
            function ($message) use ($user) {
                $message->from( env('MAIL_USERNAME') );
                $message->to($user['email'])->subject('Billboard created');
            }
        );
    }
}
