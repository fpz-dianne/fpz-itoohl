<?php

namespace App\Listeners;

use App\Events\InventoryDeleteEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Portal;
use Mail;

class InventoryDeleteListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InventoryDeleteEvent  $event
     * @return void
     */
    public function handle(InventoryDeleteEvent $event)
    {
        $user = User::find($event->userId)->toArray();
        $portal = Portal::find($event->portalId)->toArray();

        Mail::send('emails.inventory-delete-email', compact('user', 'portal'), function ($message) use ($user) {
                $message->from( env('MAIL_USERNAME') );
                $message->to($user['email'])->subject('Inventory Deleted');
            });
    }
}
