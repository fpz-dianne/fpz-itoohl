<?php

namespace App\Listeners;

use App\Events\BookingMemoSendEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\sendquotation;
use Mail;

class BookingMemoSendListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingMemoSendEvent  $event
     * @return void
     */
    public function handle(BookingMemoSendEvent $event)
    {
        $user = User::find($event->userId)->toArray();
        $data = $event->data;
        $quote = sendquotation::find($event->quoteId)->toArray();

        Mail::send('emails.booking-memo-send-email', compact('user', 'data', 'quote'), function ($message) use ($quote) {
            $message->from( env('MAIL_USERNAME') );
            $message->to($quote['receiver_email'])->subject('Send Booking Memo');
        });
    }
}
