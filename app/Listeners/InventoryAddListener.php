<?php

namespace App\Listeners;

use App\Events\InventoryAddEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use Mail;

class InventoryAddListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InventoryAddEvent  $event
     * @return void
     */
    public function handle(InventoryAddEvent $event)
    {
        $user = User::find($event->userId)->toArray();
        $data = $event->data;
        
        Mail::send('emails.inventory-add-email', compact('user', 'data'), function ($message) use ($user) {
            $message->from( env('MAIL_USERNAME') );
            $message->to($user['email'])->subject('Inventory Added');
        });
    }
}
