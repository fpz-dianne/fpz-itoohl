<?php

namespace App\Listeners;

use App\Events\BillboardDeletedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Billboard;
use Mail;

class BillboardDeletedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BillboardDeletedEvent  $event
     * @return void
     */
    public function handle(BillboardDeletedEvent $event)
    {
        $user = User::find($event->userId)->toArray();
        $billboard = Billboard::find($event->billboardId)->toArray();

        Mail::send('emails.v2.inventory-delete-email',
            compact('user', 'billboard'),
            function ($message) use ($user) {
                $message->from( env('MAIL_USERNAME') );
                $message->to($user['email'])->subject('Billboard deleted');
            }
        );
    }
}
