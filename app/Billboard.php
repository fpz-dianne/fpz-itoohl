<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billboard extends Model
{
    protected $fillable = [
        'user_id',
        'date_of_collection',
        'highway',
        'product',
        'creative_campaign',
        'structure',
        'size',
        'cost',
        'region',
        'city',
        'location',
        'image'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function bookmark()
    {
        return $this->hasMany('App\bookmark');
    }
}
