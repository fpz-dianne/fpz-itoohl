<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class requestdemo extends Model
{
    protected $fillable =  array('DemoOption_id', 'DemoOption2_id', 'name', 'email', 'company');
	
	public function user() {
		return $this->belongsTo('App\User');
	}

	public function demoOption() {
		return $this->belongsTo('App\DemoOption', 'DemoOption_id');
	}

	public function demoOption2() {
		return $this->belongsTo('App\DemoOption2', 'DemoOption2_id');
	}
}
