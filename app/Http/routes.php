<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware'], function () {

	Route::get('/', function () {
		return view('index');
	})->name('home');

	Route::get('getstarted', [
		'uses' => 'PagesController@getGetStarted',
		'as' => 'getstarted'
	]);

	Route::get('launch', [
		'uses' => 'PagesController@getLaunch',
		'as' => 'launch',
		'middleware' => 'auth'
	]);

	Route::get('portal', [
		'uses' => 'PortalController@getPortal',
		'as' => 'portal',
		'middleware' => 'auth'
	]);

	Route::get('portal-list', [
		'uses' => 'PortalController@getPortalListView',
		'as' => 'portal-list',
		'middleware' => 'auth'
	]);

	Route::get('single-news', [
		'uses' => 'NewsController@getSingleNews',
		'as' => 'single-news'
	]);

	Route::post('request-demo', [
		'uses' => 'RequestDemoController@postRequestDemo',
		'as' => 'request-demo'
	]);

	Route::get('demo-list', [
		'uses' => 'RequestDemoController@getDemoList',
		'as' => 'demo-list',
		'middleware' => 'auth'
	]);

	Route::get('demo-delete/{id}', [
		'uses' => 'RequestDemoController@getDemoDelete',
		'middleware' => 'auth'
	]);

	Route::get('send-demo/{id}', 'RequestDemoController@getSendDemo');

	Route::post('subscribe', [
		'uses' => 'SubscribeController@postSubscribe',
		'as' => 'subscribe'
	]);

	Route::get('subscriber-list', [
		'uses' => 'SubscribeController@getSubscriberList',
		'as' => 'subscriber-list',
		'middleware' => 'auth'
	]);

	Route::get('subscriber-delete/{id}', 'SubscribeController@getSubscriberDelete');

	Route::post('signup', [
		'uses' => 'UserController@postSignUp',
		'as' => 'signup'
	]);

	Route::post('signin', [
		'uses' => 'UserController@postSignin',
		'as' => 'signin'
	]);

	Route::post('deactivate-accnt', [
		'uses' => 'DeactivateAccntController@postDeactivateAccnt',
		'as' => 'deactivate-accnt'
	]);

	Route::get('change-password/{remember_token}', 'PasswordController@getChangePassword');

	Route::post('change-password', [
		'uses' => 'PasswordController@postChangePassword',
		'as' => 'change-password'
	]);

	Route::get('change-email/{remember_token}', [
		'uses' => 'PasswordController@getChangeEmail',
		'middleware' => 'auth'	
	]);

	Route::get('change-email-link', [
		'uses' => 'PasswordController@getChangeEmailLink',
		'as' => 'change-email-link'
	]);

	Route::get('lockscreen', [
		'uses' => 'UserController@getLockScreen',
		'as' => 'lockscreen',
		'middleware' => 'auth'
	]);

	Route::post('lockscreen', [
		'uses' => 'UserController@postLockScreen',
		'as' => 'lockscreen'
	]);

	Route::get('logout', [
		'uses' => 'UserController@getLogout',
		'as' => 'logout'
	]);

	//User (admin, advertiser, customer)
	Route::get('user-dashboard', [
		'uses' => 'UserController@getUserDashboard',
		'as' => 'user-dashboard',
		'middleware' => 'auth'
	]);

	//Dashboard Generate Report
	Route::get('generate-report', [
		'uses' => 'GenerateReportController@getGenerateReport',
		'as' => 'generate-report',
		'middleware' => 'auth'
	]);

	Route::post('generate-report', [
		'uses' => 'GenerateReportController@postGenerateReport',
		'as' => 'generate-report'
	]);

	Route::get('generate-report-compare', [
		'uses' => 'GenerateReportController@getGenerateReportCompare',
		'as' => 'generate-report-compare',
		'middleware' => 'auth'
	]);

	Route::post('generate-report-compare', [
		'uses' => 'GenerateReportController@postGenerateReportCompare',
		'as' => 'generate-report-compare'
	]);

	Route::get('ajax-report-products-1', [
		'uses' => 'GenerateReportController@getAjaxReportProductList1',
		'as' => 'ajax-report-products-1'	
	]);

	Route::get('ajax-report-products-2', [
		'uses' => 'GenerateReportController@getAjaxReportProductList2',
		'as' => 'ajax-report-products-2'
	]);

	Route::get('generate-report-sales', [
		'uses' => 'GenerateReportController@getGenerateReportSales',
		'as' => 'generate-report-sales',
		'middleware' => 'auth'
	]);

	//Dashboard Profile
	Route::get('profile', [
		'uses' => 'UserController@getProfile',
		'as' => 'profile',
		'middleware' => 'auth'
	]);

	//User Visit Profile
	Route::get('profile/{id}', [
		'uses' => 'UserController@getvisitProfile',
		'middleware' => 'auth'
	]);

	Route::post('update-profile', [
			'uses' => 'UserController@postUpdateProfile',
			'as' => 'profile.update'
		]);

	Route::get('add-inventory', [
		'uses' => 'UserController@getAddBillboard',
		'as' => 'add-billboard',
		'middleware' => 'auth'
	]);

	//Dashboard CRUD Inventory
	Route::get('inventory-list', [
		'uses' => 'UserController@getInventoryList',
		'as' => 'inventory-list',
		'middleware' => 'auth'
	]);

	Route::get('inventory-list/{id}', [
		'uses' => 'UserController@getInventoryEdit',
		'middleware' => 'auth'	
	]);

	Route::post('inventory-update/{id}', 'UserController@postInventoryUpdate');

	Route::get('inventory-delete/{id}', 'UserController@getInventoryDelete');

	Route::post('create-billboard', [
		'uses' => 'UserController@postCreateBillboard',
		'as' => 'create-billboard',
	]);
	
	//Update Imventory Image
	Route::post('update-inventory-image/{id}', 'UserController@updateInventoryImage');
	
	//Delete Inventory Image
	Route::get('delete-inventory-image/{id}', 'UserController@deleteInventoryImage');

	//Dashboard CRUD Quotes and Booking
	Route::get('booking', [
		'uses' => 'UserController@getBooking',
		'as' => 'booking',
		'middleware' => 'auth'
	]);

	Route::post('request-quotes', [
		'uses' => 'UserController@postRequestQuotes',
		'as' => 'request-quotes'
	]);

	Route::get('requested-quotes-delete/{id}', 'UserController@getRequestedQuotesDelete');

	Route::get('requested-quotes', [
		'uses' => 'UserController@getRequestedQuotesList',
		'as' => 'requested-quotes',
		'middleware' => 'auth'
	]);

	Route::get('request-quotes/{id}', [
		'uses' => 'UserController@getRequestedQuotesView',
		'middleware' => 'auth'
	]);

	Route::post('send-quote/{id}', 'UserController@postSendQuote');

	Route::get('quotations', [
		'uses' => 'UserController@getQuotationsList',
		'as' => 'quotations',
		'middleware' => 'auth'
	]);

	Route::get('quotes-delete/{id}', 'UserController@getQuotesDelete');

	Route::get('quotes/{id}', [
		'uses' => 'UserController@getQuotesView',
		'middleware' => 'auth'
	]);

	Route::get('bookmark/{id}', [
		'uses' => 'UserController@getBookmark'
	]);

	Route::get('bookmark-list', [
		'uses' => 'UserController@getBookmarkList',
		'as' => 'bookmark-list',
		'middleware' => 'auth',
	]);

	Route::get('bookmark-delete/{id}', [
		'uses' => 'UserController@getBookmarkDelete',
	]);

	Route::post('send-booking-memo/{id}', 'UserController@postSendBookingMemo');

	Route::get('booking', [
		'uses' => 'UserController@getBookingMemoList',
		'as' => 'booking',
		'middleware' => 'auth'
	]);

	Route::get('booking-inbox', [
		'uses' => 'UserController@getBookingInbox',
		'as' => 'booking-inbox',
		'middleware' => 'auth'
	]);

	Route::get('booking/{id}', [
		'uses' => 'UserController@getBookingMemoView',
		'middleware' => 'auth'
	]);

	Route::get('booking-delete/{id}', 'UserController@getBookingDelete');

	//User Registration List
	Route::get('user-registration', [
		'uses' => 'UserController@getUserRegistrationList',
		'as' => 'user-registration',
		'middleware' => 'auth'
	]);

	//Allow User Registration / Accept
	Route::post('allow-user-registration/{id}', 'UserController@postAllowUserRegistration');

	//Deny User Registration
	Route::get('deny-user-registration/{id}', 'UserController@getDenyUserRegistration');

	//Delete User Registration
	Route::get('delete-user-registration/{id}', 'UserController@getDeleteUserRegistration');

	//Update Profile Avatar
	Route::post('update-avatar', [
		'uses' => 'UserController@updateAvatar',
		'as' => 'update-avatar'
	]);
	
	//Delete Profile Avatar
	Route::get('delete-avatar/{avatar}', 'UserController@deleteAvatar');

	//Admin
	Route::get('login', [
		'uses' => 'Admincontroller@getLogin',
		'as' => 'login'
	]);

	Route::post('admin-login', [
		'uses' => 'Admincontroller@postAdminLogin',
		'as' => 'admin-login',
	]);

	Route::get('admin-logout', [
		'uses' => 'Admincontroller@getAdminLogout',
		'as' => 'admin-logout'
	]);

	//download pdf
	Route::get('pdf', function(){
		$pdf = PDF::loadView('pdf');
		return $pdf->download('samp.pdf');
	});

	Route::get('ajax-filter-inventory', [
		'uses' => 'PortalController@getAjaxFilterInventory',
		'as' => 'ajax-filter-inventory'	
	]);

	Route::get('ajax-filter-traffic', [
		'uses' => 'PortalController@getAjaxFilterTraffic',
		'as' => 'ajax-filter-traffic'	
	]);

	Route::get('ajax-filter-circle', [
		'uses' => 'PortalController@getAjaxFilterCircle',
		'as' => 'ajax-filter-circle'	
	]);

	Route::get('ajax-autocomplete', [
		'uses' => 'PortalController@getAjaxAutocomplete',
		'as' => 'ajax-autocomplete'	
	]);

	Route::get('ajax-autocomplete-select', [
		'uses' => 'PortalController@getAjaxAutocompleteSelect',
		'as' => 'ajax-autocomplete-select'	
	]);

	Route::get('ajax-search', [
		'uses' => 'PortalController@getAjaxSearch',
		'as' => 'ajax-search'	
	]);

	Route::get('ajax-bookmark-view/{id}', [
		'uses' => 'PortalController@getAjaxBookmarkView'
	]);

	//Notification
	Route::get('notify-demo-read/{id}', 'NotificationController@getNotfiyDemoRead');

	Route::get('notify-quotes-read/{id}', 'NotificationController@getNotfiyQuotesRead');

	Route::get('notify-send-quotes-read/{id}', 'NotificationController@getNotfiySendQuotesRead');

	Route::get('notify-send-booking-memo-read/{id}', 'NotificationController@getNotfiySendBookingMemoRead');

	Route::get('notify-new-inventory-read/{id}', 'NotificationController@getNotfiyNewInventoryRead');

	Route::get('notify-update-inventory-read/{id}', 'NotificationController@getNotfiyUpdateInventoryRead');

	Route::get('notify-signup-read/{id}', 'NotificationController@getNotfiySignupRead');

	Route::get('notify-read-all', [
		'uses' => 'NotificationController@getNotifyReadAll',
		'as' => 'notify-read-all'
	]);

	Route::get('notify-delete/{id}', 'NotificationController@getNotifyDelete');

	Route::group(['middleware'	=> 'auth'], function() {
		Route::group(['prefix' => '/billboards'], function() {
			Route::get('/', [
				'uses'	=> 'UserController@getBillboards',
				'as'	=> 'billboards'
			]);

			Route::get('/new', [
				'uses'	=> 'UserController@newBillboard',
				'as'	=> 'new-billboard'
			]);

			Route::post('/new', [
				'uses'	=> 'UserController@saveBillboard',
				'as'	=> 'save-billboard'
			]);

			Route::get('/{id}', 'UserController@getBillboard');
			Route::post('/{id}', 'UserController@updateBillboard');

			Route::get('/{id}/delete-image', 'UserController@deleteBillboardImage');
			Route::post('/{id}/update-image', 'UserController@updateBillboardImage');

			Route::get('/{id}/delete', 'UserController@deleteBillboard');
		});

	});

	//Route::get('ajax-notify-read', [
	//	'uses' => '<NotificationController@getAjaxNotifyRead></NotificationController@getAjaxNotifyRead>',
	//	'as' => 'ajax-notify-read'
	//]);

});
