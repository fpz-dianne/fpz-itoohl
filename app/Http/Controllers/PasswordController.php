<?php

namespace App\Http\Controllers;

	use Mail;
	use Alert;
	use Event;

	use App\User;

	use App\Http\Requests;
	use Illuminate\Http\Request;
	use Illuminate\Http\Response;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\Hash;

	use App\Events\ChangePasswordEvent;
	use App\Events\ChangeEmailEvent;

class PasswordController extends Controller
{
    public function getChangePassword() {
    	return view('password.change-password', ['user' => Auth::user()]);
    }

    public function postChangePassword(Request $request) {

    	$this->validate($request, [
			'password' => 'required|min:6',
			'confirm_password' => 'required|min:6'
		]);

    	$password = $request['password'];
    	$confirm_password = $request['confirm_password'];

    	if ($password == $confirm_password) {
    		
    		$user = Auth::user();
    		$userId = $user->id;
			$user->password = bcrypt($password);
			$user->update();

			Event::fire(new ChangePasswordEvent($userId));

			return redirect()->route('logout');
    	}
    	else {
    		alert()->warning('Password Dont Match!')->persistent();

    		return redirect()->back();
    	}

    }

    public function getChangeEmail($remember_token) {
		
		alert()->success('Change Email Sent')->persistent('close');

		$userId = Auth::user()->id;

		Event::fire(new ChangeEmailEvent($userId));

    	return redirect()->route('profile', ['user' => Auth::user()]);
    	
    }

    public function getChangeEmailLink() {
    
    	return view('password.change-email');
    
    }
    
}
