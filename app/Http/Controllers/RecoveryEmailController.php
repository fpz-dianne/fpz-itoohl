<?php

namespace App\Http\Controllers;

	use Mail;
	use Session;
	use Alert;

	use App\User;
	use App\recoveryemail;

	use App\Http\Requests;
	use Illuminate\Http\Request;
	use Illuminate\Http\Response;
	use Illuminate\Support\Facades\Auth;

class RecoveryEmailController extends Controller {
    
	public function postRecoveryEmail(Request $request) {
		
		$email = $request['email'];

		$recovery_email = new Recoveryemail();
		$recovery_email->recovery_email = $email;

		$request->user()->recoveryemail()->save($recovery_email);

		alert()->success('Recovery Email has been set')->persistent('Confirm');

		return redirect()->route('profile');

	}
}
