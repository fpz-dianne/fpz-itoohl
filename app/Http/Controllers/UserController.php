<?php

namespace App\Http\Controllers;
	
	use Image;
	use Mail;
	use Session;
	use Alert;
	use Carbon;
	use File;
	use Event;

	use App\User;
	use App\Portal;
	use App\subscriber;
	use App\country;
	use App\city;
	use App\industry;
	use App\size;
	use App\format;
	use App\RequestQuotation;
	use App\sendquotation;
	use App\booking;
	use App\availability;
	use App\gender;
	use App\age;
	use App\EconomicClass;
	use App\bookmark;
	use App\Region;
	use App\Billboard;

	use App\Http\Requests;
	use Illuminate\Http\Request;
	use Illuminate\Http\Response;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Notifications\Notifiable;

    use App\Notifications\RequestQuotesNotification;
    use App\Notifications\SendQuotesNotification;
    use App\Notifications\SendBookingMemoNotification;
    use App\Notifications\NewInventoryNotification;
    use App\Notifications\UpdateInventoryNotification;
    use App\Notifications\SignupNotification;

    use App\Events\UpdateInventoryEvent;
    use App\Events\UserProfileUpdateEvent;
    use App\Events\InventoryDeleteEvent;
    use App\Events\QuotationRequestEvent;
    use App\Events\QuotationSendEvent;
    use App\Events\BookingMemoSendEvent;
    use App\Events\InventoryAddEvent;
    use App\Events\BookingMemoDeleteEvent;
    use App\Events\UserRegistrationEvent;
    use App\Events\UserRegistrationAllowedEvent;
    use App\Events\UserChangeRoleEvent;
    use App\Events\UserRegistrationDeniedEvent;
	use App\Events\UserRegistrationDeleteEvent;
	use App\Events\BillboardCreatedEvent;
	use App\Events\BillboardUpdatedEvent;
	use App\Events\BillboardDeletedEvent;

	class UserController extends Controller {

		//dashboard
		public function getUserDashboard() {

			$industries = Industry::all();
			$portals = Portal::all();

			return view('user.user-dashboard', compact('industries'));
		}

		//profile
		public function getProfile() {

			$user = Auth::user();
			$portals = Portal::where('user_id', $user->id)->paginate(6);
			$notifications = Auth::user()->Notifications;

			return view('user.profile', compact('user', 'portals', 'notifications'));
		}

		//update profile
		public function postUpdateProfile( Request $request ) {

			$user = Auth::user();
			$user->first_name = $request['first_name'];
			$user->last_name = $request['last_name'];
			$user->company = $request['company'];
			$user->phone = $request['phone'];
			$user->update();

			alert()->success('Profile Updated');

			$userId = $user->id;

			Event::fire(new UserProfileUpdateEvent($userId));

			return redirect()->route('profile', ['user' => Auth::user()]);

		}

		public function updateAvatar( Request $request ) {

			$this->validate($request, [
				'avatar' => 'mimes:jpeg,jpg,png'
			]);

			//handle the user upload avatar
			if($request->hasFile('avatar')) {
				$avatar = $request->file('avatar');
				$filename = time() . '.' . $avatar->getClientOriginalExtension();
				Image::make($avatar)->resize(160, 160)->save( public_path('uploads/avatars/' . $filename ) );

				$user = Auth::user();
				$user->avatar = $filename;
				$user->save();
			}

			alert()->success('Profile Updated');

			return redirect()->route('profile', ['user' => Auth::user()]);
		}
		
		public function deleteAvatar($avatar){
			File::delete('uploads/avatars/' . $avatar);
			
			$user = Auth::user();
			$user->avatar = 'default.jpg';
			$user->update();

			alert()->success('Profile Updated');
			
			return redirect()->route('profile', ['user' => Auth::user()]);
		}

		//User Visit Profile
		public function getvisitProfile($id) {
			$user = User::findOrFail($id);
			$portals = Portal::where('user_id', $user->id)->paginate(6);

			return view('user.visit-profile', compact('user', 'portals'));
		}

		//Booking
		public function getBooking() {
			return view('user.booking');
		}

		//Inventory List
		public function getInventoryList() {

			if(Auth::user()->role == 2 ) {
				return redirect()->route('user-dashboard');
			}

			if(Auth::user()->role == 1 ) {
				$portals = Portal::orderBy('id', 'desc')->get();
			}
			else {
				$portals = Portal::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get();
			}

			return view('user.inventory-list', 
				compact('portals'),
				['user' => Auth::user()]
			);
		}

		//Inventory Edit View
		public function getInventoryEdit($id) {

			$portal = Portal::findOrFail($id);
			$countries = Country::all();
			$cities = City::all();
			$formats = Format::all();
			$industries = Industry::all();
			$sizes = Size::all();
			$availabilities = Availability::all();
			$genders = Gender::all();
			$economic_classes = EconomicClass::all();
			$ages = Age::all();

			if(Auth::user()->role == 2 ) {
				return redirect()->route('user-dashboard');
			}

			return view('user.inventory-edit', compact('portal', 'countries', 'cities', 'formats', 'industries', 'sizes', 'availabilities', 'genders', 'economic_classes', 'ages'));
		}

		//Inventory Update
		public function postInventoryUpdate($id, Request $request) {

			if(Auth::user()->role == 2 ) {
				return redirect()->route('user-dashboard');
			}

			$portal = Portal::findOrFail($id);

			$portal->update($request->all());

			alert()->success('Inventory Updated');

			//Notify
			$user = Auth::user()->first_name;
			$userId = Auth::user()->id;
			$portalId = $portal->id;
			$portal_name = $portal->name;
			
			User::where('id', 1)->first()->notify(new UpdateInventoryNotification($user, $portal_name));

			Event::fire(new UpdateInventoryEvent($userId, $portalId));

			return redirect()->route('inventory-list');
		}

		//Inventory Delete
		public function getInventoryDelete($id) {
			
			$portal = Portal::findOrFail($id);

			$userId = Auth::user()->id;
			$portalId = $portal->id;

			Event::fire(new InventoryDeleteEvent($userId, $portalId));
			
			$portal->delete();
			
			alert()->success('Inventory Deleted');
			
			return redirect()->route('inventory-list');
		}

		//Add Inventory View
		public function getAddBillboard() {

			$countries = Country::all();
			$cities = City::all();
			$industries = Industry::all();
			$sizes = Size::all();
			$formats = Format::all();
			$availabilities = Availability::all();
			$genders = Gender::all();
			$economic_classes = EconomicClass::all();
			$ages = Age::all();

			if(Auth::user()->role == 2 ) {
				return redirect()->route('user-dashboard');
			}

			return view('user.add-billboard', 
				compact('countries', 'cities', 'industries', 'sizes', 'formats', 'availabilities', 'genders', 'economic_classes', 'ages')
			);
		}


		//Add Inventory
		public function postCreateBillboard(Request $request) {	

			$name = $request['name'];
			$supplier = $request['supplier'];
			$company = $request['company'];
			$country = $request['country'];
			$city = $request['city'];
			$street_address = $request['street_address'];
			$format = $request['format'];
			$product = $request['product'];
			$industry = $request['industry'];
			$landmark = $request['landmark'];
			$size = $request['size'];
			$payment_terms = $request['payment_term'];
			$availability = $request['availability'];
			$gender = $request['gender'];
			$economic_class = $request['economic_class'];
			$age = $request['age'];
			$rates = $request['rates'];	
			$latitude = $request['latitude'];
			$longitude = $request['longitude'];
			$inventory_image = $request->file('inventory_image');
			$filename = time() . '.' . $inventory_image->getClientOriginalExtension();
			Image::make($inventory_image)->resize(300, 300)->save( public_path('uploads/inventory-images/' . $filename ) );


			$portal = new Portal();
			$portal->name = $name;
			$portal->supplier = $supplier;
			$portal->country= $country;
			$portal->company= $company;
			$portal->city = $city;
			$portal->street_address = $street_address;
			$portal->format = $format;
			$portal->products = $product;
			$portal->industry = $industry;
			$portal->landmark = $landmark;
			$portal->size = $size;
			$portal->payment_terms = $payment_terms;
			$portal->availability = $availability;
			$portal->gender = $gender;
			$portal->economic_class = $economic_class;
			$portal->age = $age;
			$portal->rates = $rates;
			$portal->latitude = $latitude;
			$portal->longitude = $longitude;
			$portal->inventory_image = $filename;
			

			$request->user()->portals()->save($portal);

			alert()->success('Inventory Added');

			//Notify
			$user = Auth::user()->first_name;
			User::where('id', 1)->first()->notify(new NewInventoryNotification($user));

			$userId = Auth::user()->id;
			$data = $request->all();

			Event::fire(new InventoryAddEvent($userId, $data));

			return redirect()->route('add-billboard');
		}

		
		//Update Inventory Image
		public function updateInventoryImage( $id, Request $request ) {

			//handle the user upload inventory image
			if($request->hasFile('inventory-image')) {
				$inventory_image = $request->file('inventory-image');
				$filename = time() . '.' . $inventory_image->getClientOriginalExtension();
				Image::make($inventory_image)->resize(1200, 500)->save( public_path('uploads/inventory-images/' . $filename ) );

				$inventory = Portal::findOrFail($id);
				$inventory->inventory_image = $filename;
				$inventory->save();
			}
			
			alert()->success('Inventory Image Updated');

			return redirect()->back();
		}
		
		//Delete Inventory Image
		public function deleteInventoryImage($id){
			$inventory = Portal::findOrFail($id);
			
			File::delete('uploads/inventory-images/' . $inventory->inventory_image);
			
			$inventory->inventory_image = 'inventory_image_default.jpg';
			$inventory->update();
			
			alert()->success('Inventory Image Deleted');
			
			return redirect()->back();
		}

		//Request Quote
		public function postRequestQuotes(Request $request) {

			$user_email = Auth::user()->email;
			$user_first_name = Auth::user()->first_name;
			$receiver_id = $request['receiver_id'];
			$receiver_email = $request['receiver_email'];
			$receiver_name = $request['receiver_name'];
			$name = $request['inventory_name'];
			$img = $request['inventory_img'];
			$placement_duration = $request['placement_duration'];
			$material_changes = $request['material_changes'];
			$material_changes_date = $request['material_changes_date'];
			$production = $request['production'];
			$instruction = $request['instruction'];

			$request_quotes = new RequestQuotation();
			$request_quotes->user_email = $user_email;
			$request_quotes->user_first_name = $user_first_name;
			$request_quotes->receiver_id = $receiver_id;
			$request_quotes->receiver_email = $receiver_email;
			$request_quotes->receiver_name = $receiver_name;
			$request_quotes->name = $name;
			$request_quotes->inventory_img = $img;
			$request_quotes->placement_duration = $placement_duration;
			$request_quotes->material_changes = $material_changes;
			$request_quotes->material_changes_date = $material_changes_date;
			$request_quotes->production = $production;
			$request_quotes->instruction = $instruction;

			$request->user()->request_quotations()->save($request_quotes);

			alert()->success('Request Submitted');

			 //Notify
        	User::where('id', $receiver_id)->first()->notify(new RequestQuotesNotification($user_first_name));
			

			$userId = Auth::user()->id;
			$data = $request->all();

			Event::fire(new QuotationRequestEvent($userId, $receiver_email, $data));

			return redirect()->route('portal');
		}

		//Quotes Requested List
		public function getRequestedQuotesList() {

			$request_quotations = RequestQuotation::where('receiver_id', Auth::user()->id)->latest()->get();

			//$user = Auth::user();

			//$notification = $user->notifications()->where('type', 'App\Notifications\RequestQuotesNotification')->get();

        	//$notification->markAsRead();

			return view('user.requested-quotes', 
				compact('request_quotations')
			);

		}

		//Send Quotes
		public function postSendQuote($id, Request $request) {

			$request_quote = RequestQuotation::findOrFail($id);

			$user_email = Auth::user()->email;
			$user_first_name = Auth::user()->first_name;
			$receiver_id = $request_quote->user_id;
			$receiver_email = $request_quote->user_email;
			$receiver_name = $request_quote->user_first_name;
			$name = $request_quote->name;
			$img = $request_quote->inventory_img;
			$placement_duration = $request['placement_duration'];
			$material_changes = $request['material_changes'];
			$material_changes_date = $request['material_changes_date'];
			$production = $request['production'];
			$instruction = $request['instruction'];
			$quotation = $request['quotation'];
			$note = $request['note'];

			$send_quote = new sendquotation();
			$send_quote->user_email = $user_email;
			$send_quote->user_first_name = $user_first_name;
			$send_quote->receiver_id = $receiver_id;
			$send_quote->receiver_email = $receiver_email;
			$send_quote->receiver_name = $receiver_name;
			$send_quote->name = $name;
			$send_quote->inventory_img = $img;
			$send_quote->placement_duration = $placement_duration;
			$send_quote->material_changes = $material_changes;
			$send_quote->material_changes_date = $material_changes_date;
			$send_quote->production = $production;
			$send_quote->instruction = $instruction;
			$send_quote->quotation = $quotation;
			$send_quote->note = $note;


			$request->user()->sendquotation()->save($send_quote);

			alert()->success('Quote Submitted');

			//Notify
			User::where('id', $receiver_id)->first()->notify(new SendQuotesNotification($user_first_name));

			$userId = Auth::user()->id;
			$data = $request->all();
			$request_quoteId = $request_quote->id;

			Event::fire(new QuotationSendEvent($userId, $data, $request_quoteId));

			return redirect()->route('requested-quotes');

		}

		//Send Booking Memo
		public function postSendBookingMemo($id, Request $request) {

			$request_quote = sendquotation::findOrFail($id);

			$user_email = Auth::user()->email;
			$user_first_name = Auth::user()->first_name;
			$receiver_id = $request_quote->user_id;
			$receiver_email = $request_quote->user_email;
			$confirmation = $request['confirmation'];
			$final_rate = $request['final_rate'];
			$receiver_name = $request_quote->user_first_name;
			$name = $request_quote->name;
			$img = $request_quote->inventory_img;
			$placement_duration = $request['placement_duration'];
			$material_changes = $request['material_changes'];
			$material_changes_date = $request['material_changes_date'];
			$production = $request['production'];
			$instruction = $request['instruction'];

			$send_booking_memo = new booking();
			$send_booking_memo->user_email = $user_email;
			$send_booking_memo->user_first_name = $user_first_name;
			$send_booking_memo->receiver_id = $receiver_id;
			$send_booking_memo->receiver_email = $receiver_email;
			$send_booking_memo->confirmation = $confirmation;
			$send_booking_memo->final_rate = $final_rate;
			$send_booking_memo->receiver_name = $receiver_name;
			$send_booking_memo->name = $name;
			$send_booking_memo->inventory_img = $img;
			$send_booking_memo->placement_duration = $placement_duration;
			$send_booking_memo->material_changes = $material_changes;
			$send_booking_memo->material_changes_date = $material_changes_date;
			$send_booking_memo->production = $production;
			$send_booking_memo->instruction = $instruction;

			$request->user()->booking()->save($send_booking_memo);

			alert()->success('Booking Memo Submitted');

			//Notify
        	User::where('id', $receiver_id)->first()->notify(new SendBookingMemoNotification($user_first_name));

			$userId = Auth::user()->id;
			$data = $request->all();
			$quoteId = $request_quote->id;

			Event::fire(new BookingMemoSendEvent($userId, $data, $quoteId));

			return redirect()->back();

		}

		//Booking Memo List
		public function getBookingMemoList() {

			$booking_memo = booking::where('user_id', Auth::user()->id)->latest()->get();

			return view('user.booking', compact('booking_memo'));
		}

		//Booking Inbox List
		public function getBookingInbox() {
			
			$booking_inbox = booking::where('receiver_id', Auth::user()->id)->latest()->get();

			return view('user.booking-inbox', compact('booking_inbox'));
		}

		//Booking Memo Single View
		public function getBookingMemoView($id) {
			
			$booking = booking::findOrFail($id);

			return view('user.booking-view', compact('booking'));
		}

		//Booking Memo Delete
		public function getBookingDelete($id) {
			
			$booking = booking::findOrFail($id);

			$userId = Auth::user()->id;
			$bookId = $id;

			Event::fire(new BookingMemoDeleteEvent($userId, $bookId));
			
			$booking->delete();
			
			alert()->success('Booking Memo Deleted');
			
			return redirect()->back();
		}

		// Quotes Requested Single View
		public function getRequestedQuotesView($id) {

			$request_quote = RequestQuotation::findOrFail($id);

			return view('user.request-quotes-view', compact('request_quote'));
		}

		// Quotes Requested Deleted
		public function getRequestedQuotesDelete($id) {
			
			$requested_quote = RequestQuotation::findOrFail($id);
			
			$requested_quote->delete();
			
			alert()->success('Requested Quote Deleted');
			
			return redirect()->route('requested-quotes');
		}

		//Quotations View
		public function getQuotationsList() {
			
			$quotes = sendquotation::where('receiver_id', Auth::user()->id)->latest()->get();

			//$user = Auth::user();

			//Notify
			//$notification = $user->notifications()->where('type', 'App\Notifications\SendQuotesNotification')->get();

			//$notification->markAsRead();

			return view('user.quotations', compact('quotes'));
		}

		// Quotes Single View
		public function getQuotesView($id) {

			$quotes = sendquotation::findOrFail($id);

			return view('user.quotes-view', compact('quotes'));
		}

		//Quotations Delete
		public function getQuotesDelete($id) {
			
			$quote = sendquotation::findOrFail($id);
			
			$quote->delete();
			
			alert()->success('Quote Deleted');
			
			return redirect()->route('quotations');
		}

		//Bookmark Inventory
		public function getBookmark($id, Request $request) {

			$portal = $id;

			$bookmark = new Bookmark();

			$bookmark->portal_id = $portal;

			$request->user()->bookmark()->save($bookmark);

			alert()->success('Bookmark Saved');

			return redirect()->route('portal');
		}

		//Bookmark View
		public function getBookmarkList() {

			$bookmarks = Bookmark::where('user_id', Auth::user()->id)->get();

			//dd($bookmarks);

			return view('user.bookmark-list', compact('bookmarks'));
		}

		//Bookmark Delete
		public function getBookmarkDelete($id) {

			$bookmark = Bookmark::findOrFail($id);

			$bookmark->delete();

			return redirect()->route('bookmark-list');
		}


		//User Register
		public function postSignUp(Request $request) {

			$this->validate($request, [
				'email' => 'required|unique:users',
				'password' => 'required|min:6'
			]);

			$email = $request['email'];
			$first_name = $request['first_name'];
			$last_name = $request['last_name'];
			$password = bcrypt($request['password']);

			$user = new User();
			$user->email = $email;
			$user->first_name = $first_name;
			$user->last_name = $last_name;
			$user->password = $password;
			$user->role = 2;

			$user->save();

			alert()->success('Your Account Has Been Register');

			User::where('id', 1)->first()->notify(new SignupNotification($first_name));

			Event::fire(new UserRegistrationEvent($email, $first_name));

			return redirect()->route('getstarted');
		}

		//User Login
		public function postSignin(Request $request) {

			$this->validate($request, [
				'email' => 'required',
				'password' => 'required'
			]);

			if(Auth::attempt(['email' => $request['email'], 'password' => $request['password'], 'status' => 1])) {

				alert()->success('Login Successful');

				return redirect()->route('launch');
			}
			elseif(Auth::attempt(['email' => $request['email'], 'password' => $request['password'], 'status' => 2], false, false)) {

				alert()->warning('Account need to verify')->persistent('Close');

				return redirect()->back()->withInput();
			}
			else {
				alert()->warning('Email or password is wrong')->persistent('Close');
			}

			alert()->warning('Email or password is wrong')->persistent('Close');

			return redirect()->back()->withInput();
		}

		//Registration List
		public function getUserRegistrationList() {
			
			$users = User::orderBy('id', 'desc')->get();

			if(Auth::user()->role != 1 ) {
				return redirect()->route('user-dashboard');
			}

			return view('user.user-registration', compact('users'));
		}

		//Allow User Registration
		public function postAllowUserRegistration($id, Request $request) {
			
			$user = User::findOrFail($id);

			$userId = $id;

			$user->role = $request->input('role', $user->role);

			if($user->status == 2) {

				$user->status = 1;
				$user->update();
				
				Event::fire(new UserRegistrationAllowedEvent($userId));

				alert()->success('User Registration Accepted');
			}
			elseif($user->status == 1) {

				$user->update();
				$role = $user->role;

				Event::fire(new UserChangeRoleEvent($userId, $role));

				alert()->success('User Role Change');

			}
			elseif($user->status == 3) {

				$user->status = 1;
				$user->update();
				
				Event::fire(new UserRegistrationAllowedEvent($userId));

				alert()->success('User Registration Accepted');
			}


			return redirect()->route('user-registration');
		}

		//Deny User Registration
		public function getDenyUserRegistration($id) {
			
			$user = User::findOrFail($id);
			$user->status = 3;

			$user->update();

			alert()->success('User Registration Denied');

			$userId = $id;

			Event::fire(new UserRegistrationDeniedEvent($userId));

			return redirect()->route('user-registration');
		}

		//Delete User Registration
		public function getDeleteUserRegistration($id) {
			
			$user = User::findOrFail($id);

			$userId = $id;

			Event::fire(new UserRegistrationDeleteEvent($userId));

			$user->delete();

			alert()->success('User Registration Deleted');

			return redirect()->route('user-registration');
		}

		//User lockscreen
		public function getLockScreen() {
    		return view('user.lockscreen', ['user' => Auth::user()]);
    	}

    	//Change password lock screen
    	public function postLockScreen(Request $request) {

    		$password = $request['password'];	

    		if(Hash::check($password, Auth::user()->password)) {
    			return redirect()->route('change-password');
			}

    		return redirect()->route('lockscreen');
    	}

		//User Logout
		public function getLogout() {
			
			Auth::logout();
			Session::flush();
			return redirect()->route('login');
		}

		public function getBillboards()
		{
			$user = Auth::user();

			if ($user->role == 2) {
				return redirect()->route('user-dashboard');
			}

			if ($user->role == 1) {
				$billboards = Billboard::orderBy('id', 'desc')->get();
			} else {
				$billboards = Billboard::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get();
			}

			return view('user.v2.inventory-list', 
				compact('billboards'),
				['user' => Auth::user()]
			);
		}

		public function newBillboard()
		{
			if (Auth::user()->role == 2) {
				return redirect()->route('user-dashboard');
			}

			$cities = City::all();
			$regions = Region::all();
			$sizes = Size::all();
			$formats = Format::all();

			return view(
				'user.v2.add-billboard', 
				compact('cities', 'regions', 'sizes', 'formats')
			);
		}

		public function getBillboard(Request $request, string $id)
		{
			if (Auth::user()->role == 2) {
				return redirect()->route('user-dashboard');
			}

			$billboard = Billboard::findOrFail($id);
			$cities = City::all();
			$regions = Region::all();
			$formats = Format::all();
			$sizes = Size::all();

			return view(
				'user.v2.inventory-edit',
				compact('billboard', 'cities', 'formats', 'sizes', 'regions')
			);
		}

		public function saveBillboard(Request $request)
		{
			$dateOfCollection = $request['date_of_collection'];
			$highway = $request['highway'];
			$product = $request['product'];
			$creativeCampaign = $request['creative_campaign'];
			$structure = $request['structure'];
			$size = $request['size'];
			$cost = $request['cost'];
			$region = $request['region'];
			$city = $request['city'];
			$location = $request['location'];
			
			$inventoryImage = $request->file('inventory_image');
			$filename = time() . '.' . $inventoryImage->getClientOriginalExtension();
			Image::make($inventoryImage)->save( public_path('uploads/inventory-images/' . $filename ) );

			$billboard = new Billboard();
			$billboard->date_of_collection = $dateOfCollection;
			$billboard->highway = $highway;
			$billboard->product = $product;
			$billboard->creative_campaign = $creativeCampaign;
			$billboard->structure = $structure;
			$billboard->size = $size;
			$billboard->cost = $cost;
			$billboard->region = $region;
			$billboard->city = $city;
			$billboard->location = $location;
			$billboard->image = "uploads/inventory-images/{$filename}";

			$request->user()->billboards()->save($billboard);

			alert()->success('Inventory Added');

			//Notify
			$user = Auth::user()->first_name;
			User::where('id', 1)->first()->notify(new NewInventoryNotification($user));

			$userId = Auth::user()->id;
			$data = $request->all();

			Event::fire(new BillboardCreatedEvent($userId, $data));

			return redirect()->route('new-billboard');
		}

		public function updateBillboard(Request $request, string $id)
		{
			if (Auth::user()->role == 2) {
				return redirect()->route('user-dashboard');
			}

			$billboard = Billboard::findOrFail($id);

			$billboard->update($request->all());

			alert()->success('Inventory Updated');

			//Notify
			$user = Auth::user();
			
			User::where('id', 1)->first()->notify(new UpdateInventoryNotification($user->first_name, $billboard->creative_campaign));
			
			Event::fire(new BillboardUpdatedEvent($user->id, $billboard->id));

			return redirect()->route('billboards');
		}

		public function deleteBillboardImage(Request $request, string $id)
		{
			$billboard = Billboard::findOrFail($id);
			
			File::delete($billboard->image);
			
			$billboard->image = 'uploads/inventory-images/inventory_image_default.jpg';
			$billboard->update();
			
			alert()->success('Inventory Image Deleted');
			
			return redirect()->back();
		}

		public function updateBillboardImage(Request $request, string $id)
		{
			if ($request->hasFile('inventory_image')) {
				$image = $request->file('inventory_image');
				$filename = time() . '.' . $image->getClientOriginalExtension();
				Image::make($image)->save( public_path('uploads/inventory-images/' . $filename ) );

				$billboard = Billboard::findOrFail($id);
				$billboard->image = "uploads/inventory-images/{$filename}";
				$billboard->save();
			}
			
			alert()->success('Inventory Image Updated');

			return redirect()->back();
		}

		public function deleteBillboard(Request $request, string $id)
		{
			$billboard = Billboard::findOrFail($id);
			$user = Auth::user();

			Event::fire(new BillboardDeletedEvent($user->id, $billboard->id));
			
			$billboard->delete();
			
			alert()->success('Billboard deleted');
			
			return redirect()->route('billboards');
		}
	}