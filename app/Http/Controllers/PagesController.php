<?php

namespace App\Http\Controllers;

	use App\Portal;
	use Session;
	use App\Http\Requests;
	use Illuminate\Http\Request;
	use Illuminate\Http\Response;
	use Illuminate\Support\Facades\Auth;

class PagesController extends Controller
{
    //Public Pages

	public function getGetstarted() {

		if (Auth::check()) {
		    return redirect()->route('user-dashboard');
		}

		return view('getstarted');
	}

	public function getLaunch() {
		return view('launch');
	}

}
