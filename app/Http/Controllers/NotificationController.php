<?php

namespace App\Http\Controllers;

	use Alert;
    use Carbon;

	use App\User;

	use App\Http\Requests;
	use Illuminate\Http\Request;
	use Illuminate\Http\Response;
	use Illuminate\Support\Facades\Auth;
    use Illuminate\Notifications\Notifiable;


class NotificationController extends Controller {
   	//Notification Mark as read
    public function getNotfiyDemoRead($id) {
        
        $user = Auth::user();
        
        $notification = $user->notifications()->where('id', $id)->first();


        if($notification) {
            
            $notification->markAsRead();
            
            return redirect()->route('demo-list');
        }
    }

    public function getNotfiyQuotesRead($id) {
    	
    	$user = Auth::user();
        
        $notification = $user->notifications()->where('id', $id)->first();


        if($notification) {
            
            $notification->markAsRead();
            
            return redirect()->route('requested-quotes');
        }
    }

    public function getNotfiySendQuotesRead($id) {
        
        $user = Auth::user();
        
        $notification = $user->notifications()->where('id', $id)->first();

        if($notification) {
            
            $notification->markAsRead();
            
            return redirect()->route('quotations');
        }
    }

    public function getNotfiySendBookingMemoRead($id) {
        
        $user = Auth::user();
        
        $notification = $user->notifications()->where('id', $id)->first();

        if($notification) {
            
            $notification->markAsRead();
            
            return redirect()->route('booking-inbox');
        }
    }

    public function getNotfiyNewInventoryRead($id) {
        
        $user = Auth::user();
        
        $notification = $user->notifications()->where('id', $id)->first();

        if($notification) {
            
            $notification->markAsRead();
            
            // return redirect()->route('`inventory`-list');
            return redirect()->route('billboards');
        }
    }

    public function getNotfiyUpdateInventoryRead($id) {
        
        $user = Auth::user();
        
        $notification = $user->notifications()->where('id', $id)->first();

        if($notification) {
            
            $notification->markAsRead();
            
            // return redirect()->route('inventory-list');
            return redirect()->route('billboards');
        }
    }

    public function getNotfiySignupRead($id) {
        
        $user = Auth::user();
        
        $notification = $user->notifications()->where('id', $id)->first();

        if($notification) {
            
            $notification->markAsRead();
            
            return redirect()->route('user-registration');
        }
    }

    public function getNotifyReadAll() {
        
        $user = Auth::user();

        $user->unreadNotifications()->update(['read_at' => Carbon::now()]);

    }

    public function getNotifyDelete($id) {

       $user = Auth::user(); 
       
       $notification = $user->notifications()->find($id);

       $notification->delete();

       return redirect()->back();
    
    }
}
