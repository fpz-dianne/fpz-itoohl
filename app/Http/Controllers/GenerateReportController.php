<?php

namespace App\Http\Controllers;
	
	use Mail;
	use Session;
	use Alert;
	use Carbon;
	use DB;

	use App\User;
	use App\Portal;
	use App\industry;
	use App\format;
	use App\size;

	use App\Http\Requests;
	use Illuminate\Http\Request;
	use Illuminate\Http\Response;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\Input;

class GenerateReportController extends Controller {

	public function getGenerateReport() {
			
		$users = Auth::user();
		$formats = Format::all();
		$industries = Industry::all();
		$sizes = Size::all();
		
		return view('user.generate-report', compact('users', 'formats', 'industries', 'sizes'));
	}

	public function getGenerateReportCompare() {

		$users = Auth::user();
		$formats = Format::all();
		$industries = Industry::all();
		$sizes = Size::all();

		$products_1 = Portal::select('products', DB::raw('count(*) as total'))
	        ->groupBy('products')
	        ->get();

	    $products_2 = Portal::select('products', DB::raw('count(*) as total'))
	        ->groupBy('products')
	        ->get();    
		
		return view('user.generate-report-compare', compact('users', 'formats', 'industries', 'sizes', 'products_1', 'products_2'));
	}

	public function getGenerateReportSales() {

		$users = Auth::user();
		$formats = Format::all();
		$industries = Industry::all();
		$sizes = Size::all();

		$products_1 = Portal::select('products', DB::raw('count(*) as total'))
	        ->groupBy('products')
	        ->get();

	    $products_2 = Portal::select('products', DB::raw('count(*) as total'))
	        ->groupBy('products')
	        ->get();    
		
		return view('user.generate-report-sales', compact('users', 'formats', 'industries', 'sizes', 'products_1', 'products_2'));
	}

	public function getAjaxReportProductList1() {

		$user_1_id = Input::get('user_1_id');
		$user_2_id = Input::get('user_2_id');
	
		$products_1 = Portal::where('user_id', '=', $user_1_id)
		    ->select('products', DB::raw('count(*) as total'))
	        ->groupBy('products')
	        ->get();

	    $products_2 = Portal::where('user_id', '=', $user_2_id)
		    ->select('products', DB::raw('count(*) as total'))
	        ->groupBy('products')
	        ->get();    

	    return response()->json($products_1);
	}

	public function getAjaxReportProductList2() {

		$user_2_id = Input::get('user_2_id');	

	    $products_2 = Portal::where('user_id', '=', $user_2_id)
		    ->select('products', DB::raw('count(*) as total'))
	        ->groupBy('products')
	        ->get();    

	    return response()->json($products_2);
	}

	//Single Reports
	public function postGenerateReport(Request $request) {

		$formats = Format::all();
		$industries = Industry::all();
		$sizes = Size::all();
		$products = Portal::select('products', DB::raw('count(*) as total'))
        	->groupBy('products')
        	->get();

		$user_1 = $request['user_1'];
		$data_1 = $request['data_1'];
		$formats_1 = $request['formats_1'];
		$chart = $request['chart_type'];

		// foreach (array($formats_1) as $key) {
		//		dd($key);
		// }

		$industries_1 = $request['industry_1'];
		$products_1 = $request['products_1'];
		$sizes_1 = $request['sizes_1'];

		//check if selected user_1 is individual or all ooh inventory
		if ($user_1 == 'all_ooh') {
			$results_1_user = 'OOH Inventory';
		}
		else {
			$results_1_user = User::findOrFail($user_1);
		}




		/* ///////////////////
		Generate Single Data
		////////////////////*/
		if ($user_1 == 'all_ooh') {
			$results_1 = Portal::where(function ($query) use ($formats_1, $formats) {
				if ($formats_1[0] == 'all_format') {
					foreach($formats as $format) {
					 	$query->orwhere('format', '=', $format->name);
					}
				}
				else {
					foreach($formats_1 as $format) {
					 	$query->orwhere('format', '=', $format);
					}
				}
	        })
	        ->where(function ($query2) use ($industries_1, $industries) {
				if ($industries_1[0] == 'all_industry') {
					foreach($industries as $industry) {
					 	
					}
				}
				else {
					foreach($industries_1 as $industry) {
					 	$query2->orwhere('industry', '=', $industry);
					}
				}
	        })
	        ->where(function ($query3) use ($products_1, $products) {
	            if ($products_1[0] == 'all_products') {
					foreach($products as $product) {
					 	
					}
				}
				else {
					foreach($products_1 as $product) {
					 	$query3->orwhere('products', '=', $product);
					}
				}
	        })
	        ->where(function ($query4) use ($sizes_1, $sizes) {
	            if ($sizes_1[0] == 'all_size') {
					foreach($sizes as $size) {
					 	
					}
				}
				else {
					foreach($sizes_1 as $size) {
					 	$query4->orwhere('size', '=', $size);
					}
				}
	        })
			->select($data_1, DB::raw('count(*) as data_count'))
			->groupBy($data_1)
			->get();
		}
		else {
			$results_1 = Portal::where('user_id', '=', $user_1)
			->where(function ($query) use ($formats_1, $formats) {
				if ($formats_1[0] == 'all_format') {
					foreach($formats as $format) {
					 	
					}
				}
				else {
					foreach($formats_1 as $format) {
					 	$query->orwhere('format', '=', $format);
					}
				}
	        })
	        ->where(function ($query2) use ($industries_1, $industries) {
				if ($industries_1[0] == 'all_industry') {
					foreach($industries as $industry) {
					 	
					}
				}
				else {
					foreach($industries_1 as $industry) {
					 	$query2->orwhere('industry', '=', $industry);
					}
				}
	        })
	        ->where(function ($query3) use ($products_1, $products) {
	            if ($products_1[0] == 'all_products') {
					foreach($products as $product) {
					 	
					}
				}
				else {
					foreach($products_1 as $product) {
					 	$query3->orwhere('products', '=', $product);
					}
				}
	        })
	        ->where(function ($query4) use ($sizes_1, $sizes) {
	            if ($sizes_1[0] == 'all_size') {
					foreach($sizes as $size) {
					 	
					}
				}
				else {
					foreach($sizes_1 as $size) {
					 	$query4->orwhere('size', '=', $size);
					}
				}
	        })
			->select($data_1, DB::raw('count(*) as data_count'))
			->groupBy($data_1)
			->get();
		}

		//dd($results_1);

		/* ///////////////////
		total number of result for data_1
		////////////////////*/
		foreach ($results_1 as $result_1) {
			if ($user_1 == 'all_ooh') {
				$data_1_total = Portal::where(function ($query) use ($formats_1, $formats) {
				if ($formats_1[0] == 'all_format') {
					foreach($formats as $key) {
					 
					}
				}
				else {
					foreach($formats_1 as $key) {
					 	$query->orwhere('format', '=', $key);
					}
				}
	        })
	        ->where(function ($query2) use ($industries_1, $industries) {
				if ($industries_1[0] == 'all_industry') {
					foreach($industries as $industry) {
					 	
					}
				}
				else {
					foreach($industries_1 as $industry) {
					 	$query2->orwhere('industry', '=', $industry);
					}
				}
	        })
	        ->where(function ($query3) use ($products_1, $products) {
	            if ($products_1[0] == 'all_products') {
					foreach($products as $product) {
					 	
					}
				}
				else {
					foreach($products_1 as $product) {
					 	$query3->orwhere('products', '=', $product);
					}
				}
	        })
	        ->where(function ($query4) use ($sizes_1, $sizes) {
	            if ($sizes_1[0] == 'all_size') {
					foreach($sizes as $size) {
					 	
					}
				}
				else {
					foreach($sizes_1 as $size) {
					 	$query4->orwhere('size', '=', $size);
					}
				}
	        })
			->select($data_1, DB::raw('count(*) as data_count'))
			->count();
			}
			elseif ($formats_1[0] == 'all_format' &&  $industries_1[0] == 'all_industry' && $products_1[0] == 'all_products' && $sizes_1[0] == 'all_size' && $user_1 != 'all_ooh') {
				$data_1_total = Portal::where('user_id', '=', $user_1)->count();
			}
			else {
			$data_1_total = Portal::where('user_id', '=', $user_1)
			->where(function ($query) use ($formats_1, $formats) {
				if ($formats_1[0] == 'all_format') {
					foreach($formats as $key) {
					 
					}
				}
				else {
					foreach($formats_1 as $key) {
					 	$query->orwhere('format', '=', $key);
					}
				}
	        })
	        ->where(function ($query2) use ($industries_1, $industries) {
				if ($industries_1[0] == 'all_industry') {
					foreach($industries as $industry) {
					 	
					}
				}
				else {
					foreach($industries_1 as $industry) {
					 	$query2->orwhere('industry', '=', $industry);
					}
				}
	        })
	        ->where(function ($query3) use ($products_1, $products) {
	            if ($products_1[0] == 'all_products') {
					foreach($products as $product) {
					 	
					}
				}
				else {
					foreach($products_1 as $product) {
					 	$query3->orwhere('products', '=', $product);
					}
				}
	        })
	        ->where(function ($query4) use ($sizes_1, $sizes) {
	            if ($sizes_1[0] == 'all_size') {
					foreach($sizes as $size) {
					 	
					}
				}
				else {
					foreach($sizes_1 as $size) {
					 	$query4->orwhere('size', '=', $size);
					}
				}
	        })
			->select($data_1, DB::raw('count(*) as data_count'))
			->count();
			}
		}

		/* ///////////////////
		Check if data count is empty
		////////////////////*/
		if ( empty( $data_1_total ) ) {
    		$data_1_total = 0;
    	}

    	return view('user.generate-report-result', compact('results_1', 'data_1', 'results_1_user', 'data_1_total', 'chart'));		
	}

	//Compare Reports
	public function postGenerateReportCompare(Request $request) {

		$formats = Format::all();
		$industries = Industry::all();
		$sizes = Size::all();
		$products = Portal::select('products', DB::raw('count(*) as total'))
        ->groupBy('products')
        ->get();

        $chart = $request['chart_type'];

		$user_1 = $request['user_1'];
		$user_2 = $request['user_2'];
		$data_1 = $request['data_1'];
		$data_2 = $request['data_2'];

		$formats_1 = $request['formats_1'];
		$formats_2 = $request['formats_2'];

		//foreach (array($formats_1) as $key) {
		//	dd($key);
		//}

		$industries_1 = $request['industry_1'];
		$industries_2 = $request['industry_2'];

		$products_1 = $request['products_1'];
		$products_2 = $request['products_2'];

		$sizes_1 = $request['sizes_1'];
		$sizes_2 = $request['sizes_2'];

		//check if selected user_1 is individual or all ooh inventory
		if ($user_1 == 'all_ooh') {
			$results_1_user = 'OOH Inventory';
		}
		else {
			$results_1_user = User::findOrFail($user_1);
		}

		//check if selected user_2 is individual or all ooh inventory
		if ($user_2 == 'all_ooh') {
			$results_2_user = 'OOH Inventory';
		}
		else {
			$results_2_user = User::findOrFail($user_2);
		}

		/* ///////////////////
		Generate Data Left side
		////////////////////*/
		if ($user_1 == 'all_ooh') {
			$results_1 = Portal::where(function ($query) use ($formats_1, $formats) {
				if ($formats_1[0] == 'all_format') {
					foreach($formats as $format) {
					 	$query->orwhere('format', '=', $format->name);
					}
				}
				else {
					foreach($formats_1 as $format) {
					 	$query->orwhere('format', '=', $format);
					}
				}
	        })
	        ->where(function ($query2) use ($industries_1, $industries) {
				if ($industries_1[0] == 'all_industry') {
					foreach($industries as $industry) {
					 	
					}
				}
				else {
					foreach($industries_1 as $industry) {
					 	$query2->orwhere('industry', '=', $industry);
					}
				}
	        })
	        ->where(function ($query3) use ($products_1, $products) {
	            if ($products_1[0] == 'all_products') {
					foreach($products as $product) {
					 	
					}
				}
				else {
					foreach($products_1 as $product) {
					 	$query3->orwhere('products', '=', $product);
					}
				}
	        })
	        ->where(function ($query4) use ($sizes_1, $sizes) {
	            if ($sizes_1[0] == 'all_size') {
					foreach($sizes as $size) {
					 	
					}
				}
				else {
					foreach($sizes_1 as $size) {
					 	$query4->orwhere('size', '=', $size);
					}
				}
	        })
			->select($data_1, DB::raw('count(*) as data_count'))
			->groupBy($data_1)
			->get();
		}
		else {
			$results_1 = Portal::where('user_id', '=', $user_1)
			->where(function ($query) use ($formats_1, $formats) {
				if ($formats_1[0] == 'all_format') {
					foreach($formats as $format) {
					 	
					}
				}
				else {
					foreach($formats_1 as $format) {
					 	$query->orwhere('format', '=', $format);
					}
				}
	        })
	        ->where(function ($query2) use ($industries_1, $industries) {
				if ($industries_1[0] == 'all_industry') {
					foreach($industries as $industry) {
					 	
					}
				}
				else {
					foreach($industries_1 as $industry) {
					 	$query2->orwhere('industry', '=', $industry);
					}
				}
	        })
	        ->where(function ($query3) use ($products_1, $products) {
	            if ($products_1[0] == 'all_products') {
					foreach($products as $product) {
					 	
					}
				}
				else {
					foreach($products_1 as $product) {
					 	$query3->orwhere('products', '=', $product);
					}
				}
	        })
	        ->where(function ($query4) use ($sizes_1, $sizes) {
	            if ($sizes_1[0] == 'all_size') {
					foreach($sizes as $size) {
					 	
					}
				}
				else {
					foreach($sizes_1 as $size) {
					 	$query4->orwhere('size', '=', $size);
					}
				}
	        })
			->select($data_1, DB::raw('count(*) as data_count'))
			->groupBy($data_1)
			->get();
		}

		//dd($results_1);

		/* ///////////////////
		total number of result for data_1
		////////////////////*/
		foreach ($results_1 as $result_1) {
			if ($user_1 == 'all_ooh') {
				$data_1_total = Portal::where(function ($query) use ($formats_1, $formats) {
				if ($formats_1[0] == 'all_format') {
					foreach($formats as $key) {
					 
					}
				}
				else {
					foreach($formats_1 as $key) {
					 	$query->orwhere('format', '=', $key);
					}
				}
	        })
	        ->where(function ($query2) use ($industries_1, $industries) {
				if ($industries_1[0] == 'all_industry') {
					foreach($industries as $industry) {
					 	
					}
				}
				else {
					foreach($industries_1 as $industry) {
					 	$query2->orwhere('industry', '=', $industry);
					}
				}
	        })
	        ->where(function ($query3) use ($products_1, $products) {
	            if ($products_1[0] == 'all_products') {
					foreach($products as $product) {
					 	
					}
				}
				else {
					foreach($products_1 as $product) {
					 	$query3->orwhere('products', '=', $product);
					}
				}
	        })
	        ->where(function ($query4) use ($sizes_1, $sizes) {
	            if ($sizes_1[0] == 'all_size') {
					foreach($sizes as $size) {
					 	
					}
				}
				else {
					foreach($sizes_1 as $size) {
					 	$query4->orwhere('size', '=', $size);
					}
				}
	        })
			->select($data_1, DB::raw('count(*) as data_count'))
			->count();
			}
			elseif ($formats_1[0] == 'all_format' &&  $industries_1[0] == 'all_industry' && $products_1[0] == 'all_products' && $sizes_1[0] == 'all_size' && $user_1 != 'all_ooh') {
				$data_1_total = Portal::where('user_id', '=', $user_1)->count();
			}
			else {
			$data_1_total = Portal::where('user_id', '=', $user_1)
			->where(function ($query) use ($formats_1, $formats) {
				if ($formats_1[0] == 'all_format') {
					foreach($formats as $key) {
					 
					}
				}
				else {
					foreach($formats_1 as $key) {
					 	$query->orwhere('format', '=', $key);
					}
				}
	        })
	        ->where(function ($query2) use ($industries_1, $industries) {
				if ($industries_1[0] == 'all_industry') {
					foreach($industries as $industry) {
					 	
					}
				}
				else {
					foreach($industries_1 as $industry) {
					 	$query2->orwhere('industry', '=', $industry);
					}
				}
	        })
	        ->where(function ($query3) use ($products_1, $products) {
	            if ($products_1[0] == 'all_products') {
					foreach($products as $product) {
					 	
					}
				}
				else {
					foreach($products_1 as $product) {
					 	$query3->orwhere('products', '=', $product);
					}
				}
	        })
	        ->where(function ($query4) use ($sizes_1, $sizes) {
	            if ($sizes_1[0] == 'all_size') {
					foreach($sizes as $size) {
					 	
					}
				}
				else {
					foreach($sizes_1 as $size) {
					 	$query4->orwhere('size', '=', $size);
					}
				}
	        })
			->select($data_1, DB::raw('count(*) as data_count'))
			->count();
			}
		}

		/* ///////////////////
		Generate Data Right side
		////////////////////*/
		if ($user_2 == 'all_ooh') {
			$results_2 = Portal::where(function ($query) use ($formats_2, $formats) {
				if ($formats_2[0] == 'all_format') {
					foreach($formats as $key) {
					 
					}
				}
				else {
					foreach($formats_2 as $key) {
					 	$query->orwhere('format', '=', $key);
					}
				}
	        })
	        ->where(function ($query2) use ($industries_2, $industries) {
				if ($industries_2[0] == 'all_industry') {
					foreach($industries as $industry) {
					 	
					}
				}
				else {
					foreach($industries_2 as $industry) {
					 	$query2->orwhere('industry', '=', $industry);
					}
				}
	        })
	        ->where(function ($query3) use ($products_2, $products) {
	            if ($products_2[0] == 'all_products') {
					foreach($products as $product) {
					 	
					}
				}
				else {
					foreach($products_2 as $product) {
					 	$query3->orwhere('products', '=', $product);
					}
				}
	        })
	        ->where(function ($query4) use ($sizes_2, $sizes) {
	            if ($sizes_2[0] == 'all_size') {
					foreach($sizes as $size) {
					 	
					}
				}
				else {
					foreach($sizes_2 as $size) {
					 	$query4->orwhere('size', '=', $size);
					}
				}
	        })
			->select($data_2, DB::raw('count(*) as data_count'))
			->groupBy($data_2)
			->get();
		}
		else {
			$results_2 = Portal::where('user_id', '=', $user_2)
			->where(function ($query) use ($formats_2, $formats) {
				if ($formats_2[0] == 'all_format') {
					foreach($formats as $key) {
					 
					}
				}
				else {
					foreach($formats_2 as $key) {
					 	$query->orwhere('format', '=', $key);
					}
				}
	        })
	        ->where(function ($query2) use ($industries_2, $industries) {
				if ($industries_2[0] == 'all_industry') {
					foreach($industries as $industry) {
					 	
					}
				}
				else {
					foreach($industries_2 as $industry) {
					 	$query2->orwhere('industry', '=', $industry);
					}
				}
	        })
	        ->where(function ($query3) use ($products_2, $products) {
	            if ($products_2[0] == 'all_products') {
					foreach($products as $product) {
					 	
					}
				}
				else {
					foreach($products_2 as $product) {
					 	$query3->orwhere('products', '=', $product);
					}
				}
	        })
	        ->where(function ($query4) use ($sizes_2, $sizes) {
	            if ($sizes_2[0] == 'all_size') {
					foreach($sizes as $size) {
					 	
					}
				}
				else {
					foreach($sizes_2 as $size) {
					 	$query4->orwhere('size', '=', $size);
					}
				}
	        })
			->select($data_2, DB::raw('count(*) as data_count'))
			->groupBy($data_2)
			->get();
		}

		/* ///////////////////
		total number of result for data_2
		////////////////////*/
		foreach ($results_2 as $result_2) {
			if ($user_2 == 'all_ooh') {
				$data_2_total = Portal::where(function ($query) use ($formats_2, $formats) {
				if ($formats_2[0] == 'all_format') {
					foreach($formats as $key) {
					 
					}
				}
				else {
					foreach($formats_2 as $key) {
					 	$query->orwhere('format', '=', $key);
					}
				}
	        })
	        ->where(function ($query2) use ($industries_2, $industries) {
				if ($industries_2[0] == 'all_industry') {
					foreach($industries as $industry) {
					 	
					}
				}
				else {
					foreach($industries_2 as $industry) {
					 	$query2->orwhere('industry', '=', $industry);
					}
				}
	        })
	        ->where(function ($query3) use ($products_2, $products) {
	            if ($products_2[0] == 'all_products') {
					foreach($products as $product) {
					 	
					}
				}
				else {
					foreach($products_2 as $product) {
					 	$query3->orwhere('products', '=', $product);
					}
				}
	        })
	        ->where(function ($query4) use ($sizes_2, $sizes) {
	            if ($sizes_2[0] == 'all_size') {
					foreach($sizes as $size) {
					 	
					}
				}
				else {
					foreach($sizes_2 as $size) {
					 	$query4->orwhere('size', '=', $size);
					}
				}
	        })
			->select($data_2, DB::raw('count(*) as data_count'))
			->count();
			}
			elseif ($formats_2[0] == 'all_format' &&  $industries_2[0] == 'all_industry' && $products_2[0] == 'all_products' && $sizes_2[0] == 'all_size' && $user_2 != 'all_ooh') {
				$data_2_total = Portal::where('user_id', '=', $user_2)->count();
			}
			else {
			$data_2_total = Portal::where('user_id', '=', $user_2)
			->where(function ($query) use ($formats_2, $formats) {
				if ($formats_2[0] == 'all_format') {
					foreach($formats as $key) {
					 
					}
				}
				else {
					foreach($formats_2 as $key) {
					 	$query->orwhere('format', '=', $key);
					}
				}
	        })
	        ->where(function ($query2) use ($industries_2, $industries) {
				if ($industries_2[0] == 'all_industry') {
					foreach($industries as $industry) {
					 	
					}
				}
				else {
					foreach($industries_2 as $industry) {
					 	$query2->orwhere('industry', '=', $industry);
					}
				}
	        })
	        ->where(function ($query3) use ($products_2, $products) {
	            if ($products_2[0] == 'all_products') {
					foreach($products as $product) {
					 	
					}
				}
				else {
					foreach($products_2 as $product) {
					 	$query3->orwhere('products', '=', $product);
					}
				}
	        })
	        ->where(function ($query4) use ($sizes_2, $sizes) {
	            if ($sizes_2[0] == 'all_size') {
					foreach($sizes as $size) {
					 	
					}
				}
				else {
					foreach($sizes_2 as $size) {
					 	$query4->orwhere('size', '=', $size);
					}
				}
	        })
			->select($data_2, DB::raw('count(*) as data_count'))
			->count();
			}
		}

		//dd($results_2);

		/* ///////////////////
		Check if data count is empty
		////////////////////*/
		if ( empty( $data_1_total ) ) {
    		$data_1_total = 0;
    	}

    	if ( empty( $data_2_total ) ) {
    		$data_2_total = 0;
    	}

		return view('user.generate-report-compare-result', compact('results_1', 'results_2', 'data_1', 'data_2', 'results_1_user', 'results_2_user', 'data_1_total', 'data_2_total', 'chart'));
	}
  
}
