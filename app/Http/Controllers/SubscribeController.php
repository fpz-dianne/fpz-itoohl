<?php

namespace App\Http\Controllers;
	
	use Mail;
	use Alert;
	use App\subscriber;
	use App\requestdemo;

	use App\Http\Requests;
	use Illuminate\Http\Request;
	use Illuminate\Http\Response;

class SubscribeController extends Controller
{
    public function postSubscribe(Request $request) {
    	
    	$email = $request['email'];

    	$subscriber = new requestdemo();
    	$subscriber->email = $email;

    	$subscriber->save();

    	alert()->success('Thank you for your interest in iTOOhL. We will get in contact with you as soon as possible.')->persistent('Confirm');

    	return redirect()->route('home');

    }

    //Subscriber List
    public function getSubscriberList() {

        $subscribers = Subscriber::all();

        return view('user.subscriber', compact('subscribers'));
    }

    //Subscriber Delete
    public function getSubscriberDelete($id) {

        $subscriber = Subscriber::findOrFail($id);

        $subscriber->delete();

        alert()->success('Subscriber Deleted');

        return redirect()->route('subscriber-list');
    }
}
