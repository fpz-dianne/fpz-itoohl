<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BillboardUpdatedEvent extends Event
{
    use SerializesModels;

    public $userId;

    public $billboardId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userId, $billboardId)
    {
        $this->userId = $userId;
        $this->billboardId = $billboardId;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
