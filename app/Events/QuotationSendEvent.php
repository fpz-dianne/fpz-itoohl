<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class QuotationSendEvent extends Event
{
    use SerializesModels;

    public $userId;
    public $data;
    public $request_quoteId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userId, $data, $request_quoteId)
    {
        $this->userId = $userId;
        $this->data = $data;
        $this->request_quoteId = $request_quoteId;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
