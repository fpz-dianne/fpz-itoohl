<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BillboardCreatedEvent extends Event
{
    use SerializesModels;

    public $userId;

    public $billboard;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userId, $billboard)
    {
        $this->userId = $userId;
        $this->billboard = $billboard;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
