<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserChangeRoleEvent extends Event
{
    use SerializesModels;

    public $userId;
    public $role;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userId, $role)
    {
        $this->userId = $userId;
        $this->role = $role;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
