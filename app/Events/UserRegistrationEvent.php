<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserRegistrationEvent extends Event
{
    use SerializesModels;

    public $email;
    public $first_name;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($email, $first_name)
    {
        $this->email = $email;
        $this->first_name = $first_name;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
