<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
        'App\Events\UpdateInventoryEvent' => [
            'App\Listeners\UpdateInventoryListener',
        ],
        'App\Events\UserProfileUpdateEvent' => [
            'App\Listeners\UserProfileUpdateListener',
        ],
        'App\Events\InventoryDeleteEvent' => [
            'App\Listeners\InventoryDeleteListener',
        ],
        'App\Events\QuotationRequestEvent' => [
            'App\Listeners\QuotationRequestListener',
        ],
        'App\Events\QuotationSendEvent' => [
            'App\Listeners\QuotationSendListener',
        ],
        'App\Events\BookingMemoSendEvent' => [
            'App\Listeners\BookingMemoSendListener',
        ],
        'App\Events\InventoryAddEvent' => [
            'App\Listeners\InventoryAddListener',
        ],
        'App\Events\BookingMemoDeleteEvent' => [
            'App\Listeners\BookingMemoDeleteListener',
        ],
        'App\Events\UserRegistrationEvent' => [
            'App\Listeners\UserRegistrationListener',
        ],
        'App\Events\UserRegistrationAllowedEvent' => [
            'App\Listeners\UserRegistrationAllowedListener',
        ],
        'App\Events\UserChangeRoleEvent' => [
            'App\Listeners\UserChangeRoleListener',
        ],
        'App\Events\UserRegistrationDeniedEvent' => [
            'App\Listeners\UserRegistrationDeniedListener',
        ],
        'App\Events\UserRegistrationDeleteEvent' => [
            'App\Listeners\UserRegistrationDeleteListener',
        ],
        'App\Events\ChangePasswordEvent' => [
            'App\Listeners\ChangePasswordListener',
        ],
        'App\Events\ChangeEmailEvent' => [
            'App\Listeners\ChangeEmailListener',
        ],
        'App\Events\BillboardCreatedEvent' => [
            'App\Listeners\BillboardCreatedListener',
        ],
        'App\Events\BillboardUpdatedEvent' => [
            'App\Listeners\BillboardUpdatedListener',
        ],
        'App\Events\BillboardDeletedEvent' => [
            'App\Listeners\BillboardDeletedListener',
        ]
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
