<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DemoOption extends Model {

    protected $fillable = array('name');

	public function requestDemos() {
        return $this->hasMany('App\requestdemo');
    }
}
