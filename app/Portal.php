<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portal extends Model
{
	 protected $fillable = array('user_id', 'name', 'supplier', 'country', 'city', 'street_address', 'latitude', 'longitude', 'format', 'products', 'industry', 'landmark', 'size', 'illumination', 'availability', 'gender', 'economic_class', 'age', 'payment_terms', 'rates', 'created_at', 'updated_at', 'inventory_image', 'company');

    public function user() {
    	return $this->belongsTo('App\User');
    }

    public function bookmark() {
        return $this->hasMany('App\bookmark');
    }
}
