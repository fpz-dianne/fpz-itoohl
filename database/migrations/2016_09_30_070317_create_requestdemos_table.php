<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestdemosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requestdemos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('DemoOption_id');
            $table->integer('DemoOption2_id');
            $table->string('name');
            $table->string('email');
            $table->string('company');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('requestdemos');
    }
}
