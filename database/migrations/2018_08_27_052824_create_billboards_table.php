<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillboardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billboards', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date_of_collection');
            $table->string('highway');
            $table->string('product');
            $table->string('creative_campaign');
            $table->string('structure');
            $table->string('size');
            $table->double('cost');
            $table->string('region');
            $table->string('city');
            $table->string('location');
            $table->string('image')->default('uploads/inventory-images/inventory_image_default.jpg');
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('billboards');
    }
}
