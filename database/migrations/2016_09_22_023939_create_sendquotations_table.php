<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendquotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sendquotations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('user_email');
            $table->string('user_first_name');
            $table->integer('receiver_id');
            $table->string('receiver_email');
            $table->string('receiver_name');
            $table->string('name');
            $table->string('inventory_img');
            $table->string('placement_duration');
            $table->string('material_changes');
            $table->string('material_changes_date');
            $table->string('production');
            $table->string('instruction');
            $table->string('quotation');
            $table->string('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sendquotations');
    }
}
