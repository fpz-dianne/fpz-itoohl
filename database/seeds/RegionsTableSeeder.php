<?php

use Illuminate\Database\Seeder;
use App\Region;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = [
            'Metro Manila',
            'Ilocos Region',
            'Cordillera Administrative Region',
            'Cagayan Valley',
            'Central Luzon',
            'CALABARZON',
            'MIMAROPA',
            'Bicol Region',
            'Western Visayas',
            'Central Visayas',
            'Eastern Visayas',
            'Zamboanga Peninsula',
            'Northern Mindanao',
            'Davao Region',
            'SOCCSKSARGEN',
            'Caraga Region',
            'Autonomous Region in Muslim Mindanao'
        ];

        foreach ($regions as $region) {
            (new Region(['name' => $region]))->save();
        }
    }
}
