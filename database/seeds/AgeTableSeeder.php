<?php

use Illuminate\Database\Seeder;

class AgeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $age = new \App\age([
            'name' => '18-24',
        ]);
        $age->save();

        $age = new \App\age([
            'name' => '25-34',
        ]);
        $age->save();

        $age = new \App\age([
            'name' => '35-44',
        ]);
        $age->save();

        $age = new \App\age([
            'name' => '45-54',
        ]);
        $age->save();

        $age = new \App\age([
            'name' => '55-64',
        ]);
        $age->save();

        $age = new \App\age([
            'name' => '65+',
        ]);
        $age->save();
    }
}
