<?php

use Illuminate\Database\Seeder;

class SizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$size = new \App\size([
        	'name' => '15 x 60',
        ]);
        $size->save();

        $size = new \App\size([
        	'name' => '15 x 256',
        ]);
        $size->save();

        $size = new \App\size([
        	'name' => '15 x 256',
        ]);
        $size->save();

        $size = new \App\size([
        	'name' => '20 x 19',
        ]);
        $size->save();

        $size = new \App\size([
        	'name' => '20 x 30',
        ]);
        $size->save();

        $size = new \App\size([
        	'name' => '20 x 58',
        ]);
        $size->save();

        $size = new \App\size([
        	'name' => '20 x 119',
        ]);
        $size->save();

        $size = new \App\size([
        	'name' => '40 x 39',
        ]);
        $size->save();

        $size = new \App\size([
        	'name' => '40 x 50',
        ]);
        $size->save();

        $size = new \App\size([
        	'name' => '40 x 60',
        ]);
        $size->save();

        $size = new \App\size([
        	'name' => '98 x 38',
        ]);
        $size->save();

        $size = new \App\size([
        	'name' => '120 x 50',
        ]);
        $size->save();

    }
}
