<?php

use Illuminate\Database\Seeder;

class GenderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gender = new \App\gender([
            'name' => 'Male(70%)-Female(30%)',
        ]);
        $gender->save();

        $gender = new \App\gender([
            'name' => 'Male(30%)-Female(70%)',
        ]);
        $gender->save();
    }
}
