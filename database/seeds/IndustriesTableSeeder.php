<?php

use Illuminate\Database\Seeder;

class IndustriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $industry = new \App\industry([
            'name' => 'Apparel',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Appliances',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Automobile Accessories',
        ]);
        $industry->save();

        $industry = new \App\industry([
        	'name' => 'Beverage',
        ]);
        $industry->save();

        $industry = new \App\industry([
        	'name' => 'BPO',
        ]);
        $industry->save();

        $industry = new \App\industry([
        	'name' => 'Cleaning',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Digital',
        ]);
        $industry->save();

        $industry = new \App\industry([
        	'name' => 'Electronic Hardware',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Entertainment',
        ]);
        $industry->save();

         $industry = new \App\industry([
            'name' => 'Estate',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Event',
        ]);
        $industry->save();

        $industry = new \App\industry([
        	'name' => 'Financial',
        ]);
        $industry->save();

        $industry = new \App\industry([
        	'name' => 'Food',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Furnitures',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Gadgets',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Hardware',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'House Hold',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Mall',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Monitoring',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Motoring',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'NSP (Network Service Provider)',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Office Equipment',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Pawnshop',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Personal Care',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Pharmaceuticals',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'QSR (quick service restaurant)',
        ]);
        $industry->save();

        $industry = new \App\industry([
        	'name' => 'Real Estate',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Restaurant',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Services',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Store',
        ]);
        $industry->save();

        $industry = new \App\industry([
            'name' => 'Toys',
        ]);
        $industry->save();

        $industry = new \App\industry([
        	'name' => 'Others',
        ]);
        $industry->save();
    }
}
