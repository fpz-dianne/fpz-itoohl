<?php

use Illuminate\Database\Seeder;

class Demo_Option2TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $demo_option = new \App\DemoOption2([
        	'name' => 'Search Functionality',
        ]);
        $demo_option->save();

        $demo_option = new \App\DemoOption2([
        	'name' => 'Media Planning',
        ]);
        $demo_option->save();

        $demo_option = new \App\DemoOption2([
        	'name' => 'Research, Insights and Report Generation',
        ]);
        $demo_option->save();

        $demo_option = new \App\DemoOption2([
        	'name' => 'All of the above',
        ]);
        $demo_option->save();
    }
}
