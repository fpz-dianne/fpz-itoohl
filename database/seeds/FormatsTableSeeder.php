<?php

use Illuminate\Database\Seeder;

class FormatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $format = new \App\format([
            'name' => 'Board Up',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Building LED Curtain',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Building Sticker Wrap',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Bus Shed Ads',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'LED Board',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'LED Billboard',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'LED Board Gantry',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'LRT Central Pillar',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Market Privilege Panel',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'MRT Center Billboard',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'MRT Center Copin Banner',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'MRT Center Pillar',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'MRT Foxy Banner',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'MRT Vertical Banner',
        ]);
        $format->save();

        $format = new \App\format([
        	'name' => 'Static Billboard',
        ]);
        $format->save();

        $format = new \App\format([
        	'name' => 'Static Billboard W/ LED Board',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Static Billboard / Board Up',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Static Billboard / Building Wrap',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Static Billboard / LRT Rail Billboard',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Street Billboard',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Static Billboard / Mall External',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Street Billboard (BLDG Wrap)',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Street Billboard / Building Wrap',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Street Billboard / LRT Rail Billboard',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Wall Murals',
        ]);
        $format->save();

        /*Ambient

        $format = new \App\format([
            'name' => 'LCD',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Malls',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Walkways / Overpass',
        ]);
        $format->save();

        //Transit Ads

        $format = new \App\format([
            'name' => 'Airports',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Bus Rears',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Jeep Top Ads',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'LRT',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'MRT',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Seaports',
        ]);
        $format->save();

        $format = new \App\format([
            'name' => 'Taxi Ads',
        ]);
        $format->save();
        */
    }
}
