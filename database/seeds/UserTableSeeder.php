<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $user = new \App\User([
       		'first_name' => 'admin',
       		'last_name' => 'Itoohl',
       		'email' => 'admin@itoohl.com',
       		'password' => bcrypt('admin123'),
       		'role' => 1,
       		'status' => 1,
       ]);
       $user->save();

      $user = new \App\User([
          'first_name' => 'United',
          'last_name' => 'Neon',
          'email' => 'united_neon@itoohl.com',
          'password' => bcrypt('123'),
          'role' => 3,
          'status' => 1,
       ]);
       $user->save();

       $user = new \App\User([
          'first_name' => 'robert',
          'last_name' => 'cruda',
          'email' => 'robert@fourptzero.com',
          'password' => bcrypt('123'),
          'role' => 1,
          'status' => 1,
       ]);
       $user->save();
	   
	   $user = new \App\User([
       		'first_name' => 'Miko',
       		'last_name' => 'Raymundo',
       		'email' => 'meecoyna@gmail.com',
       		'password' => bcrypt('123'),
       		'role' => 1,
       		'status' => 1,
       ]);
       $user->save();

       $user = new \App\User([
          'first_name' => 'Demo',
          'last_name' => 'Itoohl',
          'email' => 'demo@itoohl.com',
          'password' => bcrypt('123'),
          'role' => 2,
          'status' => 1,
       ]);
       $user->save();

       $user = new \App\User([
          'first_name' => 'Adcity',
          'last_name' => 'Demo',
          'email' => 'adcity@itoohl.com',
          'password' => bcrypt('adcity'),
          'role' => 3,
          'status' => 1,
       ]);
       $user->save();

       $user = new \App\User([
          'first_name' => 'smart',
          'last_name' => 'telecommunication',
          'email' => 'smart@itoohl.com',
          'password' => bcrypt('smart123'),
          'role' => 2,
          'status' => 1,
       ]);
       $user->save();
    }
}
