<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new \App\role([
        	'name' => 'Admin',
        ]);
        $role->save();

        $role = new \App\role([
        	'name' => 'Advertiser',
        ]);
        $role->save();

        $role = new \App\role([
        	'name' => 'Vendor',
        ]);
        $role->save();
    }
}
