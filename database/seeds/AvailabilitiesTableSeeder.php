<?php

use Illuminate\Database\Seeder;

class AvailabilitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $availability = new \App\availability([
        	'name' => 'Yes',
        ]);
        $availability->save();
        
        $availability = new \App\availability([
        	'name' => 'No',
        ]);
        $availability->save();
    }
}
