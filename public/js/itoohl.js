

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
})

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});



// Toggle footer item
$('#map.footer-treeview').on('click', function(event){
        $('#map.footer-treeview-menu').toggleClass('is-visible');
});
$('#inventory.footer-treeview').on('click', function(event){
        $('#inventory.footer-treeview-menu').toggleClass('is-visible');
});
$('#media.footer-treeview').on('click', function(event){
        $('#media.footer-treeview-menu').toggleClass('is-visible');
});
$('#update.footer-treeview').on('click', function(event){
        $('#update.footer-treeview-menu').toggleClass('is-visible');
});
$('#vendor.footer-treeview').on('click', function(event){
        $('#vendor.footer-treeview-menu').toggleClass('is-visible');
});
$('#ooh.footer-treeview').on('click', function(event){
        $('#ooh.footer-treeview-menu').toggleClass('is-visible');
});

$('.feature-block').on('mouseover', function(){
	   $(this).closest('.feature').find('.monitor-img .clip .bg').attr('style', $(this).find('.clip-hide').attr('style'));
		$(this).closest('.feature').find('.monitor-img').addClass('scal-img');
});

// On Scroll Animation Effect
wow = new WOW({ 
    animateClass: 'animated',
    offset: 75,
    mobile: false
});

wow.init();