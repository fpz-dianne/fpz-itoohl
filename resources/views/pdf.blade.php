<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href='http://fonts.googleapis.com/css?family=Lora' rel='stylesheet' type='text/css' media="all">
	<link href="{{ URL::to('css/bootstrap.min.css')}}" rel="stylesheet"  media="all"><!-- Bootstrap Core CSS -->
	<link rel="stylesheet" href="{{ URL::to('css/reset.css') }}" media="all"> <!-- CSS reset -->
	<link rel="stylesheet" href="{{ URL::to('css/portal.css') }}" media="all"> <!-- Resource style -->
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css" media="all">
	<!-- Sweet Alert Style -->
 	<link rel="stylesheet" href="{{URL::to('plugins/sweet-alert/sweetalert.css')}}" media="all">
 	<!-- daterange picker -->
 	<link rel="stylesheet" href="{{URL::to('plugins/daterangepicker/daterangepicker.css')}}" media="all">
 	<!-- daterange picker -->
 	<link rel="stylesheet" href="{{URL::to('plugins/datepicker/datepicker3.css')}}" media="all">

	<script src="{{ URL::to('js/modernizr.min.js') }}"></script> <!-- Modernizr -->

	<title>Portal</title>
</head>
<body>	
	<section id="google-map">		
		<div id="google-container"></div>
		<div id="circle"></div>
		<div id="traffic"></div>
		<div id="zoom-in"></div>
		<div id="zoom-out"></div>
	</section>

	<div class="filter filter-is-visible">
		<div class="filter-title">
			<a href="{{ URL::route('user-dashboard') }}">Itoohl</a>
		</div>
		<form>
			<div class="filter-block">

				<div class="filter-content">
					<input type="search" placeholder="Search..."  id="search">
				</div> <!-- filter-content -->
			</div> <!-- filter-block -->

			<div class="filter-block">
			<h4>Product</h4>

				<ul class="filter-content filters list" style="display: none;">
					<li>
						<input class="filter" data-filter=".apparel" type="checkbox" id="apparel" >
						<label class="checkbox-label" for="apparel">Apparel</label>
					</li>

					<li>
						<input class="filter" data-filter=".beverage" type="checkbox" id="beverage" >
						<label class="checkbox-label" for="beverage">Beverage</label>
					</li>

					<li>
						<input class="filter" data-filter=".bpo" type="checkbox" id="bpo" >
						<label class="checkbox-label" for="bpo">BPO</label>
					</li>

					<li>
						<input class="filter" data-filter=".cleaning" type="checkbox" id="cleaning" >
						<label class="checkbox-label" for="cleaning">Cleaning</label>
					</li>

					<li>
						<input class="filter" data-filter=".electronic" type="checkbox" id="electronic" >
						<label class="checkbox-label" for="electronic">Electronic Hardware</label>
					</li>

					<li>
						<input class="filter" data-filter=".entertainment" type="checkbox" id="entertainment" >
						<label class="checkbox-label" for="entertainment" >Entertainment</label>
					</li>

					<li>
						<input class="filter" data-filter=".finance" type="checkbox" id="finance" >
						<label class="checkbox-label" for="finance">Finance</label>
					</li>

					<li>
						<input class="filter" data-filter=".food" type="checkbox" id="food" >
						<label class="checkbox-label" for="food">Food</label>
					</li>

					<li>
						<input class="filter" data-filter=".realestate" type="checkbox" id="realestate" >
						<label class="checkbox-label" for="realestate">Real Estate</label>
					</li>

					<li>
						<input class="filter" data-filter=".personalcare" type="checkbox" id="personalcare" >
						<label class="checkbox-label" for="personalcare">Personal Care</label>
					</li>

					<li>
						<input class="filter" data-filter=".qsr" type="checkbox" id="qsr" >
						<label class="checkbox-label" for="qsr">QSR (quick service restaurant)</label>
					</li>

					<li>
						<input class="filter" data-filter=".nsp" type="checkbox" id="nsp" >
						<label class="checkbox-label" for="nsp">NSP (Network Service Provider)</label>
					</li>

					<li>
						<input class="filter" data-filter=".monitoring" type="checkbox" id="monitoring" >
						<label class="checkbox-label" for="monitoring">Monitoring</label>
					</li>

					<li>
						<input class="filter" data-filter=".mall" type="checkbox" id="mall" >
						<label class="checkbox-label" for="mall">Malls</label>
					</li>

					<li>
						<input class="filter" data-filter=".pharmaceuticals" type="checkbox" id="pharmaceuticals" >
						<label class="checkbox-label" for="pharmaceuticals">Pharmaceuticals</label>
					</li>

					<li>
						<input class="filter" data-filter=".others" type="checkbox" id="others" >
						<label class="checkbox-label" for="others">Others</label>
					</li>
				</ul><!-- filter-content -->
			</div>		

			<div class="filter-block">
				<h4>Format</h4>

				<ul class="filter-content filters list" style="display: none;">
					<li style="margin-left: 1em;">
						<h4>Ambient</h4>

						<ul class="filter-content filters list" style="display: none;">
							<li>
								<input class="filter" data-filter=".lcd" type="checkbox" id="lcd" >
								<label class="checkbox-label" for="lcd">LCD's</label>
							</li>
							<li>
								<input class="filter" data-filter=".malls" type="checkbox" id="malls" >
								<label class="checkbox-label" for="malls">Malls</label>
							</li>
							<li>
								<input class="filter" data-filter=".walkways" type="checkbox" id="walkways" >
								<label class="checkbox-label" for="walkways">Walkways / Overpass</label>
							</li>
						</ul>
					</li>
					<li>
						<input class="filter" data-filter=".ledbillboards" type="checkbox" id="ledbillboards" >
						<label class="checkbox-label" for="ledbillboards">LED Billboards</label>
					</li>
					<li>
						<input class="filter" data-filter=".staticbillboards" type="checkbox" id="staticbillboards" >
						<label class="checkbox-label" for="staticbillboards">Static Billboards</label>
					</li>
					<li>
						<input class="filter" data-filter=".streetbillboards" type="checkbox" id="streetbillboards" >
						<label class="checkbox-label" for="streetbillboards">Street Billboards</label>
					</li>
					<li style="margin-left: 1em; padding-top: 1em;">
						<h4>Transit Ads</h4>
						<ul class="filter-content filters list" style="display: none;">
							<li>
								<input class="filter" data-filter=".airports" type="checkbox" id="airports" >
								<label class="checkbox-label" for="airports">Airports</label>
							</li>
							<li>
								<input class="filter" data-filter=".busrears" type="checkbox" id="busrears" >
								<label class="checkbox-label" for="busrears">Bus Rears</label>
							</li>
							<li>
								<input class="filter" data-filter=".jeep" type="checkbox" id="jeep" >
								<label class="checkbox-label" for="jeep">Jeep Top Ads</label>
							</li>
							<li>
								<input class="filter" data-filter=".lrt" type="checkbox" id="lrt" >
								<label class="checkbox-label" for="lrt">LRT</label>
							</li>
							<li>
								<input class="filter" data-filter=".mrt" type="checkbox" id="mrt" >
								<label class="checkbox-label" for="mrt">MRT</label>
							</li>
							<li>
								<input class="filter" data-filter=".seaports" type="checkbox" id="seaports" >
								<label class="checkbox-label" for="seaports">Seaports</label>
							</li>
							<li>
								<input class="filter" data-filter=".taxi" type="checkbox" id="taxi" >
								<label class="checkbox-label" for="taxi">Taxi Ads</label>
							</li>
						</ul>
					</li>
					<li>
						<input class="filter" data-filter=".wallmurals" type="checkbox" id="wallmurals" >
						<label class="checkbox-label" for="wallmurals">Wall Murals</label>
					</li>
				</ul> <!-- filter-content -->
			</div> <!-- filter-block -->
			<div class="filter-block">
				<h4>Availability</h4>
				<ul class="filter-content filters list" style="display: none;">
					<li>
						<input class="filter" data-filter=".yes" type="checkbox" name="yes" id="yes" >
						<label class="checkbox-label" for="yes">Yes</label>
					</li>
					<li>
						<input class="filter" data-filter=".no" type="checkbox" id="no" >
						<label class="checkbox-label" for="no">No</label>
					</li>
				</ul><!-- filter-content -->
			</div> <!-- filter-block -->
			<div class="filter-block">
				<h4>Location</h4>
				<ul class="filter-content filters list" style="display: none;">
					<li>
						<input class="filter" data-filter=".nlex" type="checkbox" id="nlex" >
						<label class="checkbox-label" for="nlex">NLEX</label>
					</li>
					<li>
						<input class="filter" data-filter=".slex" type="checkbox" id="slex" >
						<label class="checkbox-label" for="slex">SLEX</label>
					</li>
					<li>
						<input class="filter" data-filter=".edsa" type="checkbox" id="edsa" >
						<label class="checkbox-label" for="edsa">EDSA</label>
					</li>
					<li>
						<input class="filter" data-filter=".c5" type="checkbox" id="c5" >
						<label class="checkbox-label" for="c5">C5</label>
					</li>
				</ul><!-- filter-content -->
			</div> <!-- filter-block -->
		</form>
		<div class="col-md-12 side-bar-container">
			<div class="side-bar-box">
				<ul id="markers">	
				</ul>
			</div>
		</div>
	</div> <!-- filter -->
	<a href="#0" class="filter-trigger"></a>
	<!-- Inventory Single Item -->
	<div class="inventory">
		<div class="row">
			<div class="col-md-12">
				<div class="inventory-box">
					<div class="inventory-header">
						<a href="#0" class="close">
							<i class="fa fa-chevron-left"></i
							><span> Back</span>
						</a>
					</div>
					<div class="inventory-body">
						<img class="inventory-image" src="img/inventory_image_default.jpg">
						<video  class="inventory-image" autoplay="autoplay" controls="false" loop>
							<source src="" type="video/mp4" />
						</video>
					</div>
					<div class="inventory-footer">
						<a href="#" id="name"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="inventory-content">
					<li><b>Size (H x W):</b> <span id="size"></span></li>
					<li><b>Availability:</b> RC Cola</li>
					<li><b>Industry:</b> <span id="industry"></span></li>
					<li><b>Illumination:</b> Yes</li>
					<li><b>Traffic Count/Day:</b> 25,000</li>
					<li><b>Monthly Rate:</b> 100,000</li>
					<li><b>Payment Terms:</b> 3 months advance</li>
				</div>
				<div class="inventory-content-footer">
					<a href="#" data-toggle="modal" data-target="#request-quotes">
						<li>
							<i class="fa fa-wechat"></i>
							<span>Request a Quote</span>	
						</li>
					</a>
					<a href="#" onclick="myFunction()">
						<li>
							<i  class="fa fa-print"></i>
							<span>Print</span>	
						</li>
					</a>
					<a href="pdf">
						<li>
							<i  class="fa fa-file-pdf-o"></i>
							<span>Download PDF</span>	
						</li>
					</a>
				</div>
			</div>
		</div>
	</div> <!-- filter -->
	<!-- Modal -->
	@include('includes.quotes-modal')

	<script src="{{ URL::to('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdv73isDC-89aOdpnF9VPzSpQW1ukI3ts&libraries=visualization,places"></script>
    <script src="{{ URL::to('js/bootstrap.min.js') }}"></script> <!-- Bootstrap Core JavaScript -->
	<script src="{{ URL::to('js/portal.min.js')}}"></script> <!-- Resource jQuery -->
	
	<!-- Sweet Alert -->
	<script src="{{ URL::to('plugins/sweet-alert/sweetalert.min.js') }}"></script>
	@include('sweet::alert')
	<!-- date-range-picker -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
	<script src="{{ URL::to('plugins/daterangepicker/daterangepicker.js')}}"></script>
	<!-- bootstrap datepicker -->
	<script src="{{ URL::to('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
		<!-- Page script -->
	<!-- Slimscroll -->
	<script src="{{ URL::to('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>	
	<script>
	  $(function () {
	 

	    //Date range picker
	    $('#reservation').daterangepicker();


		//Date picker
	    $('#material_changes').datepicker({
	      autoclose: true
	    });


	  });

</script>

<script>
//portal print pdf
	
function myFunction() {
    window.print();
}


</script>
	
</body>
</html>