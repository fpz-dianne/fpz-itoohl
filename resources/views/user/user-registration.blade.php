@extends('layouts.user-dashboard-layout')

@section('title')
  iTOOhL | User Registration
@endsection

@section('style')
 <!-- DataTables -->
  <link rel="stylesheet" href="{{ URL::to('plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('dashboard-title')
  User Registration
@endsection

@section('content')
     <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="registration-list" class="table table-bordered table-hover">
                <thead>
                <tr class="bg-blue">
                  <th>Name</th>
                  <th>Email</th>
                  <th>Role</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach( $users as $user )
                      <tr>
                        <td>
                          <a href="{{ URL::to('profile/' . $user->id  ) }}">{{ $user->first_name }} {{ $user->last_name }}</a>
                        </td>
                        <td>{{ $user->email }}</td>
                        <td>
                          <div class="form-group">
                            <form action="{{ URL::to('allow-user-registration/'. $user->id ) }}" method="post">
                            <select class="form-control select2 role" name="role" style="width: 150px !important;">
                              @if( $user->role == 1 )
                                <option selected disabled hidden value="{{$user->role}}" >
                                  Admin
                                </option>
                                @elseif( $user->role == 2 )
                                 <option selected disabled hidden value="{{$user->role}}" >
                                  Advertiser
                                </option>
                                @else
                                <option selected disabled hidden value="{{$user->role}}" >
                                  Vendor
                                </option>
                              @endif
                              <option value="1">Admin</option>
                              <option value="2">Advertiser</option>
                              <option value="3">Vendor</option>
                            </select>
                          </div>
                        </td>
                        <td>
                          @if( $user->status == 1 )
                             <span class="label label-primary">Approved</span>
                             @elseif( $user->status == 2 )
                             <span class="label label-warning">Pending</span>
                             @elseif( $user->status == 3 )
                             <span class="label label-danger">Denied</span>
                          @endif
                        </td>
                        <td class="text-center">
                          <button type="submit" class="{{ URL::to('allow-user-registration' . $user->id) }} allow-user-registration"><i class="fa fa-thumbs-up"></i></button>
                          {{ csrf_field() }}
                          </form>
                          <a href="{{ URL::to('deny-user-registration/'. $user->id ) }}" class="deny-user-registration"><i class="fa fa-thumbs-down"></i></a>
                          <a href="{{ URL::to('delete-user-registration/'. $user->id ) }}" class="delete-user-registration"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
@endsection

@section('script')

<script>
  //Delete user registration
  $('.delete-user-registration').on('click', function(e){

  e.preventDefault();

  var deleteUserRegistration = $(this).attr('href');

  swal({   
    title: "Are you sure?",
    text: "You will not be able to recover this lorem ipsum!",         
    type: "warning",   
    showCancelButton: true,   
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Delete", 
    closeOnConfirm: false 
  }, 
    
  function(){   
     window.location.href = deleteUserRegistration;
  });
});

@foreach($users as $user)
//Allow user registration
  $('{{'.allow-user-registration' . $user->id}}').on('click', function(e){

  e.preventDefault();


  swal({   
    title: "Are you sure?",
    text: "You will not be able to recover this lorem ipsum!",         
    type: "warning",   
    showCancelButton: true,   
    confirmButtonColor: "#4591C5",
    confirmButtonText: "Accept", 
    closeOnConfirm: false 
  }, 
    
  function(){   
     $('{{'#allow-user-registration' . $user->id}}').submit();
  });
});
@endforeach

//Deny user registration
  $('.deny-user-registration').on('click', function(e){

  e.preventDefault();

  var denyUserRegistration = $(this).attr('href');

  swal({   
    title: "Are you sure?",
    text: "You will not be able to recover this lorem ipsum!",         
    type: "warning",   
    showCancelButton: true,   
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Deny", 
    closeOnConfirm: false 
  }, 
    
  function(){   
     window.location.href = denyUserRegistration;
  });
});  
</script>

<!-- DataTables -->
<script src="{{ URL::to('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#registration-list").DataTable({
       "sort": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

@endsection

