@extends('layouts.user-dashboard-layout')

@section('title')
iTOOhL | Dashboard
@endsection

@section('dashboard-title')
Add Inventory
@endsection

@section('content')

<!-- Main row -->
<div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
        <!--add billboard -->
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('create-billboard') }}" method="post">
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="inventory_image">Inventory Image</label>
                            <input type="file" name="inventory_image" id="inventory_image" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Name" required>
                        </div>
                        <div class="form-group">
                            <label for="supplier">Supplier</label>
                            <input type="text" class="form-control" name="supplier" id="supplier" placeholder="Supplier">
                        </div>
                        <div class="form-group">
                            <label for="company">Company</label>
                            <input type="text" class="form-control" name="company" id="company" placeholder="Company">
                        </div>
                        <div class="form-group">
                            <label>Inventory Country Location</label>
                            <select class="form-control select2" name="country" id="country" style="width: 100%;">
                            @foreach( $countries as $country )
                                <option value="{{$country->name}}">{{ $country->name }}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Inventory City Location</label>
                            <select class="form-control select2" name="city" id="city" style="width: 100%;">
                            @foreach( $cities as $city )
                                <option value="{{ $city->name }}">{{ $city->name }}</option>
                            @endforeach  
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="street_address">Inventory Street Address Location</label>
                            <input type="text" class="form-control" name="street_address" id="street_address" placeholder="Street Address">
                        </div>
                        <div class="form-group">
                            <label>Format</label>
                            <select class="form-control select2" name="format" id="format" style="width: 100%;">
                            @foreach( $formats as $format )
                                <option value="{{ $format->name }}">{{ $format->name }}</option>
                            @endforeach 
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="product">Product</label>
                            <input type="text" class="form-control" name="product" id="product" placeholder="product">
                        </div>
                        <div class="form-group">
                            <label>Industry</label>
                            <select class="form-control select2" name="industry" id="industry" style="width: 100%;">
                            @foreach( $industries as $industry )
                                <option value="{{$industry->name}}">{{ $industry->name }}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="landmark">Land Mark</label>
                            <input type="text" class="form-control" name="landmark" id="landmark" placeholder="Land Mark">
                        </div>
                        <div class="form-group">
                            <label>Size</label>
                            <select class="form-control select2" name="size" id="size" style="width: 100%;">
                            @foreach( $sizes as $size )
                                <option value="{{ $size->name }}">{{ $size->name }}</option>
                            @endforeach  
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Payment Terms</label>
                            <input text="payment_term" name="payment_term" class="form-control" id="payment_term" placeholder="Payment Terms">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Avalability</label>
                            <select class="form-control select2" name="availability" id="availability" style="width: 100%;">
                            @foreach( $availabilities as $availability )
                                <option value="{{ $availability->name }}">{{ $availability->name }}</option>
                            @endforeach  
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Gender</label>
                            <select class="form-control select2" name="gender" id="gender" style="width: 100%;">
                            @foreach( $genders as $gender )
                                <option value="{{ $gender->name }}">{{ $gender->name }}</option>
                            @endforeach  
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Economic Class</label>
                            <select class="form-control select2" name="economic_class" id="economic_class" style="width: 100%;">
                            @foreach( $economic_classes as $economic_class )
                                <option value="{{ $economic_class->name }}">{{ $economic_class->name }}</option>
                            @endforeach  
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Age</label>
                            <select class="form-control select2" name="age" id="age" style="width: 100%;">
                            @foreach( $ages as $age )
                                <option value="{{ $age->name }}">{{ $age->name }}</option>
                            @endforeach  
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="rates">Monthly Rates</label>
                            <input type="number" class="form-control" name="rates" id="rates" placeholder="rates">
                        </div>
                        <div class="form-group">
                            <label for="searchmap">Inventory Map Location</label>
                            <input type="text" class="form-control" id="searchmap" placeholder="Search Location">
                        </div>
                        <div class="form-group">
                            <div id="map" style="height: 500px;"></div>
                        </div>
                        <!--<div class="form-group">-->
                        <!--<label for="latitude">Latitude</label>-->
                        <input type="hidden" class="form-control" name="latitude" id="latitude" placeholder="Latitude">
                        <!--</div>-->
                        <!--<div class="form-group">-->
                        <!--<label for="longitude">Longitude</label>-->
                        <input type="hidden" class="form-control" name="longitude" id="longitude" placeholder="Longitude">
      <!--</div>-->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer col-md-12">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    {{ csrf_field() }}
                </div>
            </form>
        </div>
    </section>
</div>
@endsection

@section('script')
    <script>
        function initMap() {
            var mapDiv = document.getElementById('map');
            var map = new google.maps.Map(mapDiv, {
                center: {
                    lat: 14.554505, 
                    lng: 121.023867
                },
                zoom: 15
            });

            var marker = new google.maps.Marker({
                position: {
                    lat: 14.554505, 
                    lng: 121.023867
                },
                map: map,
                draggable: true
            });

            var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

            google.maps.event.addListener(searchBox, 'places_changed',function(){
                var places = searchBox.getPlaces();
                var bounds = new google.maps.LatLngBounds();
                var i, places;

                for (i=0; place=places[i];i++) {
                    bounds.extend(place.geometry.location);
                    marker.setPosition(place.geometry.location);
                }

                map.fitBounds(bounds);
                map.setZoom(15);
            });

            google.maps.event.addListener(marker,'position_changed',function(){
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();

            $('#latitude').val(lat);
            $('#longitude').val(lng);
        })
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8hnRJwlyh4zeJ_AjxFwkAHsxBtAG0Kcs&callback=initMap&libraries=visualization,places"></script>
@endsection
