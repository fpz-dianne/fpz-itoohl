@extends('layouts.user-dashboard-layout')


@section('title')
iTOOhL | Compare Report
@endsection

@section('style')
<!-- Morris chart -->
<link rel="stylesheet" href="{{ URL::to('plugins/morris/morris.css')}}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ URL::to('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endsection

@section('dashboard-title')
Compare Report
@endsection

@section('content')
<!-- Main row -->
<div class="row">
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
<section class="col-lg-12 connectedSortable">
    <div class="col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data</h3>
        </div><!-- /.box-header -->
        <form role="form" action="{{ URL::route('generate-report-compare') }}" method="post">
        <div class="box-body">
            <div class="form-group">
              <label>User</label>
              <select class="form-control select2" name="user_1" id="user_1" style="width: 100%;">
                <option value="{{ $users->id }}">{{ $users->first_name }} {{ $users->last_name }}</option>
                <option selected value="all_ooh">OOH Inventory</option>
              </select>
            </div>
            <div class="form-group">
              <label>Group by</label>
              <select class="form-control select2" name="data_1" id="data_1" style="width: 100%;">
                <option value="format">Formats</option>
                <option value="industry">Industries</option>
                <option value="products">Products</option>
                <option value="size">Size</option>
              </select>
            </div>
            <div class="form-group">
              <label>Format</label>
              <select class="form-control select2" multiple="multiple"  data-placeholder="Select Format" name="formats_1[]" id="formats_1" style="width: 100%;">
                <option selected value="all_format">All</option>
                @foreach($formats as $format)
                  <option value="{{ $format->name }}">{{ $format->name }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>Industry</label>
              <select class="form-control select2" multiple="multiple"  data-placeholder="Select Industry" name="industry_1[]" id="industry_1" style="width: 100%;">
                <option selected value="all_industry">All</option>
                @foreach($industries as $industry)
                  <option value="{{ $industry->name }}">{{ $industry->name }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>Product</label>
              <select class="form-control select2" multiple="multiple" data-placeholder="Select Product" name="products_1[]" id="products_1" style="width: 100%;">
              <option selected value="all_products">All</option>
              @foreach($products_1 as $product)
                <option value="{{ $product->products }}">{{ $product->products }}</option>
              @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>Size</label>
              <select class="form-control select2" multiple="multiple" data-placeholder="Select Size" name="sizes_1[]" id="sizes_1" style="width: 100%;">
              <option selected value="all_size">All</option>
              @foreach($sizes as $size)
                  <option value="{{ $size->name }}">{{ $size->name }}</option>
              @endforeach
              </select>
            </div>
        </div><!-- /.box-body -->
      </div>  
    </div>
      
      <div class="col-md-6">
        <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="form-group">
              <label>User</label>
              <select class="form-control select2" name="user_2" id="user_2" style="width: 100%;">
                <option value="{{ $users->id }}">{{ $users->first_name }} {{ $users->last_name }}</option>
                <option selected value="all_ooh">OOH Inventory</option>
              </select>
            </div>
            <div class="form-group">
              <label>Group by</label>
              <select class="form-control select2" name="data_2" id="data_2" style="width: 100%;">
                <option value="format">Formats</option>
                <option value="industry">Industries</option>
                <option value="products">Products</option>
                <option value="size">Size</option>
              </select>
            </div>
            <div class="form-group">
              <label>Format</label>
              <select class="form-control select2" multiple="multiple" data-placeholder="Select Format" name="formats_2[]" id="formats_2" style="width: 100%;">
                <option selected value="all_format">All</option>
                @foreach($formats as $format)
                  <option value="{{ $format->name }}">{{ $format->name }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>Industry</label>
              <select class="form-control select2" multiple="multiple"  data-placeholder="Select Industry" name="industry_2[]" id="industry_2" style="width: 100%;">
                <option selected value="all_industry">All</option>
                @foreach($industries as $industry)
                  <option value="{{ $industry->name }}">{{ $industry->name }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>Product</label>
              <select class="form-control select2" multiple="multiple" data-placeholder="Select Product" name="products_2[]" id="products_2" style="width: 100%;">
              <option selected value="all_products">All</option>
              </select>
            </div>
            <div class="form-group">
              <label>Size</label>
              <select class="form-control select2" multiple="multiple" data-placeholder="Select Size" name="sizes_2[]" id="sizes_2" style="width: 100%;">
              <option selected value="all_size">All</option>
              @foreach($sizes as $size)
                  <option value="{{ $size->name }}">{{ $size->name }}</option>
              @endforeach
              </select>
            </div>
        </div><!-- /.box-body -->
      </div>  
      </div>

      <div class="col-md-6 pull-right">
        <div class="info-box box-info">
          <span class="info-box-icon bg-white" style="height: 86px;"><i class="fa fa-bar-chart-o"></i></span>
          <div class="info-box-content">
           <div class="form-group">
              <label>Chart Type</label>
              <select class="form-control select2" name="chart_type" id="chart_type" style="width: 100%;">
                <option value="pieChart">Doughnut</option>
                <option value="barChart">Bar</option>
              </select>
            </div>
          </div>
          <!-- /.info-box-content -->
        </div>  
      </div>
      
        <div class="box-footer col-md-12">
          <button type="submit" class="btn btn-primary pull-right">Generate Report</button>
        </div>
        {{ csrf_field() }}
        </form>
</section>
            <!-- right col -->
          </div>
          <!-- /.row (main row) -->
@endsection

@section('script')
  <script>
      //Ajax call for products
      $('#user_1').on('change', function(e){
        console.log(e);

        var user_1_id = e.target.value;

        $.get('ajax-report-products-1?user_1_id=' + user_1_id, function(data) {
          //console.log(data);
        $('#products_1').not(':first').empty();
          $.each(data, function(index, productObj){
            $('#products_1').append('<option value"' +productObj.products+ '">'+productObj.products+'</option>');
          });
        });
      });

      $('#user_2').on('change' ,function(e){
        console.log(e);

        var user_2_id = e.target.value;

        $.get('ajax-report-products-2?user_2_id=' + user_2_id, function(data) {
          //console.log(data);
         $('#products_2').not(':first').empty();
          $.each(data, function(index, productObj){
            $('#products_2').append('<option value"' +productObj.products+ '">'+productObj.products+'<option>');
          });
        });
      });

      //remove all in list
      $('#formats_1').on('change' ,function(e){
         if( !$('#formats_1').val() ) { 
            $("#formats_1 option[value='all_format']").prop('selected', 'selected');
          }
          else {
           $("#formats_1").find("option[value='all_format']").prop('selected', false);          
          }
      });

      $('#formats_2').on('change' ,function(e){
         if( !$('#formats_2').val() ) { 
            $("#formats_2 option[value='all_format']").prop('selected', 'selected');
          }
          else {
           $("#formats_2").find("option[value='all_format']").prop('selected', false);          
          }
      });

      $('#industry_1').on('change' ,function(e){
         if( !$('#industry_1').val() ) { 
            $("#industry_1 option[value='all_industry']").prop('selected', 'selected');
          }
          else {
           $("#industry_1").find("option[value='all_industry']").prop('selected', false);          
          }
      });

      $('#industry_2').on('change' ,function(e){
         if( !$('#industry_2').val() ) { 
            $("#industry_2 option[value='all_industry']").prop('selected', 'selected');
          }
          else {
           $("#industry_2").find("option[value='all_industry']").prop('selected', false);          
          }
      });

      $('#products_1').on('change' ,function(e){
         if( !$('#products_1').val() ) { 
            $("#products_1 option[value='all_products']").prop('selected', 'selected');
          }
          else {
           $("#products_1").find("option[value='all_products']").prop('selected', false);          
          }
      });

      $('#products_2').on('change' ,function(e){
         if( !$('#products_2').val() ) { 
            $("#products_2 option[value='all_products']").prop('selected', 'selected');
          }
          else {
           $("#products_2").find("option[value='all_products']").prop('selected', false);          
          }
      });

      $('#sizes_1').on('change' ,function(e){
         if( !$('#sizes_1').val() ) { 
            $("#sizes_1 option[value='all_size']").prop('selected', 'selected');
          }
          else {
           $("#sizes_1").find("option[value='all_size']").prop('selected', false);          
          }
      });

      $('#sizes_2').on('change' ,function(e){
         if( !$('#sizes_2').val() ) { 
            $("#sizes_2 option[value='all_size']").prop('selected', 'selected');
          }
          else {
           $("#sizes_2").find("option[value='all_size']").prop('selected', false);          
          }
      });

  </script>
@endsection
