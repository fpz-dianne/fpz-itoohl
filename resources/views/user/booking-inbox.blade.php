@extends('layouts.user-dashboard-layout')

@section('title')
  iTOOhL | Booking Inbox
@endsection

@section('style')
 <!-- DataTables -->
  <link rel="stylesheet" href="{{ URL::to('plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('dashboard-title')
  Booking Inbox
@endsection

@section('content')
	<!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @if( !$booking_inbox->isEmpty() )
                <div class="box">
                    <div class="box-header"></div>
                    <div class="box-body">
                        <table id="booking-inbox-list" class="table table-bordered table-hover">
                            <thead>
                                <tr class="bg-blue">
                                    <th>Inventory Name</th>
                                    <th>Booking Memo From</th>
                                    <th>Placement Duration</th>
                                    <th>Material Changes</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach( $booking_inbox as $booking )
                                <tr>
                                    <td>{{$booking->name}}</td>
                                    <td>{{$booking->user->email}}</td>
                                    <td>{{$booking->placement_duration}}</td>
                                    <td>{{$booking->material_changes}}</td>
                                    <td><span class="label label-warning">Pending</span></td>
                                    <td class="text-center">
                                        <a href="{{ URL::to('booking/' . $booking->id ) }}"><i class="fa fa-eye"></i></a>
                                        <a href="{{ URL::to('booking-delete/' . $booking->id ) }}" class="delete-booking"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @else
                <div class="box">
                    <div class="box-body">
                        <h1 class="text-center">No Records Found</h1>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </section>
@endsection

@section('script')

<script>
    $('.delete-booking').on('click', function(e){
        e.preventDefault();
        var deleteBooking = $(this).attr('href');
        swal({   
            title: "Are you sure?",
            text: "You will not be able to recover this lorem ipsum!",         
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete", 
            closeOnConfirm: false 
        }, 
        function(){   
            window.location.href = deleteBooking;
        });
    })
</script>

<!-- DataTables -->
<script src="{{ URL::to('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
        $("#booking-inbox-list").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
  });
</script>

@endsection

