@extends('layouts.user-dashboard-layout')

@section('title')
  iTOOhL | Subscriber
@endsection

@section('style')
 <!-- DataTables -->
  <link rel="stylesheet" href="{{ URL::to('plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('dashboard-title')
  Subscriber
@endsection

@section('content')
     <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          @if( !$subscribers->isEmpty() )
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="subscriber-list" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach( $subscribers as $subscriber )
                      <tr>
                        <td>{{ $subscriber->id }}</td>
                        <td>{{ $subscriber->email }}</td>
                        <td class="text-center">
                          <a href="{{ URL::to('subscriber-delete/'. $subscriber->id ) }}" class="delete-subscriber"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          @else
          <div class="box">
            <div class="box-body">
              <h1 class="text-center">No Records Found</h1>
            </div>
          </div>  
          @endif
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
@endsection

@section('script')

<script>
  //Delete subscriber
  $('.delete-subscriber').on('click', function(e){

  e.preventDefault();

  var deleteSubscriber = $(this).attr('href');

  swal({   
    title: "Are you sure?",
    text: "You will not be able to recover this lorem ipsum!",         
    type: "warning",   
    showCancelButton: true,   
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Delete", 
    closeOnConfirm: false 
  }, 
    
  function(){   
     window.location.href = deleteSubscriber;
  });
});
 
</script>

<!-- DataTables -->
<script src="{{ URL::to('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#subscriber-list").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

@endsection

