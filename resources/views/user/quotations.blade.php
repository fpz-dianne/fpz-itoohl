@extends('layouts.user-dashboard-layout')

@section('title')
  iTOOhL | Quotes
@endsection

@section('style')
 <!-- DataTables -->
  <link rel="stylesheet" href="{{ URL::to('plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('dashboard-title')
  Quotes
@endsection

@section('content')
     <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          @if( !$quotes->isEmpty() )
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="quotations-list" class="table table-bordered table-hover">
                <thead>
                <tr class="bg-blue">
                  <th>Inventory Name</th>
                  <th>Quotes From</th>
                  <th>Placement Duration</th>
                  <th>Material Changes</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach( $quotes as $quote )
                      <tr>
                        <td>{{$quote->name}}</td>
                        <td>{{$quote->user->email}}</td>
                        <td>{{$quote->placement_duration}}</td>
                        <td>{{$quote->material_changes}}</td>
                        <td class="text-center">
                          <a href="{{ URL::to('quotes/' . $quote->id ) }}" class="delete-inventory"><i class="fa fa-eye"></i></a>
                          <a href="{{ URL::to('quotes-delete/' . $quote->id ) }}" class="delete-quote"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
            <!-- /.box -->
      </div>
      @else
        <div class="box">
          <div class="box-body">
            <h1 class="text-center">No Records Found</h1>
          </div>
        </div>
      @endif
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection

@section('script')

<script>
  $('.delete-quote').on('click', function(e){

  e.preventDefault();

  var deleteQuote = $(this).attr('href');

  swal({   
    title: "Are you sure?",
    text: "You will not be able to recover this lorem ipsum!",         
    type: "warning",   
    showCancelButton: true,   
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Delete", 
    closeOnConfirm: false 
  }, 
    
  function(){   
     window.location.href = deleteQuote;
  });
})
</script>

<!-- DataTables -->
<script src="{{ URL::to('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#quotations-list").DataTable({
      "sort": false
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

@endsection

