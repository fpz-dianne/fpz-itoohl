@extends('layouts.user-dashboard-layout')

@section('title')
iTOOhL | Dashboard
@endsection

@section('dashboard-title')
Add Inventory
@endsection

@section('content')

<!-- Main row -->
<div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
        <!--add billboard -->
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('save-billboard') }}" method="post">
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="inventory_image">Inventory Image</label>
                            <input type="file" name="inventory_image" id="inventory_image" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="date_of_collection">Date of Collection</label>
                            <input type="date" class="form-control" name="date_of_collection" id="date_of_collection">
                        </div>
                        <div class="form-group">
                            <label for="highway">Highway</label>
                            <input type="text" class="form-control" name="highway" id="highway" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Product</label>
                            <input type="text" class="form-control" name="product" id="product" required>
                        </div>
                        <div class="form-group">
                            <label for="supplier">Creative Campaign</label>
                            <input type="text" class="form-control" name="creative_campaign" id="creative_campaign">
                        </div>
                        <div class="form-group">
                            <label>Structure</label>
                            <select class="form-control select2" name="structure" id="structure" style="width: 100%;">
                            @foreach( $formats as $format )
                                <option value="{{ $format->name }}">{{ $format->name }}</option>
                            @endforeach 
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Size</label>
                            <select class="form-control select2" name="size" id="size" style="width: 100%;">
                            @foreach( $sizes as $size )
                                <option value="{{ $size->name }}">{{ $size->name }}</option>
                            @endforeach  
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="cost">Cost</label>
                            <input type="number" class="form-control" name="cost" id="cost">
                        </div>
                        <div class="form-group">
                            <label>Region</label>
                            <select class="form-control select2" name="region" id="region" style="width: 100%;">
                            @foreach( $regions as $region )
                                <option value="{{ $region->name }}">{{ $region->name }}</option>
                            @endforeach  
                            </select>
                        </div>
                        <div class="form-group">
                            <label>City</label>
                            <select class="form-control select2" name="city" id="city" style="width: 100%;">
                            @foreach( $cities as $city )
                                <option value="{{ $city->name }}">{{ $city->name }}</option>
                            @endforeach  
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="location">Location</label>
                            <input type="text" class="form-control" name="location" id="location">
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer col-md-12">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    {{ csrf_field() }}
                </div>
            </form>
        </div>
    </section>
</div>
@endsection
