@extends('layouts.user-dashboard-layout')

@section('title')
iTOOhL | Inventory Edit
@endsection

@section('dashboard-title')
Edit {{ $billboard->name }}
@endsection

@section('content')
<!-- Main row -->
<div>
	<form class="form-horizontal col-md-12" enctype="multipart/form-data" action="{{ URL::to('billboards/' . $billboard->id . '/update-image') }}" method="post">
		<div class="form-group">
			<div class="image" style="position: relative;">
				<label for="inventory_image" id="upload-design">
					<img src="#" id="file-image" width="160" height="160" style="display: none; background-color: #fff;">
					<img src="{{ URL::to($billboard->image) }}" alt="User Image" id="current-image">
				</label>
				<input type="file" name="inventory_image" class="masterTooltip input-inventory-img" id="inventory_image" title="Upload Photo">
				{{ csrf_field() }}
				<p id="file-name"></p>
			</div>
			<button type="submit" class="btn btn-yellow" id="upload-photo" style="position: relative; display: none; margin: auto;">Upload Photo</button>
				@if ($billboard->image != 'uploads/inventory-images/inventory_image_default.jpg')
					<a href="{{ URL::to('billboards/' . $billboard->id) . '/delete-image' }}" type="submit" class="btn btn-yellow delete-inventory" id="delete-photo" style="position: relative;">Delete Photo</a>
				@endif
		</div>
	</form>
</div>
<div class="row">
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<!-- /.box-header -->
					<!-- form start -->
					<form role="form" action="{{ URL::to('billboards/' . $billboard->id ) }}" method="post">
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label for="date_of_collection">Date of Collection</label>
									<input type="date" class="form-control" name="date_of_collection" id="date_of_collection" value="{{ $billboard->date_of_collection }}">
								</div>
								<div class="form-group">
									<label for="highway">Highway</label>
									<input type="text" class="form-control" name="highway" id="highway" value="{{ $billboard->highway }}" required>
								</div>
								<div class="form-group">
									<label for="name">Product</label>
									<input type="text" class="form-control" name="product" id="product" value="{{ $billboard->product }}" required>
								</div>
								<div class="form-group">
									<label for="supplier">Creative Campaign</label>
									<input type="text" class="form-control" name="creative_campaign" id="creative_campaign" value="{{ $billboard->creative_campaign }}">
								</div>
								<div class="form-group">
									<label>Structure</label>
									<select class="form-control select2" name="structure" id="structure" style="width: 100%;">
										<option value="{{ $billboard->structure }}">{{ $billboard->structure }}</option>
										@foreach( $formats as $format )
											<option value="{{ $format->name }}">{{ $format->name }}</option>
										@endforeach 
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Size</label>
									<select class="form-control select2" name="size" id="size" style="width: 100%;">
										<option value="{{ $billboard->size }}">{{ $billboard->size }}</option>
										@foreach( $sizes as $size )
											<option value="{{ $size->name }}">{{ $size->name }}</option>
										@endforeach  
									</select>
								</div>
								<div class="form-group">
									<label for="cost">Cost</label>
									<input type="number" class="form-control" name="cost" id="cost" value="{{ $billboard->cost }}">
								</div>
								<div class="form-group">
									<label>Region</label>
									<select class="form-control select2" name="region" id="region" style="width: 100%;">
										<option value="{{ $billboard->region }}">{{ $billboard->region }}</option>
										@foreach( $regions as $region )
											<option value="{{ $region->name }}">{{ $region->name }}</option>
										@endforeach  
									</select>
								</div>
								<div class="form-group">
									<label>City</label>
									<select class="form-control select2" name="city" id="city" style="width: 100%;">
										<option value="{{ $billboard->city }}">{{ $billboard->city }}</option>
										@foreach( $cities as $city )
											<option value="{{ $city->name }}">{{ $city->name }}</option>
										@endforeach  
									</select>
								</div>
								<div class="form-group">
									<label for="location">Location</label>
									<input type="text" class="form-control" name="location" id="location" value="{{ $billboard->location }}">
								</div>
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer col-md-12">
							<button type="submit" class="btn btn-primary">Update</button>
						</div>
						{{ csrf_field() }}
					</form>
				</div>
				<!-- /.box -->


			</div>
		</div>

	</section>
</div>
@endsection

@section('script')
<script>
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#file-image').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}

$('input[type=file]').change(function(){
	if ($(this).get(0).files.length != 0) {
		readURL(this);
		$('#file-image').css('display', '');
		$('#upload-photo').css('display', '');
		$('#delete-photo').css('display', 'none');
		$('img#current-image').css('display', 'none');
	} else {
		$('#file-image').css('display', 'none');
		$('#upload-photo').css('display', 'none');
		$('#delete-photo').css('display', '');
		$('img#current-image').css('display', '');
	}
});
</script>

<script>
$(document).ready(function() {
	$('form.form-horizontal').mouseover(function() {
		$('form.form-horizontal input[type=file]').addClass('input-hover');
		$('form.form-horizontal #upload-design').addClass('input-hover');
	});

	$('form.form-horizontal').mouseout(function() {
		$('form.form-horizontal input[type=file]').removeClass('input-hover');
		$('form.form-horizontal #upload-design').removeClass('input-hover');
	});
});
</script>

<script>
$('.delete-inventory').on('click', function(e){

	e.preventDefault();

	var deleteInventory = $(this).attr('href');

	swal({   
		title: "Are you sure?",
		text: "You will not be able to recover this lorem ipsum!",         
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Delete", 
		closeOnConfirm: false 
	}, 

	function(){   
		window.location.href = deleteInventory;
	});
})
</script>
@endsection
