@extends('layouts.user-dashboard-layout')

@section('title')
Itoohl | Bookmark List
@endsection

@section('style')
<!-- DataTables -->
<link rel="stylesheet" href="{{ URL::to('plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('dashboard-title')
Bookmark List
@endsection

@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            @if( !$bookmarks->isEmpty() )
            <div class="box">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table  id="bookmark-list" class="table table-bordered table-striped">
                        <thead>
                            <tr class="bg-blue">
                                <th>Supplier</th>
                                <th>Format</th>
                                <th>Name</th>
                                <th>Location</th>
                                <th>Industry</th>
                                @if(Auth::user()->role != 3)
                                <th>Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach( $bookmarks as $bookmark)
                            <tr>
                                <td>{{ $bookmark->portal->supplier }}</td>
                                <td>{{ $bookmark->portal->format }}</td>
                                <td>{{ $bookmark->portal->name }}</td>
                                <td>{{ $bookmark->portal->city }} {{ $bookmark->portal->street_address }}</td>
                                <td>{{ $bookmark->portal->industry }}</td>
                                @if(Auth::user()->role != 3)
                                <td class="text-center">
                                    <a href="{{ URL::to('ajax-bookmark-view/' .  $bookmark->id ) }}"><i class="fa fa-eye"></i></a>
                                    <a href="{{ URL::to('bookmark-delete/' .  $bookmark->id ) }}" class="delete-bookmark"><i class="fa fa-trash"></i></a>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            @else
            <div class="box">
                <div class="box-body">
                  <h1 class="text-center">No Records Found</h1>
              </div>
            </div>  
            @endif
        </div>
    </div>
</section>
@endsection

@section('script')

<script>
    $('.delete-bookmark').on('click', function(e){
        e.preventDefault();
        var deletebookmark = $(this).attr('href');
        swal({   
            title: "Are you sure?",
            text: "You will not be able to recover this lorem ipsum!",         
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete", 
            closeOnConfirm: false 
        }, 
        function() {   
            window.location.href = deletebookmark;
        });
    })
</script>

<!-- DataTables -->
<script src="{{ URL::to('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(function () {
        $("#bookmark-list").DataTable({
            "sort": false
        });
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
@endsection

