@extends('layouts.user-dashboard-layout')

@section('title')
  iTOOhL | Requested Quotes
@endsection

@section('style')
 <!-- DataTables -->
  <link rel="stylesheet" href="{{ URL::to('plugins/datatables/dataTables.bootstrap.css') }}">
@endsection


@section('dashboard-title')
  Requested Quotes
@endsection

@section('content')
     <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          @if( !$request_quotations->isEmpty() )
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="requested-quotes-list" class="table table-bordered table-hover">
                <thead>
                <tr class="bg-blue">
                  <th>Inventory Name</th>
                  <th>Request From</th>
                  <th>Placement Duration</th>
                  <th>Material Changes</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach( $request_quotations as $requested_quote )
                      <tr>
                        <td>{{ $requested_quote->name }}</td>
                        <td>{{ $requested_quote->user->email }}</td>
                        <td>{{ $requested_quote->placement_duration }}</td>
                        <td>{{ $requested_quote->material_changes }}</td>
                        <td class="text-center">
                          <a href="{{ URL::to('request-quotes/' . $requested_quote->id ) }}"><i class="fa fa-eye"></i></a>
                          <a href="{{ URL::to('requested-quotes-delete/' . $requested_quote->id ) }}" class="delete-requested-quote"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
            <!-- /.box -->
      </div>
      @else
        <div class="box">
          <div class="box-body">
            <h1 class="text-center">No Records Found</h1>
          </div>
        </div>
      @endif
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection

@section('script')

<script>
  $('.delete-requested-quote').on('click', function(e){

  e.preventDefault();

  var deleteRequestedQuote = $(this).attr('href');

  swal({   
    title: "Are you sure?",
    text: "You will not be able to recover this lorem ipsum!",         
    type: "warning",   
    showCancelButton: true,   
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Delete", 
    closeOnConfirm: false 
  }, 
    
  function(){   
     window.location.href = deleteRequestedQuote;
  });
})
</script>

<!-- DataTables -->
<script src="{{ URL::to('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#requested-quotes-list-list").DataTable({
      "sort": false
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

@endsection

