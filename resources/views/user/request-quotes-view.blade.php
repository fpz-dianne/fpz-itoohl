@extends('layouts.user-dashboard-layout')

@section('style')
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{URL::to('plugins/daterangepicker/daterangepicker.css')}}">
  <!-- daterange picker -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css">
@endsection

@section('title')
  iTOOhL | Requested Quotes
@endsection

@section('dashboard-title')
   Quote Request
@endsection

@section('content')
    
    @include('includes.send-quote-modal')

	   <!-- Main content -->
  <section class="invoice" id="invoice">
    <!-- title row -->
    <div class="printonly">
      <h1>From Itoohl.com</h1>
      <div class="print-space"></div>
    </div>
    <div class="row">
      <div class="col-xs-6">
         <img class="img-responsive inventory-image" src="{{ URL::to('uploads/inventory-images/' . $request_quote->inventory_img )}}" alt="">
      </div>
      <div class="col-xs-6" style="margin-bottom: 1em;">
        <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-map-marker"></i> {{ $request_quote->name }}
          <small class="pull-right">Date: {{ $request_quote->created_at->format('m/d/y') }}</small>
        </h2>
       <h4>From</h4>
        <address style="margin-bottom: 0;">
          <strong>{{ $request_quote->user->first_name }}</strong><br>
          Phone: {{ $request_quote->user->phone }}<br>
          Email: {{ $request_quote->user_email }}
        </address>
      </div>
      <!-- /.col -->
      <div class="col-xs-12">
        <h4>Request</h4>
        <b>Placement Duration:</b> {{ $request_quote->placement_duration }}<br>
        <b>Material Changes:</b> {{ $request_quote->material_changes }}<br>
        <b>Date of Material Changes:</b> {{ $request_quote->material_changes_date }}<br>
        <b>Production:</b> 968-34567
      </div>
      <!-- /.col -->
      <div class="col-xs-12">
        <h4>Special Instruction</h4>
        <div class="instruction-container col-sm-12">
          {{ $request_quote->instruction }}
        </div>
      </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
         <button type="button" data-toggle="modal" data-target="#send-quote" class="btn btn-primary pull-right" style="margin-right: 5px;">
          Send Quote
        </button>
        <a onclick="printContent('invoice')" target="_blank" class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-print"></i> Print</a>
      </div>
    </div>

  </section>
  <!-- /.content -->


@endsection

@section('script')

<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ URL::to('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
  <!-- Page script -->
<script>
  $(function () {
 

    //Date range picker
    $('.reservation').daterangepicker();


  //Date picker
    $('.material_changes_date').datepicker({
      autoclose: true
    });


  });
</script>

<script>
  $('.delete-inventory').on('click', function(e){

  e.preventDefault();

  var deleteInventory = $(this).attr('href');

  swal({   
    title: "Are you sure?",
    text: "You will not be able to recover this lorem ipsum!",         
    type: "warning",   
    showCancelButton: true,   
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Delete", 
    closeOnConfirm: false 
  }, 
    
  function(){   
     window.location.href = deleteInventory;
  });
})
</script>

<script>
//print qoutes
function printContent(el){
  var restorepage = document.body.innerHTML;
  var printcontent = document.getElementById(el).innerHTML;
  document.body.innerHTML = printcontent;
  window.print();
  document.body.innerHTML = restorepage;
}
</script>

@endsection

