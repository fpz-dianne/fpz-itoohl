@extends('layouts.master')

@section('title')
    iTOOhL | Launch
@endsection

@section('content')
	<section id="services" class="col-md-12 text-center">
        <div class="row feature feature-margin">
            <div class="col-md-3">
                <div class="feature-block f-h-1 wow fadeInLeft">
                    <img src="{{ URL::to('img/Plattform.png')}}">
                    <h6>Platform</h6>
                    <p>
                        We provide data for the different platform inventory formats and allow direct interaction with the rich media inventory
                    </p>
                    <div class="clip-hide" style="background-image:url(img/Services-Platform.gif)"></div>
                </div>
                <div class="feature-block f-h-2 wow fadeInLeft delay-05s">
                    <img src="{{ URL::to('img/Insights.png')}}">
                    <h6>Insights</h6>
                    <p>iTOOhL enables planners and advertisers to strategically plan with the use of updated research and insights</p>
                    <div class="clip-hide" style="background-image:url(img/Services-Insights.gif)"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="features-monitor">
                    <div class="monitor-mask">
                        <img src="{{ URL::to('img/monitor.png') }}" alt="">
                    </div>
                    <div class="monitor-img scal-img">
                        <div class="clip">
                            <div class="bg bg-bg-chrome act transition-05s"> <!-- style="background-image:url('img/Services-Locations.gif')" -->
                            </div>
                            <div class="video-bg">
                                <video loop muted autoplay poster="img/Services-Platform.gif" class="video" style="height: 100%;">
                                    <source src="img/Services-Locations.mp4" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    </div>
                </div>                  
            </div>
            <div class="col-md-3">
                <div class="feature-block f-h-3 wow fadeInRight">
                    <img src="{{ URL::to('img/Locations.png')}}">
                    <h6>Locations</h6>
                    <p>We allow planners and advertisers to search for inventory in different locations nationwide</p>
                    <div class="clip-hide" style=""></div>
                </div>
                <div class="feature-block f-h-4 wow fadeInRight delay-05s">
                    <img src="{{ URL::to('img/Products.png')}}">  
                    <h6>Industries</h6>
                    <p>We can generate specific product placements per industry</p>
                    <div class="clip-hide" style="background-image:url(img/Services-Products.gif)"></div>
                </div>
            </div>
        </div><!--end of row-->
        <div class="row launch">
        	<div class="col-md-4 col-md-offset-4" >
        		<a href="{{ URL::route('portal') }}"><button class="got-portal-btn btn btn-primary">Go to Portal</button></a>
        	</div>
        </div><!--end of row-->
    </section><!--end of services-->
@endsection

@section('script')
    <script>
        $('#address').popover({ html:true, content: '<img src="http://maps.googleapis.com/maps/api/staticmap?center=14.645425,+121.052854&zoom=16&scale=false&size=400x400&maptype=roadmap&key=AIzaSyBYRrmLiM21xODjVlJuCyCyauWkeOPFD48&format=png&visual_refresh=true&markers=icon:http://itoohl.com/img/icon-location.png%7Cshadow:true%7C14.645425,+121.052854" alt="" />', placement:'top' })
    </script>
@endsection