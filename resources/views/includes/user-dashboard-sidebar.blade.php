<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ URL::to('uploads/avatars/' . Auth::user()->avatar) }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><a href="{{ URL::route('profile') }}">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</a></p>
                <a href="#"><i class="fa fa-circle text-green"></i> Online</a>
            </div>
        </div>
        <!-- search form 
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <!-- <li class="header">MAIN NAVIGATION</li> -->
            <li class="treeview {{ (\Request::route()->getName() == 'user-dashboard') ? 'active' : '' }}">
                <a href="{{ URL::route('user-dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="{{ URL::route('portal') }}" target="_blank">
                    <i class="fa fa-globe"></i> <span>Portal</span>
                </a>
            </li>
            @if(Auth::user()->role != 2)
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i> <span>Inventory</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-down pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ URL::route('billboards') }}"><i class="fa fa-circle-o"></i> <span>Inventory List</span></a>
                    </li>
                    <li>
                        <a href="{{ URL::route('new-billboard') }}"><i class="fa fa-circle-o"></i> <span>Add Inventory</span></a>
                    </li>
                </ul>
            </li>
            @endif
            <li class="treeview {{ (\Request::route()->getName() == 'bookmark-list') ? 'active' : '' }}">
                <a href="{{ URL::route('bookmark-list') }}">
                    <i class="fa fa-bookmark"></i> <span>Bookmark</span>
                </a>
            </li>  
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-calendar"></i> <span>Booking</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-down pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ URL::route('booking') }}">
                            <i class="fa fa-circle-o"></i> <span>Booking History</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::route('booking-inbox') }}">
                            <i class="fa fa-circle-o"></i> <span>Booking Inbox</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file-text-o"></i> <span>Quotations</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-down pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ URL::route('quotations') }}">
                            <i class="fa fa-circle-o"></i> <span>Quotes List</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::route('requested-quotes') }}">
                            <i class="fa fa-circle-o"></i> <span>Requested Quotes</span>
                        </a>
                    </li>
                </ul>
            </li>
            @if(Auth::user()->role == 1)
            <li class="treeview {{ (\Request::route()->getName() == 'user-registration') ? 'active' : '' }}">
                <a href="{{ URL::route('user-registration') }}">
                    <i class="fa fa-users"></i> <span>Registration</span>
                </a>
            </li>
            <li class="treeview {{ (\Request::route()->getName() == 'demo-list') ? 'active' : '' }}">
                <a href="{{ URL::route('demo-list') }}">
                    <i class="fa fa-compass"></i> <span>Demo</span>
                </a>
            </li>  
            <li class="treeview {{ (\Request::route()->getName() == 'subscriber-list') ? 'active' : '' }}">
                <a href="{{ URL::route('subscriber-list') }}">
                    <i class="fa fa-rss"></i> <span>Subscriber</span>
                </a>
            </li>    
            @endif
        </ul>
    </section>
<!-- /.sidebar -->
</aside>