<div class="modal modal-primary fade in" id="booking-memo" tabindex="-1" role="dialog" aria-labelledby="request-quotesLabel">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="request-quotesLabel">Send Booking Memo</h4>
                </div>
                <div class="modal-body">
                    <form role="form" action="{{ URL::to('send-booking-memo/' . $quotes->id) }}"  method="post" >
                        <div class="box-body">
                            <div class="form-group">
                                <label for="confirmation">Confirmation</label>
                                <input type="text" class="form-control" name="confirmation" id="confirmation" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="final_rate">Final Rate</label>
                                <input type="number" class="form-control" name="final_rate" id="final_rate" placeholder="">
                            </div>

                            <input type="hidden" name="placement_duration" class="form-control pull-right reservation" id="reservation" value="{{ $quotes->placement_duration }}">
                            <input type="hidden" class="form-control" name="material_changes"  id="material_changes" value="{{ $quotes->material_changes }}">
                            <input type="hidden" name="material_changes_date" class="form-control pull-right material_changes_date" id="material_changes_date2" value="{{ $quotes->material_changes_date }}">
                            <input type="hidden" class="form-control" name="production" id="production" value="{{ $quotes->production }}">
                            <input type="hidden" class="form-control" name="instruction" value="{{ $quotes->instruction }}"></input>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            {{ csrf_field() }}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
