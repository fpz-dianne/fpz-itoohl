<div class="modal fade modal-primary" id="request-demo" tabindex="-1" role="dialog" aria-labelledby="request-quotesLabel">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-white">&times;</span>
                    </button>
                    <h3 class="text-semi-gray">Request Demo</h3>
                    <h4 class="text-semi-gray">Tick the boxes applicable to you</h4>
                </div>

                <div class="modal-body">
                    <form action="{{ URL::route('request-demo') }}" method="post" class="demo-form">
                        <div class="col-lg-6 no-padding">    
                            @foreach( $demooptions as $demooption )
                                <div class="form-group">
                                    <label class="text-semi-gray">
                                        <input type="checkbox" name="option[]" value="{{ $demooption->id }}" class="square-yellow demo-checkbox">
                                        {{ $demooption->name }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                      
                        <div class="col-lg-6 no-padding">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control demo-input" id="name" placeholder="Name" required>
                            </div>
                        </div>
                      
                        <div class="col-lg-6 no-padding">
                            <div class="form-group">
                                <input type="text" name="company" class="form-control demo-input" id="comapny" placeholder="Company">
                            </div>
                        </div>
                      
                        <div class="col-lg-6 no-padding">
                            <div class="form-group">
                                <input type="email" name="email" placeholder="Email" class="form-control demo-input" required>
                            </div>
                        </div>
                      
                        <div class="form-group">
                            <button type="submit" name="search" id="demo-btn" class="form-control btn demo-btn">Send Request <i class="fa fa-send"></i>
                            </button>
                            {{ csrf_field() }}
                        </div>
                    </form>
                </div> 
            </div>
        </div>
    </div>
</div>