<script type="text/javascript">
	
    $(function () {

        'use strict';

        // Make the dashboard widgets sortable Using jquery UI
        $(".connectedSortable").sortable({
            placeholder: "sort-highlight",
            connectWith: ".connectedSortable",
            handle: ".box-header, .nav-tabs",
            forcePlaceholderSize: true,
            zIndex: 999999
        });

        $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

        /* ChartJS
        * -------
        * Here we will create a few charts using ChartJS
        */

        //-------------
        //- PIE CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var pieChartCanvas1 = $("#pieChart1").get(0).getContext("2d");
        var pieChart1 = new Chart(pieChartCanvas1);

        var pieChartCanvas2 = $("#pieChart2").get(0).getContext("2d");
        var pieChart2 = new Chart(pieChartCanvas2);
        var PieData1 = [
            @foreach($results_1 as $result_1) {
                  value: {{ $result_1->data_count }},
                  label: "{{ $result_1->$data_1 }}"
            	},
            @endforeach
        ];

        PieData1.forEach(function (e, i) {
            e.color = "hsl(" + (i / PieData1.length * 360) + ", 89%, 65%)";
            e.highlight = "hsl(" + (i / PieData1.length * 360) + ", 100%, 70%)";
        });

        var PieData2 = [
            @foreach($results_2 as $result_2) {
                value: {{ $result_2->data_count }},
                label: "{{ $result_2->$data_2 }}"
            },
            @endforeach
        ];

        PieData2.forEach(function (e, i) {
            e.color = "hsl(" + (i / PieData2.length * 360) + ", 89%, 65%)";
            e.highlight = "hsl(" + (i / PieData2.length * 360) + ", 100%, 70%)";
        });


        var pieOptions = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke: true,
            //String - The colour of each segment stroke
            segmentStrokeColor: "#fff",
            //Number - The width of each segment stroke
            segmentStrokeWidth: 1,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 50, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps: 100,
            //String - Animation easing effect
            animationEasing: "easeOutBounce",
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate: true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale: false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: false,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
            //String - A tooltip template
            //tooltipTemplate: "<%=value %> <%=label%> Inventory"
            tooltipTemplate: "<%=value %> <%=label%>"
        };
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        pieChart1.Doughnut(PieData1, pieOptions).generateLegend();
        pieChart2.Doughnut(PieData2, pieOptions).generateLegend();

        legend(document.getElementById('legend-1'), PieData1);
        legend(document.getElementById('legend-2'), PieData2);
        //-----------------
        //- END PIE CHART -
        //-----------------
    });

    $('document').ready(function(){

        $('.legend').css({
            'width': '10em'
        }); 

        $('.legend .title').css({
            'display': 'block',
            'margin-bottom': '0.5em',
            'line-height': '1.2em',
            'padding': '0 0.3em'
        }); 

        $('.legend .color-sample').css({
            'display': 'block',
            'float': 'left',
            'width': '1em',
            'height': '1em',
            'border-radius': '0.5em', /* Comment out if you prefer squarish samples */
            'margin-right': '0.5em'
        }); 
    });  

</script>