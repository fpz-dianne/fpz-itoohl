<script type="text/javascript">
	var lat = 14.5501849903,
     	lng = 121.0319780001,
		map_zoom = 11;


	// google map custom marker icon - .png fallback for IE11
	var is_internetExplorer11= navigator.userAgent.toLowerCase().indexOf('trident') > -1;
	var marker_url = ( is_internetExplorer11 ) ? 'img/icon-locationp.png' : 'img/icon-location.png';

	var marker_dark_url = ( is_internetExplorer11 ) ? 'img/icon-location.png' : 'img/icon-location.png';

	// define the basic color of your map, plus a value for saturation and brightness
	var	main_color = '#2d313f',
	saturation_value= -20,
	brightness_value= 5;

	// we define here the style of the map
	var style= [
	
	];

	// set google map options
	var map_options = {
		center: new google.maps.LatLng(lat, lng),
		zoom: map_zoom,
		panControl: false,
		zoomControl: false,
		mapTypeControl: false,
		mapTypeControlOptions: {
			position: google.maps.ControlPosition.TOP_RIGHT
		},
		streetViewControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		scrollwheel: true,
		styles: style,
	}

	// inizialize the map
    var map = new google.maps.Map(document.getElementById('google-container'), map_options);

	// add a custom marker to the map				
	// var marker = new google.maps.Marker({
	//		position: new google.maps.LatLng(lat, lng),
	// 		map: map,
	//		visible: false,
	// 		icon: marker_url
	// });

	google.maps.event.addListener(map, 'click', function(event) {
		addMarker(event.latLng, map);
	});

	// add custom buttons for the tools on the map
	function CustomMapview(controlviewDiv, map) {
		// grap the mapview elements from the DOM and insert them in the map
		controlUImapview = document.getElementById('map-view'),
		controlUIlistview = document.getElementById('list-view');

		controlviewDiv.appendChild(controlUImapview);
		controlviewDiv.appendChild(controlUIlistview);

		google.maps.event.addDomListener(controlUImapview, 'click', function() {
			window.location.href = "{{ URL::route('portal') }}";
		});

		google.maps.event.addDomListener(controlUIlistview, 'click', function() {
			window.location.href = "{{ URL::route('portal-list') }}";
		});
	}

	var viewControlDiv = document.createElement('div');
        viewControlDiv.style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
        viewControlDiv.style.display = 'flex';
        viewControlDiv.style.borderRadius = '3px';
        viewControlDiv.style.cursor = 'pointer';
        viewControlDiv.style.marginTop = '10px';
        viewControlDiv.style.marginRight = '0px';
	var viewControl = new CustomMapview(viewControlDiv, map);

  	// insert the zoom div on the right left of the map
  	map.controls[google.maps.ControlPosition.RIGHT_TOP].push(viewControlDiv);


			// add custom buttons for the tools on the map
	function CustomZoomControl(controlDiv, map) {
		// grap the tool elements from the DOM and insert them in the map 
		var controlUIzoomIn = document.getElementById('zoom-in'),
			controlUIzoomOut = document.getElementById('zoom-out'),
			controlUItraffic = document.getElementById('traffic'),
			controlUIcircle = document.getElementById('circle');

		controlDiv.appendChild(controlUIzoomIn);
		controlDiv.appendChild(controlUIzoomOut);
		controlDiv.appendChild(controlUIcircle);
		controlDiv.appendChild(controlUItraffic);

		// Setup the click event listeners and zoom-in or out according to the clicked element
		google.maps.event.addDomListener(controlUIzoomIn, 'click', function() {
			map.setZoom(map.getZoom()+1)
		});
		google.maps.event.addDomListener(controlUIzoomOut, 'click', function() {
			map.setZoom(map.getZoom()-1)
		});
		google.maps.event.addDomListener(controlUItraffic, 'click', function() {
			var trafficLayer = new google.maps.TrafficLayer();
			trafficLayer.setMap(map);
            // Trigger Traffic layer tool
			$.get('ajax-filter-traffic', function(data) {
		     	$.each(data, function(index, inventoryObj) {
		             marker = new google.maps.Marker({
				        position: new google.maps.LatLng(inventoryObj.latitude, inventoryObj.longitude),
				        map: map,
				        category: inventoryObj.industry,
				        type: inventoryObj.format,
				        availability: inventoryObj.availability,
				        company: inventoryObj.company,
				        size: inventoryObj.size,
				        product: inventoryObj.products,
				        supplier: inventoryObj.supplier,
				        visible: true,
				       // animation: google.maps.Animation.DROP,
				        icon: marker_url
				      });

		            $('#markers').append('<li class="marker-link" data-markerid="'+inventoryObj.id+'" ><div class="markers-content"><a class="marker-link" data-markerid="'+index+'" href="#">' + inventoryObj.name + '</a><br/><small>' + inventoryObj.industry + '</small></div><img class="marker-link"  class="marker-link" src="uploads/inventory-images/'+inventoryObj.inventory_image+'"/></li>');

		            // Click Text format trigger marker function
					$('.marker-link').on('click', function () {
				        google.maps.event.trigger(markers[$(this).data('markerid')], 'click');
				    });


		            google.maps.event.addListener(marker, 'click', (function(marker, i) {
						return function() {
			          		triggerInventory(true);
				           	$("#name").text(inventoryObj.name);
				           	$("#format").text(inventoryObj.format);
				           	$("#size").text(inventoryObj.size);
				           	$("#availability").text(inventoryObj.availability);
				           	$("#product").text(inventoryObj.products);
				           	$("#industry").text(inventoryObj.industry);
				           	$("#rates").text(inventoryObj.rates);

				           	$(".inventory-image").attr('src', "uploads/inventory-images/" + inventoryObj.inventory_image);
				           	// modal content
				           	$("#inventory-name").val(inventoryObj.name);
				           	$("#inventory-name-text").text(inventoryObj.name);
				           	$("#inventory-supplier").val(inventoryObj.supplier);
				           	$("#inventory-supplier-text").text(inventoryObj.supplier);
				           	$("#inventory-user").val(inventoryObj.user.first_name);
					   	   	$("#inventory-user-text").text(inventoryObj.user.first_name);
				           	$("#inventory-company").val(inventoryObj.company);
				           	$("#inventory-company-text").text(inventoryObj.company);
				           	$("#inventory-rate").val(inventoryObj.rates);
				           	$("#inventory-rate-text").text(inventoryObj.rates);
				           	// modal content hidden field value
				           	$("#receiver_email").val(inventoryObj.user.email);
				           	$("#receiver_id").val(inventoryObj.user.id);
				           	$("#inventory_img").val(inventoryObj.inventory_image);
				           	$("#bookmark").attr('href', "bookmark/" + inventoryObj.id);
			           		map.setZoom(18);
					   		map.panTo(marker.position);
			        	}
			      	})(marker, inventoryObj.id));
		            markers.push(marker);
		           // console.log(inventoryObj);
		        });
	        });
		});

		google.maps.event.addDomListener(controlUIcircle, 'click', function() {
			var circle = new google.maps.Circle({
				map: map,
				center: map.getCenter(),
				radius: 6000,
				fillColor: '#4590C5',
				strokeOpacity: 0.8,
	    		strokeWeight: 0,
				editable: true,
				draggable: true,
			});

			// show marker if inside circle radius
			google.maps.event.addListener( circle, 'drag', function(e) {
				markers.forEach(function(marker) {
					if(circle.getBounds().contains( marker.getPosition() ) == true ) {
						marker.setVisible(true);
					}
					else {
						marker.setVisible(false);
					}
				});
			});

			$.get('ajax-filter-circle', function(data) {
	         	$.each(data, function(index, inventoryObj) {
		            marker = new google.maps.Marker({
				    	position: new google.maps.LatLng(inventoryObj.latitude, inventoryObj.longitude),
				        map: map,
				        category: inventoryObj.industry,
				        type: inventoryObj.format,
				        availability: inventoryObj.availability,
				        company: inventoryObj.company,
				        size: inventoryObj.size,
				        product: inventoryObj.products,
				        supplier: inventoryObj.supplier,
				        visible: false,
				       	// animation: google.maps.Animation.DROP,
				        icon: marker_url
				    });

		            $('#markers').append('<li class="marker-link" data-markerid="'+inventoryObj.id+'" ><div class="markers-content"><a class="marker-link" data-markerid="'+index+'" href="#">' + inventoryObj.name + '</a><br/><small>' + inventoryObj.industry + '</small></div><img class="marker-link"  class="marker-link" src="uploads/inventory-images/'+inventoryObj.inventory_image+'"/></li>');

		            // Click Text format trigger marker function
					$('.marker-link').on('click', function () {
				        google.maps.event.trigger(markers[$(this).data('markerid')], 'click');
				    });


		            google.maps.event.addListener(marker, 'click', (function(marker, i) {
						return function() {
			          		triggerInventory(true);
				           	$("#name").text(inventoryObj.name);
				           	$("#format").text(inventoryObj.format);
				           	$("#size").text(inventoryObj.size);
				           	$("#availability").text(inventoryObj.availability);
				           	$("#product").text(inventoryObj.products);
				           	$("#industry").text(inventoryObj.industry);
				           	$("#rates").text(inventoryObj.rates);

				           	$(".inventory-image").attr('src', "uploads/inventory-images/" + inventoryObj.inventory_image);
				           	// modal content
				           	$("#inventory-name").val(inventoryObj.name);
				           	$("#inventory-name-text").text(inventoryObj.name);
				           	$("#inventory-supplier").val(inventoryObj.supplier);
				           	$("#inventory-supplier-text").text(inventoryObj.supplier);
				           	$("#inventory-user").val(inventoryObj.user.first_name);
					       	$("#inventory-user-text").text(inventoryObj.user.first_name);
				           	$("#inventory-company").val(inventoryObj.company);
				           	$("#inventory-company-text").text(inventoryObj.company);
				           	$("#inventory-rate").val(inventoryObj.rates);
				           	$("#inventory-rate-text").text(inventoryObj.rates);
				           	// modal content hidden field value
				           	$("#receiver_email").val(inventoryObj.user.email);
				           	$("#receiver_id").val(inventoryObj.user.id);
				           	$("#inventory_img").val(inventoryObj.inventory_image);
				           	$("#bookmark").attr('href', "bookmark/" + inventoryObj.id);
				           	map.setZoom(18);
						   	map.panTo(marker.position);
			        	}
			      	})(marker, inventoryObj.id));
		            markers.push(marker);
		           	//  console.log(inventoryObj);
		        });
	        });
		});
	}

			//Page stop reload if hit enter key
	$('#search').on('keyup keypress', function(e) {
		var keyCode = e.keyCode || e.which;
		if (keyCode === 13) { 
			e.preventDefault();
				return false;
		}
	});


	var zoomControlDiv = document.createElement('div');
	var zoomControl = new CustomZoomControl(zoomControlDiv, map);

  	//insert the zoom div on the right left of the map
  	map.controls[google.maps.ControlPosition.RIGHT_TOP].push(zoomControlDiv);


  	// Create the search box and link it to the UI element.
  	var input = document.getElementById('search');
  	var searchBox = new google.maps.places.SearchBox(input);

	// Bias the SearchBox results towards current map's viewport.
  	map.addListener('bounds_changed', function() {
  		searchBox.setBounds(map.getBounds());
  	});

  	var markers = [];
  	// Listen for the event fired when the user selects a prediction and retrieve
  	// more details for that place.
  	searchBox.addListener('places_changed', function() {
  		var places = searchBox.getPlaces();
  		if (places.length == 0) {
  			return;
  		}
		// Clear out the old markers.
		markers.forEach(function(marker) {
		    marker.setMap(map);
		});
		//  markers = [];
		// For each place, get the icon, name and location.
	    var bounds = new google.maps.LatLngBounds();
	    places.forEach(function(place) {
	    	var icon = {
	    		url: marker_url,
	    		//size: new google.maps.Size(71, 71),
	    		//origin: new google.maps.Point(0, 0),
	    		//anchor: new google.maps.Point(17, 34),
	    		//scaledSize: new google.maps.Size(25, 25)
	    	};

		    // Create a marker for each place.
	        // markers.push(new google.maps.Marker({
	        // 		map: map,
	        // 		icon: icon,
	        // 		title: place.name,
	        // 		visible: false,
	        //  	position: place.geometry.location
	        // }));

		    if (place.geometry.viewport) {
		        // Only geocodes have viewport.
		        bounds.union(place.geometry.viewport);
		    } 
		    else {
		    	bounds.extend(place.geometry.location);
		    }
		});
		map.fitBounds(bounds);
	});

    var locations;

    //Trigger Filter Basic & Demographic
    $('.filter').on('change', function(e) {
        console.log(e);

        var filter_id = e.target.value;
        map.setZoom(11);

        console.log(filter_id);

        if(this.checked) {
        	console.log("Hello");
        	$.get('ajax-filter-inventory?filter_id=' + filter_id, function(data) {
	         	$.each(data, function(index, inventoryObj) {
		            marker = new google.maps.Marker({
				    	position: new google.maps.LatLng(inventoryObj.latitude, inventoryObj.longitude),
				        map: map,
				        name: inventoryObj.name,
				        category: inventoryObj.industry,
				        type: inventoryObj.format,
				        availability: inventoryObj.availability,
				        company: inventoryObj.company,
				        size: inventoryObj.size,
				        product: inventoryObj.products,
				        supplier: inventoryObj.supplier,
				        gender: inventoryObj.gender,
				        economic_class: inventoryObj.economic_class,
				        age: inventoryObj.age,
				        profile: inventoryObj.profile,
				        city: inventoryObj.city,
				        visible: true,
				        id: inventoryObj.id,
				       	// animation: google.maps.Animation.DROP,
				        icon: marker_url
				    });

		            google.maps.event.addListener(marker, 'click', (function(marker, data) {
						return function() {
				           	triggerInventory(true);
				           	$("#name").text(inventoryObj.name);
				           	$("#format").text(inventoryObj.format);
				           	$("#size").text(inventoryObj.size);
				           	$("#availability").text(inventoryObj.availability);
				           	$("#product").text(inventoryObj.products);
				           	$("#industry").text(inventoryObj.industry);
				           	$("#rates").text(inventoryObj.rates);

				           	$(".inventory-image").attr('src', "uploads/inventory-images/" + inventoryObj.inventory_image);
				           	// modal content
				           	$("#inventory-name").val(inventoryObj.name);
				           	$("#inventory-name-text").text(inventoryObj.name);
				           	$("#inventory-supplier").val(inventoryObj.supplier);
				           	$("#inventory-supplier-text").text(inventoryObj.supplier);
				           	$("#inventory-user").val(inventoryObj.user.first_name);
					       	$("#inventory-user-text").text(inventoryObj.user.first_name);
				           	$("#inventory-company").val(inventoryObj.company);
				           	$("#inventory-company-text").text(inventoryObj.company);
				           	$("#inventory-rate").val(inventoryObj.rates);
				           	$("#inventory-rate-text").text(inventoryObj.rates);
				           	// modal content hidden field value
				           	$("#receiver_email").val(inventoryObj.user.email);
				           	$("#receiver_id").val(inventoryObj.user.id);
				           	$("#inventory_img").val(inventoryObj.inventory_image);
				           	$("#bookmark").attr('href', "bookmark/" + inventoryObj.id);
				           	map.setZoom(18);
						   	map.panTo(marker.position);
						   	// Change marker icon onclick
						   	marker.setIcon(marker_dark_url);
			        	}
			      	})(marker, index));

		            markers.push(marker);

		            $('#markers').append('<li class="marker-link" data-markerid="'+index+'" data-marker="'+filter_id+'"><div class="markers-content"><a class="marker-link" data-markerid="'+index+'" data-marker="'+filter_id+'" href="#">' + inventoryObj.name + '</a><br/><small>' + inventoryObj.industry + '</small></div><img src="uploads/inventory-images/'+inventoryObj.inventory_image+'"/></li>');

		            //Click Text format trigger marker function
					$('.marker-link').on('click', function () {
				        google.maps.event.trigger(markers[$(this).data('markerid')], 'click');
				    });
		           	// console.log(inventoryObj);
		        });
	        });
        }
        else {
        	console.log("Hello again");
        	markers.forEach(function(marker) {
			 	if(marker.category == filter_id) {
			 		marker.setMap(null);
			 		map.setZoom(11);
			 		$('.marker-link[data-marker="'+filter_id+'"]').remove();
			 	}
			 	if(marker.type == filter_id) {
			 		marker.setMap(null);
			 		map.setZoom(11);
			 		$('.marker-link[data-marker="'+filter_id+'"]').remove();
			 	}
			 	if(marker.availability == filter_id) {
			 		marker.setMap(null);
			 		$('.marker-link[data-marker="'+filter_id+'"]').remove();
			 	}
			 	if(marker.gender == filter_id) {
			 		marker.setMap(null);
			 		map.setZoom(11);
			 		$('.marker-link[data-marker="'+filter_id+'"]').remove();
			 	}
			 	if(marker.economic_class == filter_id) {
			 		marker.setMap(null);
			 		map.setZoom(11);
			 		$('.marker-link[data-marker="'+filter_id+'"]').remove();
			 	}
			 	if(marker.age == filter_id) {
			 		marker.setMap(null);
			 		$('.marker-link[data-marker="'+filter_id+'"]').remove();
			 	}
			 	if(marker.profile == filter_id) {
			 		marker.setMap(null);
			 		$('.marker-link[data-marker="'+filter_id+'"]').remove();
			 	}
			 	if(marker.city == filter_id) {
			 		marker.setMap(null);
			 		map.setZoom(11);
			 		$('.marker-link[data-marker="'+filter_id+'"]').remove();
			 	}
	   		});
        }
    });


	// $('#search').change(function() {
	//		alert( "Handler for .change() called." );
	//	});

//Search Trigger Function
	$('#search').keypress(function(e) {
		console.log(e);

		var search_id = e.target.value;
		var clear_id = e.target.value;

		if(e.which == 13) {
			$.get('ajax-search?search_id=' + search_id, function(data) { 
				console.log(data);
				$.each(data, function(index, inventoryObj) {
					marker = new google.maps.Marker({
				        position: new google.maps.LatLng(inventoryObj.latitude, inventoryObj.longitude),
				        map: map,
				        category: inventoryObj.industry,
				        type: inventoryObj.format,
				        availability: inventoryObj.availability,
				        company: inventoryObj.company,
				        size: inventoryObj.size,
				        product: inventoryObj.products,
				        supplier: inventoryObj.supplier,
				        name: inventoryObj.name,
				        visible: true,
				       // animation: google.maps.Animation.DROP,
				        icon: marker_url
				    });

					google.maps.event.addListener(marker, 'click', (function(marker, i) {
						return function() {
				          	triggerInventory(true);
				           	$("#name").text(inventoryObj.name);
				           	$("#format").text(inventoryObj.format);
				           	$("#size").text(inventoryObj.size);
				           	$("#availability").text(inventoryObj.availability);
				           	$("#industry").text(inventoryObj.industry);
				           	$("#rates").text(inventoryObj.rates);
				           	$(".inventory-image").attr('src', "uploads/inventory-images/" + inventoryObj.inventory_image);
				           	// modal content
				           	$("#inventory-name").val(inventoryObj.name);
				           	$("#inventory-name-text").text(inventoryObj.name);
				           	$("#inventory-supplier").val(inventoryObj.supplier);
				           	$("#inventory-supplier-text").text(inventoryObj.supplier);
				           	$("#inventory-user").val(inventoryObj.user.first_name);
						   	$("#inventory-user-text").text(inventoryObj.user.first_name);
				           	$("#inventory-company").val(inventoryObj.company);
				           	$("#inventory-company-text").text(inventoryObj.company);
				           	$("#inventory-rate").val(inventoryObj.rates);
				           	$("#inventory-rate-text").text(inventoryObj.rates);
				           	// modal content hidden field value
				           	$("#receiver_email").val(inventoryObj.user.email);
				           	$("#receiver_id").val(inventoryObj.user.id);
				           	$("#inventory_img").val(inventoryObj.inventory_image);
				            $("#bookmark").attr('href', "bookmark/" + inventoryObj.id);
				           	map.setZoom(18);
						   	map.panTo(marker.position);
		        		}
		      		})(marker, inventoryObj.id));
	             	markers.push(marker);
				});
			});
		};
	});


	// Trigger data on click auto complete
	$(function() {
		$("#search").autocomplete({
	  		source: "ajax-autocomplete",
	  		minLength: 3,
	  		select: function(event, data) {
	  			console.log(data.item.value);
	  			var data_id = data.item.value;
	  			// $('#markers li').remove();
	  			$.get('ajax-autocomplete-select?data_id=' + data_id, function(data) { 
					console.log(data);
					$.each(data, function(index, inventoryObj) {
						marker = new google.maps.Marker({
					        position: new google.maps.LatLng(inventoryObj.latitude, inventoryObj.longitude),
					        map: map,
					        category: inventoryObj.industry,
					        type: inventoryObj.format,
					        availability: inventoryObj.availability,
					        company: inventoryObj.company,
					        size: inventoryObj.size,
					        product: inventoryObj.products,
					        supplier: inventoryObj.supplier,
					        name: inventoryObj.name,
					        visible: true,
					       // animation: google.maps.Animation.DROP,
					        icon: marker_url
					    });

						google.maps.event.addListener(marker, 'click', (function(marker, i) {
							return function() {
			          			triggerInventory(true);
					           	$("#name").text(inventoryObj.name);
					           	$("#format").text(inventoryObj.format);
					           	$("#size").text(inventoryObj.size);
					           	$("#availability").text(inventoryObj.availability);
					           	$("#product").text(inventoryObj.products);
					           	$("#industry").text(inventoryObj.industry);
					           	$("#rates").text(inventoryObj.rates);
					           	$(".inventory-image").attr('src', "uploads/inventory-images/" + inventoryObj.inventory_image);
			           			// modal content
					           	$("#inventory-name").val(inventoryObj.name);
					           	$("#inventory-name-text").text(inventoryObj.name);
					           	$("#inventory-supplier").val(inventoryObj.supplier);
					           	$("#inventory-supplier-text").text(inventoryObj.supplier);
					           	$("#inventory-user").val(inventoryObj.user.first_name);
					           	$("#inventory-user-text").text(inventoryObj.user.first_name);
					           	$("#inventory-company").val(inventoryObj.company);
					           	$("#inventory-company-text").text(inventoryObj.company);
					           	$("#inventory-rate").val(inventoryObj.rates);
					           	$("#inventory-rate-text").text(inventoryObj.rates);
					           	// modal content hidden field value
					           	$("#receiver_email").val(inventoryObj.user.email);
					           	$("#receiver_id").val(inventoryObj.user.id);
					           	$("#inventory_img").val(inventoryObj.inventory_image);
					            $("#bookmark").attr('href', "bookmark/" + inventoryObj.id);
					           	map.setZoom(18);
							   	map.panTo(marker.position);
		        			}
		      			})(marker, inventoryObj.id));

	        			markers.push(marker);

	        			$('#markers').append('<li class="marker-link"data-markerid="' + index + '" data-marker="' + data_id + '"><div class="markers-content"><a class="marker-link" data-markerid="' + index + '" href="#">' + inventoryObj.name + '</a><br/><small>' + inventoryObj.industry + '</small></div><img src="uploads/inventory-images/' + inventoryObj.inventory_image + '"/></li>');

	            		// Click Text format trigger marker function
						$('.marker-link').on('click', function () {
			        		google.maps.event.trigger(markers[$(this).data('markerid')], 'click');
			    		});
					});
				});
	  		}
		});
	});

 	// Click Zoom out
	$('#close').on('click', function () {
		map.setZoom(11);
	});

	function triggerInventory($bool) {
		var elementsToTrigger = $([$('.inventory')]);
		elementsToTrigger.each(function(){
			$(this).toggleClass('inventory-is-visible', $bool);
		});
	}
</script>