<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container navbar-inner-container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header page-scroll">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand page-scroll" href="{{ URL::to('/')}}"> <img src="{{ URL::to('img/logo.png')}}"></a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-left">
				<li class="hidden">
					<a href="#page-top"></a>
				</li>
				<li>
					<a class="page-scroll" href="{{ URL::to('/')}}">Home</a>
				</li>
				<li>
					<a class="page-scroll" href="#about">About</a>
				</li>
				<li>
					<a class="page-scroll" href="#services">Services</a>
				</li>
				<li>
					<a class="page-scroll" href="#news">News</a>
				</li>
				<li>
					<a class="page-scroll" href="#contact">Contact</a>
				</li>
				<!--<li>
                    @if( Auth::check() )
					</a><a href="{{ URL::route('logout') }}"><button class="get-started btn btn-primary">Log out</button></a>
                    @else
                   <a href="{{ URL::route('getstarted') }}"><button class="get-started btn btn-primary">Get Started</button></a>
                    @endif
				</li>-->
			</ul>
		</div>
	</div>
</nav>
