<h2>User list</h2>
<div class="confer-user-list-container">
	<ul class="confer-user-list confer-online-list">
@if ( ! $users['online']->isEmpty())
	@foreach ($users['online'] as $user)
		<li data-userId="{{ $user->id }}">
			<img class="confer-user-avatar" src="{{ url('/') . config('confer.avatar_dir') . $user->avatar }}">
			<span class="confer-user-name">{{ $user->first_name }} {{ $user->last_name }}</span>
			<i class="fa fa-circle user-status text-green pull-right"></i>
		</li>
	@endforeach
@else
	<!--<p>There are no users online (apart from you!)</p>-->
@endif
</ul>

<ul class="confer-user-list confer-not-online-list">
@if ( ! $users['offline']->isEmpty())
	@foreach ($users['offline'] as $user)
		<li data-userId="{{ $user->id }}">
			<img class="confer-user-avatar" src="{{ url('/') . config('confer.avatar_dir') . $user->avatar }}">
			<span class="confer-user-name">{{ $user->first_name }} {{ $user->last_name }}</span>
			<i class="fa fa-circle user-status  text-gray pull-right"></i>
		</li>
	@endforeach
@else
	<p>It looks like everyone is online... that's weird.</p>
</ul>
@endif
</div>