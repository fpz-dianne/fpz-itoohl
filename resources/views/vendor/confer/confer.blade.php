<div class="confer-overlay bg-aqua">
	<button class="confer-overlay-close"><i class="fa fa-close"></i></button>
	<img class="confer-bar-loader" src="{{ url('/') . config('confer.loader') }}">
	<img class="confer-overlay-loader" src="{{ url('/') . config('confer.loader') }}"/>
	<div class="confer-overlay-content">
	</div>
</div>

<div class="confer-conversation-context-menu">
	<ul class="confer-conversation-context-menu-options-list">
		<li class="confer-button-mini" id="confer-context-leave-conversation">Leave</li>
		<li class="confer-button-mini" id="confer-context-close-conversation">Close</li>
	</ul>
</div>

@if (Auth::check())
<div class="confer-open-conversations">
	<ul class="confer-open-conversations-list">
    @if ($confer_conversations)
        {!! $confer_conversations !!}
    @elseif (config('confer.allow_global'))
        	<li data-conversationId="1">
	        	<div class="confer-message confer-message-east" href="#">
	        		<span class="confer-message-item">
	        			<img class="confer-open-conversation-avatar" src="{{ url('/') . config('confer.company_avatar') }}"/>
	        			<span class="confer-open-conversation-name">All</span>
	        		</span>
	        		<span class="confer-message-content">New message!</span>
	        	</div>
        	</li>
    @endif
	</ul>
	<div class="confer-icon-list">
		<!--<a href="javascript:void(0)" class="confer-all-conversations-icon"></a>-->
		<!--<i class="fa fa-cog confer-settings-icon"></i>-->
		<a href="javascript:void(0)" class="confer-user-list-icon"></a>
	</div>
	<div id="confer-gradient"></div>
</div>
@endif