<!DOCTYPE html>
<html>
<head>
<title></title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('css/email.css') }}">
<style type="text/css">
	/*Custom by Meeco*/
	ul {
		float: left;
		width: 50%;
		margin: 0;
	}
	
	@media screen and (max-width: 525px) {
		ul {
			float: none;
			width: 100%;
			margin: auto;
		}
	}
</style>
</head>
<body class="no-spaces">

<!-- HIDDEN PREHEADER TEXT -->
<div class="hidden-preheader-text">
	iTOOhL is a platform that bonds together local OOH market data and proprietary insights to bring about efficient and effective planning for Out Of Home campaigns.
</div>

<!-- HEADER -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td bgcolor="#3C8DBC" align="center">
			<!--[if (gte mso 9)|(IE)]>
			<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
			<tr>
			<td align="center" valign="top" width="500">
			<![endif]-->
			<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="wrapper">
				<tr>
					<td align="center" valign="top" style="padding: 15px 0;" class="logo">
						<a href="http://litmus.com" target="_blank" style="text-decoration: none;">
							<!--img alt="Logo" src="http://i347.photobucket.com/albums/p458/Meeco_Raymundo/itoohl/itoohl-logo_zps4etdys4e.png?t=1475115803" width="60" height="60" style="display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;" border="0"-->
							<b class="itoohl-title">iTOOhL</b>
						</a>
					</td>
				</tr>
			</table>
			<!--[if (gte mso 9)|(IE)]>
			</td>
			</tr>
			</table>
			<![endif]-->
		</td>
	</tr>
	<tr>
		<td bgcolor="#D8F1FF" align="center" style="padding: 70px 15px 70px 15px;" class="section-padding">
			<!--[if (gte mso 9)|(IE)]>
			<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
			<tr>
			<td align="center" valign="top" width="500">
			<![endif]-->
			<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="responsive-table">
				<tr>
					<td>
						<!-- HERO IMAGE -->
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="padding" align="center">
									<a href="http://litmus.com" target="_blank"><img src="{{ $billboard['inventory_image'] }}" width="500" height="400" border="0" alt="Insert alt text here" class="hero-image img-max"></a>
								</td>
							</tr>
							<tr>
								<td>
									<!-- COPY -->
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="center" class="hello-message padding">Hi {{ $user['first_name'] }}!</td>
										</tr>
										<tr>
											<td align="center" class="email-message padding">Your Inventory, {{ $billboard['creative_campaign'] }}, has been added!</td>
										</tr>
										<tr>
											<td align="center" class="email-message padding">
												<ul style="list-style: none; padding: 0; text-align: left;">
													<li class="email-message-list"><b>Date of Collection:</b> 
														@if ($billboard['date_of_collection'] != "") {{ $billboard['date_of_collection'] }} @else None @endif
													</li>
													<li class="email-message-list"><b>Highway:</b>
														@if ($billboard['highway'] != "") {{ $billboard['highway'] }} @else None @endif
													</li>
													<li class="email-message-list"><b>Product:</b>
														@if ($billboard['product'] != "") {{ $billboard['product'] }} @else None @endif
													</li>
													<li class="email-message-list"><b>Creative Campaign:</b>
														@if ($billboard['creative_campaign'] != "") {{ $billboard['creative_campaign'] }} @else None @endif
													</li>
													<li class="email-message-list"><b>Structure:</b>
														@if ($billboard['structure'] != "") {{ $billboard['structure'] }} @else None @endif
													</li>
												</ul>
												<ul style="list-style: none; padding: 0; text-align: left;">
													<li class="email-message-list"><b>Size:</b>
														@if ($billboard['size'] != "") {{ $billboard['size'] }} @else None @endif
													</li>
													<li class="email-message-list"><b>Cost:</b>
														@if ($billboard['cost'] != "") {{ $billboard['cost'] }} @else None @endif
													</li>
													<li class="email-message-list"><b>Region:</b>
														@if ($billboard['region'] != "") {{ $billboard['region'] }} @else None @endif
													</li>
													<li class="email-message-list"><b>City:</b>
														@if ($billboard['city'] != "") {{ $billboard['city'] }} @else None @endif
													</li>
													<li class="email-message-list"><b>Location:</b>
														@if ($billboard['location'] != "") {{ $billboard['location'] }} @else None @endif
													</li>
												</ul>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="center">
									<!-- BULLETPROOF BUTTON -->
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="center" style="padding-top: 25px;" class="padding">
												<table border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
													<tr>
														<td align="center" style="border-radius: 3px;" bgcolor="#256F9C"><a href="{{ URL::to('billboards') }}" target="_blank" class="bulletproof-button mobile-button">View Now &rarr;</a></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<!--[if (gte mso 9)|(IE)]>
			</td>
			</tr>
			</table>
			<![endif]-->
		</td>
	</tr>
	<tr>
		<td bgcolor="#3C8DBC" align="center" style="padding: 20px 0px;">
			<!--[if (gte mso 9)|(IE)]>
			<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
			<tr>
			<td align="center" valign="top" width="500">
			<![endif]-->
			<!-- UNSUBSCRIBE COPY -->
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width: 500px;" class="responsive-table">
				<tr>
					<td align="center" class="address-message">
						713 Jade Bldg, Rosewood, Ususan, Taguig City              
					</td>
				</tr>
			</table>
			<!--[if (gte mso 9)|(IE)]>
			</td>
			</tr>
			</table>
			<![endif]-->
		</td>
	</tr>
</table>

</body>
</html>
