<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>@yield('title')</title>

	<link rel="icon" href="{{ URL::to('favicon.ico') }}" >

	<!-- Bootstrap Core CSS -->
	<link href="{{ URL::to('css/bootstrap.min.css')}}" rel="stylesheet">

	<!-- Modernizr -->
	<script src="{{ URL::to('js/modernizr.min.js') }}"></script>

	<!-- Custom CSS -->
	<link href="{{ URL::to('css/itoohl.css') }}" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="{{ URL::to('css/animate.css') }}" rel="stylesheet">

	<!-- Individual style -->
	@yield('style')

	<!-- Custom Fonts -->
	<link href="{{ URL::to('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
	<!--<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>-->

	<!-- Sweet Alert Style -->
  	<link rel="stylesheet" href="{{URL::to('plugins/sweet-alert/sweetalert.css')}}">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body id="page-top" class="index">

    @include('includes.header')

    <section class="content">
		@yield('content')
    </section><!--end of content-->


	 @include('includes.footer')	
    <!-- jQuery -->
    <script src="{{ URL::to('js/jquery.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ URL::to('js/bootstrap.min.js') }}"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="{{ URL::to('js/classie.min.js') }}"></script>
    <script src="{{ URL::to('js/cbpAnimatedHeader.min.js') }}"></script>

    <!-- Contact Form JavaScript -->
    <script src="{{ URL::to('js/jqBootstrapValidation.min.js') }}"></script>
    <script src="{{ URL::to('js/contact_me.min.js') }}"></script>

    <!-- WoW -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

    <!-- Sweet Alert -->
	<script src="{{ URL::to('plugins/sweet-alert/sweetalert.min.js') }}"></script>
	@include('sweet::alert')
	
    <!-- Custom Itoohl JavaScript -->
    <script src="{{ URL::to('js/itoohl.min.js') }}"></script>
    @yield('script')
	</body>
</html>