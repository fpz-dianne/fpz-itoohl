<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ URL::to('css/bootstrap.min.css')}} " media="all">
  
    @yield('style')
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ URL::to('font-awesome/css/font-awesome.min.css') }}" media="all">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" media="all">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ URL::to('plugins/select2/select2.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::to('css/admin.css') }}" media="all">
    <!-- Itoohl Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ URL::to('css/skins/_all-skins.min.css') }}" media="all">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ URL::to('plugins/iCheck/flat/blue.css') }}" media="all">
    <link rel="stylesheet" href="{{ URL::to('plugins/iCheck/all.css') }}" media="all">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ URL::to('plugins/datatables/dataTables.bootstrap.css') }}" media="all">
    <!-- Pace style -->
    <link rel="stylesheet" href="{{URL::to('plugins/pace/pace.min.css')}}" media="all">
    <!-- Sweet Alert Style -->
    <link rel="stylesheet" href="{{URL::to('plugins/sweet-alert/sweetalert.css')}}" media="all">
    <!-- Confer -->
    <link href="/vendor/confer/css/confer.css" rel="stylesheet">
    @include('confer::confer')


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-black-light sidebar-mini">
    <div class="wrapper">
    @include('includes.user-dashboard-nav')
    <!-- Left side column. contains the logo and sidebar -->
    @include('includes.user-dashboard-sidebar')
    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" id="print-all">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    @yield('dashboard-title')
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                @yield('content')
                <!-- Small boxes (Stat box) -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Create the tabs -->
            <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
                <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
            <!-- Home tab content -->
                <div class="tab-pane" id="control-sidebar-home-tab">
                    <h3 class="control-sidebar-heading">Recent Activity</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                                    <p>Will be 23 on April 24th</p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-user bg-yellow"></i>
                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>
                                    <p>New phone +1(800)555-1234</p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>
                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>
                                    <p>nora@example.com</p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="menu-icon fa fa-file-code-o bg-green"></i>
                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>
                                    <p>Execution time 5 seconds</p>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- /.control-sidebar-menu -->
                    <h3 class="control-sidebar-heading">Tasks Progress</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            <a href="javascript:void(0)">
                                <h4 class="control-sidebar-subheading">
                                    Custom Template Design <span class="label label-danger pull-right">70%</span>
                                </h4>
                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <h4 class="control-sidebar-subheading">
                                    Update Resume
                                    <span class="label label-success pull-right">95%</span>
                                </h4>
                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <h4 class="control-sidebar-subheading">
                                    Laravel Integration
                                    <span class="label label-warning pull-right">50%</span>
                                </h4>
                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <h4 class="control-sidebar-subheading">
                                    Back End Framework
                                    <span class="label label-primary pull-right">68%</span>
                                </h4>
                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- /.control-sidebar-menu -->
                </div>
                <!-- /.tab-pane -->
                <!-- Stats tab content -->
                <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                <!-- /.tab-pane -->
                <!-- Settings tab content -->
                <div class="tab-pane" id="control-sidebar-settings-tab">
                    <form method="post">
                        <h3 class="control-sidebar-heading">General Settings</h3>
                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Report panel usage
                                <input type="checkbox" class="pull-right" checked>
                            </label>    
                            <p>
                                Some information about this general settings option
                            </p>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Allow mail redirect
                                <input type="checkbox" class="pull-right" checked>
                            </label>
                            <p>
                                Other sets of options are available
                            </p>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Expose author name in posts
                                <input type="checkbox" class="pull-right" checked>
                            </label>
                            <p>
                                Allow the user to show his name in blog posts
                            </p>
                        </div>
                        <!-- /.form-group -->
                        <h3 class="control-sidebar-heading">Chat Settings</h3>
                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Show me as online
                                <input type="checkbox" class="pull-right" checked>
                            </label>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Turn off notifications
                                <input type="checkbox" class="pull-right">
                            </label>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Delete chat history
                                <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                            </label>
                        </div>
                    </form>
                </div>
            </div>
        </aside>
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
<!--
<div class="col-md-3 chat-container">
  <div class="box box-warning collapsed-box">
    <div class="box-header with-border">
      <h3 class="box-title">Chat</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <ul class="products-list product-list-in-box">
        @foreach($users as $user)
          <li class="item">
            <div class="product-img">
              <img class="img-circle" src="{{ URL::to('uploads/avatars/' . $user->avatar) }}" alt="Product Image">
            </div>
            <div class="product-info">
              <a href="javascript:void(0)" class="product-title">{{ $user->first_name }} {{ $user->last_name }}</a>
                  <span class="product-description">
                    Admin
                  </span>
            </div>
          </li>  
        @endforeach
      </ul>  
    </div>
  </div>
</div>
-->

<!-- ./wrapper -->

    <!-- jQuery 2.2.3 -->
    <script src="{{ URL::to('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ URL::to('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ URL::to('js/bootstrap.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ URL::to('plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <!-- Custom Script Per Page -->
    @yield('script')
    <!-- Slimscroll -->
    <script src="{{ URL::to('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{ URL::to('plugins/iCheck/icheck.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ URL::to('plugins/fastclick/fastclick.js') }}"></script>
    <!-- Itoohl App -->
    <script src="{{ URL::to('js/app.min.js')}} "></script>
    <!-- Itoohl for demo purposes -->
    <script src="{{ URL::to('js/demo.js') }}"></script>
    <!-- Sweet Alert -->
    <script src="{{ URL::to('plugins/sweet-alert/sweetalert.min.js') }}"></script>
    @include('sweet::alert')
    <!-- PACE -->
    <script src="{{ URL::to('plugins/pace/pace.min.js')}}"></script>
    <script type="text/javascript">
      // To make Pace works on Ajax calls
        $(document).ajaxStart(function() { Pace.restart(); });
        $('.ajax').click(function(){
            $.ajax({url: '#', success: function(result){
                $('.ajax-content').html();
            }});
        });

       //Ajax call for products
        $('#notify_read_all').on('click' ,function(e){
            console.log(e);
            //var notify_read_all_id = e.target.value;
            $('#notify-count').empty();
            $.get('notify-read-all');
        });  

        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/pusher/3.0.0/pusher.min.js"></script>
    <script src="/js/moment.min.js"></script>
    @include('confer::js')
</body>
</html>
