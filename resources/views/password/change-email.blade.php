<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Itoohl| Change password</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ URL::to('css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ URL::to('font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::to('css/admin.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ URL::to('plugins/iCheck/square/blue.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition lockscreen">
    <!-- Automatic element centering -->
    <div class="lockscreen-wrapper">
        <!-- User name -->
        <div class="lockscreen-name"></div>
        <!-- START LOCK SCREEN ITEM -->
        <div class="lockscreen-item">
            <!-- /.lockscreen-image -->
            <!-- lockscreen credentials (contains the form) -->
            <div class="login-box-body">
                <form action="{{ URL::route('change-password') }}" method="post">
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" placeholder="New password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                    <!-- /.col -->
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Change Email</button>
                            {{ csrf_field() }}
                        </div>
                    <!-- /.col -->
                    </div>
                </form>
            </div>
        <!-- /.lockscreen credentials -->
        </div>
        <!-- /.lockscreen-item -->
        <div class="text-center">
            <a href="{{ URL::route('profile') }}">Go Back </a> Or
            <a href="{{ URL::route('logout') }}"> sign in as a different user</a>
        </div>
    </div>
    <!-- /.center -->
    <!-- jQuery 2.2.3 -->
    <script src="{{ URL::to('js/jquery.js') }}"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ URL::to('js/bootstrap.min.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ URL::to('plugins/iCheck/icheck.min.js')}}"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
</body>
</html>
