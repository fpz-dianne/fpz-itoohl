@extends('layouts.master')

@section('title')
    iTOOhL
@endsection

@section('style')
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ URL::to('plugins/iCheck/all.css') }}">
@endsection

<header>
    <div class="container">
        <div class="intro-text">
            <div class="intro-lead-in">
                <h1 class="text-center">
                    <span class="text-white">Your OOH Hub</span>
                </h1>
            </div>
            <div class="intro-heading">
                is a platform that bonds together local OOH market data and proprietary insights to bring
                <br>about efficient and effective planning for Out Of Home campaigns
            </div>
            <div class="col-md-4  col-md-offset-4">
                <a href="javascript::void(0)" class="btn btn-primary bg-white text-aqua" data-toggle="modal" data-target="#request-demo"><i class="fa fa-arrow-circle-o-down"></i> Request for a Demo</a>
            </div>
        </div>
    </div>
</header>

@section('content')

    @include('includes.request-demo-modal')    

    <div class="row">
        <section id="about" class="col-md-12 text-center">
            <div class="row">
                <div class="col-md-12 wow fadeinUp">
                    <h2 class="section-heading">About</h2>
                </div>  
            </div><!--end of row-->

            <div class="row">
                <div class="row" style="padding: 4em 4em 78px;">
                    <div class="col-md-4">
                        <div class="box box-default has-shadow wow fadeinRight">
                            <div class="box-body no-padding">
                                <div class="who"></div>
                            </div>
                            <div class="box-footer">
                                <h3 class="text-pink">WHO</h3>
                                <p>We are passionate advocates seeking unified and advanced interests for the Out Of Home advertising industry.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-default has-shadow wow fadeinRight delay-05s">
                            <div class="box-body no-padding">
                                <div class="what"></div>
                            </div>
                            <div class="box-footer">
                                <h3 class="text-pink">WHAT</h3>
                                <p>We provide services for brands to strategically plan, timely monitor and analyze Out Of Home campaigns with efficiency and accuracy.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-default has-shadow wow fadeinRight delay-1s">
                            <div class="box-body no-padding">
                                <div class="why"></div>
                            </div>
                            <div class="box-footer">
                                <h3 class="text-pink">WHY</h3>
                                <p>We believe that Out Of Home advertising has been growing immensely; and with this, acquiring accurate insights and effective tools are necessities to protect the investment in Out Of Home execution.</p>
                            </div>
                        </div>
                    </div><!--end of row-->
                </div>
            </div>
        </section><!--end of about-->

        <section id="services" class="col-md-12 text-center">
            <div class="row">
                <div class="col-md-12 wow fadeinUp">
                    <h2 class="section-heading">Our Services</h2>
                    <p class="sub-heading">Know about iTOOhL offerings</p>
                </div>
            </div>

            <div class="row feature feature-margin">
                <div class="col-md-3">
                    <div class="feature-block f-h-1 wow fadeInLeft">
                        <img src="{{ URL::to('img/Plattform.png')}}">
                        <h6>Platform</h6>
                        <p>We provide data for the different platform inventory formats and allow direct interaction with the rich media inventory</p>
                        <div class="clip-hide" style="background:url(img/Services-Platform.gif); background-position: 50% 50%;
    background-repeat: no-repeat; background-size: cover;"></div>
                    </div>
                    <div class="feature-block f-h-2 wow fadeInLeft delay-05s">
                        <img src="{{ URL::to('img/Insights.png')}}">
                        <h6>Insights</h6>
                        <p>iTOOhL enables planners and advertisers to strategically plan with the use of updated research and insights</p>
                        <div class="clip-hide" style="background:url(img/Services-Insights.gif);background-position: 50% 50%;
    background-repeat: no-repeat; background-size: cover;"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="features-monitor">
                        <div class="monitor-mask">
                            <img src="{{ URL::to('img/monitor.png') }}" alt="">
                        </div>
                        <div class="monitor-img scal-img">
                            <div class="clip">
                                <div class="bg bg-bg-chrome act transition-05s"> <!-- style="background-image:url('img/Services-Locations.gif')" -->
                                </div>
                                <div class="video-bg">
                                    <video loop muted autoplay poster="img/Services-Platform.gif" class="video" style="height: 100%;">
                                        <source src="img/Services-Locations.mp4" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>                  
                </div>
                <div class="col-md-3">
                    <div class="feature-block f-h-3 wow fadeInRight">
                        <img src="{{ URL::to('img/Locations.png')}}">
                        <h6>Locations</h6>
                        <p> We allow planners and advertisers to search for inventory in different locations nationwide</p>
                        <div class="clip-hide" style=""></div>
                    </div>
                    <div class="feature-block f-h-4 wow fadeInRight delay-05s">
                        <img src="{{ URL::to('img/Products.png')}}">  
                        <h6>Industries</h6>
                        <p>We can generate specific product placements per industry</p>
                        <div class="clip-hide" style="background:url(img/Services-Products.gif); background-position: 50% 50%;
    background-repeat: no-repeat; background-size: cover;"></div>
                    </div>
                </div>
            </div><!--end of row-->
        </section><!--end of services-->

        <section id="provide" class="col-md-12 text-center">
            <div class="row">
                <div class="col-md-12 wow fadeinUp">
                    <h2 class="section-heading">We Also Provide</h2>
                </div>
            </div>

            <div class="row">
                <div class="row" style="padding: 4em 0 0;">
                    
                    <div class="col-md-6 wow fadeInLeft">
                        <img src="{{ URL::to('img/provide-map.png')}}" class="provide-map" alt="">
                    </div>

                    <div class="col-md-6" style="padding: 3em;">
                        <div class="provide-list" data-toggle="collapse" href="#collapse-one" aria-expanded="true" aria-controls="collapse-one">
                            <div class="provide-list-header" role="tab">
                              <h4>
                                <a href="javascript::void(0)">
                                 <i class="fa fa-expand provide-icon"></i> Extensive Search and Mapping tools <i class="fa fa-angle-down pull-right"></i>
                                </a>
                              </h4>

                              <div id="collapse-one" class="collapse" role="tabpanel">
                                  <div class="provide-list">
                                    <ul>
                                        <li>Industry/Brand capture per platform/industry</li>
                                        <li>Media Format Selection</li>
                                        <li>Site Specification</li>
                                    </ul>
                                  </div>
                              </div>

                            </div>
                        </div>

                        <div class="provide-list">
                            <div class="provide-list-header" data-toggle="collapse" href="#collapse-two" aria-expanded="true" role="tab">
                              <h4>
                                <a href="javascript::void(0)">
                                 <i class="fa fa-tag provide-icon" style="transform: rotate(90deg);" ></i> Powerful Inventory Management <i class="fa fa-angle-down pull-right"></i>
                                </a>
                              </h4>

                              <div id="collapse-two" class="collapse" role="tabpanel">
                                  <div class="provide-list">
                                        <ul>
                                            <li>Market Inventory</li>
                                            <li>Report generation</li>
                                        </ul>
                                    </div>
                              </div>
                            </div>
                        </div>

                        <div class="provide-list">
                            <div class="provide-list-header" data-toggle="collapse" href="#collapse-three" aria-expanded="true" role="tab">
                              <h4>
                                <a href="javascript::void(0)">
                                  <i class="fa fa-star provide-icon"></i> Media Plan Creation <i class="fa fa-angle-down pull-right"></i>
                                </a>
                              </h4>

                                <div id="collapse-three" class="collapse" role="tabpanel">
                                  <div class="provide-list">
                                        <ul>
                                            <li>Strategy Output</li>
                                            <li>Creative Testing</li>
                                            <li>Investment Maximization</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="provide-list">
                            <div class="provide-list-header" data-toggle="collapse" href="#collapse-four" aria-expanded="true" role="tab">
                              <h4>
                                <a href="javascript::void(0)">
                                  <i class="fa fa-signal provide-icon"></i> Relevant Insights and Updated OOH News <i class="fa fa-angle-down pull-right"></i>
                                </a>
                              </h4>

                              <div id="collapse-four" class="collapse" role="tabpanel">
                                  <div class="provide-list">
                                        <ul>
                                            <li>Traffic Count</li>
                                            <li>Brand Insight</li>
                                            <li>iTOOhL Scoring</li>
                                            <li>Industry Updates</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="provide-list">
                            <div class="provide-list-header" data-toggle="collapse" href="#collapse-five" aria-expanded="true" role="tab">
                              <h4>
                                <a href="javascript::void(0)">
                                <img src="{{ URL::to('img/clock.png')}}" class="provide-list-img"> Vendor Portal Functionalities <i class="fa fa-angle-down pull-right"></i>
                                </a>
                              </h4>

                              <div id="collapse-five" class="collapse" role="tabpanel">
                                  <div class="provide-list">
                                        <ul>
                                            <li>Request for quotation</li>
                                            <li>Booking Memo Issuance</li>
                                            <li>Messaging and Notifications</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="provide-list">
                            <div class="provide-list-header" data-toggle="collapse" href="#collapse-six" aria-expanded="true" role="tab">
                              <h4>
                                <a href="javascript::void(0)">
                                  <img src="{{ URL::to('img/ruler.png')}}" class="provide-list-img"> OOH Effectiveness Measurement <i class="fa fa-angle-down pull-right"></i>
                                </a>
                              </h4>

                              <div id="collapse-six" class="collapse" role="tabpanel">
                                  <div class="provide-list">
                                        <ul>
                                            <li>Pre Campaign Management</li>
                                            <li>Post Campaign Analysis</li>
                                            <li>Specific Inventory Target</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </section><!--end of provide--> 

        <section id="news" class="col-md-12">
            <div class="row text-center text-gray">
                <div class="col-md-12 wow fadeInUp">
                    <h2 class="section-heading">Latest News and Case Studies</h2>
                </div>
            </div>

            <div class="row news-container" style="padding: 4em 4em 114px;">

                <div class="col-md-4 no-padding">
                    <div class="col-md-12 no-padding">
                        <a href="http://www.adweek.com/adfreak/here-are-years-4-best-out-home-campaigns-judged-obie-awards-170982" target="_blank">
                        <div class="box box-default wow fadeInRight">
                            <div class="box-body text-gray no-padding">
                                <img class="long-img" src="{{ URL::to('img/news-item-1.jpg')}}">
                                <h4 class="news-content-title text-left">Here Are the Year's 4 Best Out-of-Home Campaigns...<br></h4>
                            </div>
                            <div class="box-footer long-box-footer" style="padding-top: 0;">
                                <ul class="list-inline">
                                    <li class="pull-left">The Outdoor Advertising Association of America gave out its 2016 OBIE Awards for the year's best out-of-home advertising in Boca Raton, Fla., this week...</a></li>
                                    <li class="pull-left" style="margin-top: 1em;"><i class="fa fa-calendar"></i> 21 April 2015</li>
                                </ul>
                            </div>
                        </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-4 no-padding">
                    <div class="col-md-12">
                        <a href="http://www.campaignlive.co.uk/article/havas-rolls-digital-outdoor-shop-adcity-global-network/1400378" target="_blank">
                        <div class="box box-default wow fadeInLeft delay-05s">
                            <div class="box-body text-gray no-padding">
                            <img src="{{ URL::to('img/news-item-3.jpg')}}">
                                <h4 class="news-content-title text-left">Havas rolls out digital outdoor shop Adcity as global network<br></h4>
                            </div>
                            <div class="box-footer" style="padding-top: 0;">
                                <ul class="list-inline">
                                    <li class="pull-left"><i class="fa fa-calendar"></i> 20 July 2015</li>
                                </ul>
                            </div>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-12" >
                        <a href="http://www.immap.com.ph/news/check-out-whos-speaking-immap-digicon-the-power-of-x/" target="_blank">
                        <div class="box box-default wow fadeInLeft delay-05s">
                            <div class="box-body text-gray no-padding">
                            <img src="{{ URL::to('img/news-item-4.jpg')}}">
                                <h4 class="news-content-title text-left">Check out who’s speaking @IMMAP DigiCon: The Power of X<br></h4>
                            </div>
                            <div class="box-footer" style="padding-top: 0;">
                                <ul class="list-inline">
                                    <li class="pull-left"><i class="fa fa-calendar"></i> 22 March 2016</li>
                                </ul>
                            </div>
                        </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-4 no-padding">
                    <div class="col-md-12 no-padding">
                        <a href="http://www.campaignlive.co.uk/article/marketing-new-thinking-awards-2016-full-results/1410561" target="_blank">
                        <div class="box box-default wow fadeInRight">
                            <div class="box-body text-gray no-padding">
                                <img class="long-img" src="{{ URL::to('img/news-item-2.jpg')}}">
                                <h4 class="news-content-title text-left">Marketing New Thinking Awards 2016: the full results<br></h4>
                            </div>
                            <div class="box-footer long-box-footer" style="padding-top: 0;">
                                <ul class="list-inline">
                                    <li class="pull-left">The Channel 4's innovative promotion of its Humans show has won the Grand Prix at the Marketing New Thinking Awards 2016, in partnership with Sky Media...</a></li>
                                    <li class="pull-left" style="margin-top: 1em;"><i class="fa fa-calendar"></i> 29 September 2016</li>
                                </ul>
                            </div>
                        </div>
                        </a>
                    </div>
                </div>  
            </div>
        </div><!--end of row-->
    </section><!--end of news-->

</div><!--end of row-->


@endsection

@section('script')
    <script>
        $('#address').popover({ html:true, content: '<img src="http://maps.googleapis.com/maps/api/staticmap?center=14.645425,+121.052854&zoom=14&scale=false&size=700x400&maptype=roadmap&key=AIzaSyBYRrmLiM21xODjVlJuCyCyauWkeOPFD48&format=png&visual_refresh=true&markers=icon:http://itoohl.com/img/icon-location.png%7Cshadow:true%7C14.645425,+121.052854" alt="" />', placement:'top' })
    </script>
    <!-- iCheck 1.0.1 -->
    <script src="{{ URL::to('plugins/iCheck/icheck.min.js') }}"></script>
    <script>
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].square-yellow, input[type="radio"].square-yellow').iCheck({
          checkboxClass: 'icheckbox_flat-blue',
          radioClass: 'icheckbox_flat-blue'
        });
    </script>
@endsection